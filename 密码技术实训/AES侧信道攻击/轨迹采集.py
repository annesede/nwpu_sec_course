from osrtoolkit.oscilloscope import Pico3000
from osrtoolkit.trace import ETSStorer

pico = Pico3000()  # 声明pico示波器对象
pico.form  # 打开pico示波器配置界面

# 补全存放文件的名称
ets = ETSStorer('trace.ets', open_type='w')  # open_type = 'w' or 'a'，如果已经存在文件， 'w' 将覆盖, ‘a’ 将在原文件进行追加 



# 补全引用库
import serial
import numpy as np

# 补全函数参数
mega = serial.Serial('COM3', 115200, timeout=1)

def get_meta():
    # 产生随机明文，补全函数参数
    p = np.random.randint(0, 256, 16, dtype=np.uint8)
    send = p.tobytes()
    # 发送数据到开发板，补全函数
    mega.write(send)
    # 读取加密结果，补全函数
    recv = mega.read(16)
    c = np.frombuffer(recv, dtype=np.uint8)
    return p, c



from Crypto.Cipher import AES

# 与开发板设置相同的密钥，验证开发板结果
key = bytes([0x00, 0x01, 0x02, 0x03, 
    0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b, 
    0x0c, 0x0d, 0x0e, 0x0f])

# 验证函数，使用Crypto库的AES类验证开发板结果是否正确
def verify(p, c):
    # 补全函数
    c_correct = aes.encrypt(p.tobytes())
    if c.tobytes() == c_correct:
        return True
    else:
        return False
    
# 得到随机明文和开发板的加密结果
p,c = get_meta()
# 使用验证函数验证开发板结果是否正确
verify(p, c)



# 补全引用库
import matplotlib.pyplot as plt

plt.rcParams['figure.figsize'] = 20,6

pico.arm()  # 将示波器置于等待触发模式，等待一次触发时间进行采集

for i in range(pico.sequence_num):
    get_meta()# 控制TOE进行加密，完成对示波器的触发

trs = pico.acquire_samples()  # 从pico示波器取回数据

plt.plot(trs[0][0].T)
plt.grid()
plt.show()

from tqdm import tnrange
import time
# 共采集1万条数据，补全循环参数
for i in tnrange(100):
    # 采集一个secquence的数据，检查示波器设置
    pico.arm()
    # 创建空数组存放明文和密文，补全函数参数
    plaintext = np.empty((pico.sequence_num, 16), dtype=np.uint8)
    ciphertext = np.empty((pico.sequence_num, 16), dtype=np.uint8)
    flag = True
    for j in range(pico.sequence_num):
        m = get_meta()
        if not verify(m[0], m[1]):
            flag = False
            break
        plaintext[j] = m[0]
        ciphertext[j] = m[1]
    if not flag:
        flag = True
        print('ERROR')
        continue
    # 进行延时操作，补全函数
    time.sleep(0.05)  # 这里必须对示波器进行一定的延迟后再进行数据拉取操作，以响应示波器时间
    # 从示波器取回数据，补全函数
    trs = pico.acquire_samples()
    data = {'meta':{'plaintext': plaintext, 'ciphertext': ciphertext}, 'samples': trs}
    ets.update(data)
ets.finish()

# 关闭接口
mega.close()
