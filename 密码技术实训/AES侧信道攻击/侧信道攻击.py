from osrtoolkit.oscilloscope import Pico3000
from osrtoolkit.trace import ETSStorer
import matplotlib.pyplot as plt

pico = Pico3000()  # 声明pico示波器对象
pico.form  # 打开pico示波器配置界面



import scared
from scared import aes

S = aes.selection_functions.encrypt.FirstSubBytes()

a = scared.CPAAttack(
        selection_function=S,
        model=scared.HammingWeight(),
        discriminant=scared.maxabs)

# 补充轨迹文件名称
ths = scared.traces.read_ths_from_ets_file('trace.ets')

container = scared.Container(ths)

a.run(container)

a.results.shape

key = bytes([0x00, 0x01, 0x02, 0x03, 
    0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b, 
    0x0c, 0x0d, 0x0e, 0x0f])

plt.plot(a.results[:,0, :].T, 'C1')
plt.plot(a.results[key[0], 0, :], 'C2')
plt.show()



import numpy as np
cpa_first_round_key = np.argmax(a.scores, axis=0) 
np.array_equal(bytearray(key), cpa_first_round_key)

cpa_first_round_key
