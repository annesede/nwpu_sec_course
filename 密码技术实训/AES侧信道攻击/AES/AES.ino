#include "aes.h"

uint8_t key[] = {
    0x00, 0x01, 0x02, 0x03, 
    0x04, 0x05, 0x06, 0x07, 
    0x08, 0x09, 0x0a, 0x0b, 
    0x0c, 0x0d, 0x0e, 0x0f
};
uint8_t in[16];

uint8_t out[16]; // 128
uint8_t *w;
void setup() {
  int i;
  w = aes_init(sizeof(key));
  aes_key_expansion(key, w);
  Serial.begin(115200);
  pinMode(8, OUTPUT); 
  digitalWrite(8, LOW);
}

void loop() {
  if (Serial.available()== 16){
    Serial.readBytes(in, 16);
    digitalWrite(8, HIGH);
    aes_cipher(in, out , w);
    digitalWrite(8, LOW);
    Serial.write(out, 16);
  }
}
