#include <iostream>
#include <fstream>
#include <vector>

static const int NUM = int(1e6 + 1);
static bool isSieved[NUM] = {false};
std::vector<int> prime;

void linear_sieve() {
	isSieved[1] = true;
	for (int curr = 2; curr < NUM; ++curr) {
		if (!isSieved[curr]) prime.emplace_back(curr);
		for (int multi = 0; multi < prime.size() && prime[multi] * curr < NUM; ++multi) {
			isSieved[prime[multi] * curr] = true;
			if (curr % prime[multi] == 0) break;
		}
	}
}

int main() {
	linear_sieve();
	std::cout << "The number of primes between 1 and 1e6 is: " << prime.size() << std::endl;

	std::ofstream outfile("./prime_list.dat");
	for (int i : prime) outfile << i << std::endl;
	outfile.close();
	return 0;
}
