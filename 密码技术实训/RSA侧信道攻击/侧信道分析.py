import numpy as np
import matplotlib.pyplot as plt

#numpy print options to hexadecimal
np.set_printoptions(formatter={'int':hex})
plt.rcParams["figure.figsize"] = [20,6]

trace = np.load('rsa.npy')

plt.plot(trace)
plt.show()

plt.plot(trace)

plt.plot([21800 for _ in range(-80,30)],[i for i in range(-80,30)],color = 'r')
plt.plot([81600 for _ in range(-80,30)],[i for i in range(-80,30)],color = 'r')
plt.plot([99100 for _ in range(-80,30)],[i for i in range(-80,30)],color = 'y')
plt.plot([139000 for _ in range(-80,30)],[i for i in range(-80,30)],color = 'y')

plt.show()


m = 128
d = 97673 # 根据示波器图片得到
n = 7753
s = pow(128,d,7753)
s
