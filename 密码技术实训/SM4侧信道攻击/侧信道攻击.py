import scared
import numpy as np
from osrtoolkit.cipher import sm4
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = 20,6

ths = scared.traces.read_ths_from_ets_file('trace.ets')



@scared.attack_selection_function()
def first_sub_bytes(plaintext, guesses):
    x = plaintext[:, 4:8] ^ plaintext[:, 8:12] ^ plaintext[:, 12:16]
    res = np.empty((x.shape[0], len(guesses), x.shape[1]), dtype='uint8')
    for guess in guesses:
        res[:, guess, :] = sm4.Constant.SBOX[x ^ guess]
    return res

# 补充攻击函数设置
a = scared.CPAAttack(
        selection_function=first_sub_bytes,
        model=scared.HammingWeight(),
        discriminant=scared.maxabs)

container = scared.container.Container(ths)

a.run(container)

round_key_1 = np.argmax(a.scores, axis=0)  # 第一轮轮密钥
print(round_key_1)

for i in range(4):
    plt.subplot(2,2,i+1)
    plt.bar(range(256), a.scores[:, i])
    plt.bar([round_key_1[i]], a.scores[round_key_1[i], i])
    plt.xlabel('Key guesses')
    plt.ylabel('score')
    plt.title(f'byte {i}')
plt.tight_layout()
plt.show()



def sm4_round(plaintext, round_key):
    '''根据轮密钥和明文，计算轮输出值
    Parameters:
            plaintext: 2 维ndarray, 最后一维有16个元素.
            round_key: 2 维ndarray，第一维为轮数，第二维有4个元素，表示该轮的轮密钥
    Output:
        ciphertext: 2 维ndarray, 最后一维有16个元素.
    '''
    plaintext_to_use = sm4.SubFunction.bytes_to_word(plaintext).reshape((-1, 4))
    round_key_word = sm4.SubFunction.bytes_to_word(round_key.reshape((-1, 4)))
    ciphertext_to_use = np.copy(plaintext_to_use)
    for current_round in range(len(round_key_word)):
        save_state = np.copy(ciphertext_to_use)
        ciphertext_to_use[:, 0] = ciphertext_to_use[:, 1] ^ ciphertext_to_use[:, 2] ^ ciphertext_to_use[:, 3] ^ round_key_word[current_round]
        ciphertext_to_use[:, 1:4] = np.zeros((ciphertext_to_use.shape[0], 3), dtype=np.uint32)
        ciphertext_to_use[:, 0] = sm4.SubFunction.sub_bytes(ciphertext_to_use[:, 0])
        ciphertext_to_use[:, 0] = sm4.SubFunction.op_l(ciphertext_to_use[:, 0])
        ciphertext_to_use = np.bitwise_xor(ciphertext_to_use, save_state)
        ciphertext_to_use = np.roll(ciphertext_to_use, -1, axis=1)
    cipher_bytes = sm4.SubFunction.word_to_bytes(ciphertext_to_use).reshape((-1, 16))
    return cipher_bytes

sm4_round(ths[0].plaintext, round_key_1)

@scared.attack_selection_function()
def second_sub_bytes(plaintext, guesses):
    r1_out = sm4_round(plaintext, round_key_1)
    x = r1_out[:, 4:8] ^ r1_out[:, 8:12] ^ r1_out[:, 12:16]  
    res = np.empty((x.shape[0], len(guesses), x.shape[1]), dtype='uint8')
    for guess in guesses:
        res[:, guess, :] = sm4.Constant.SBOX[x ^ guess]
    return res

# 补充攻击函数设置
a = scared.CPAAttack(
        selection_function=first_sub_bytes,
        model=scared.HammingWeight(),
        discriminant=scared.maxabs)

a.run(container)

round_key_2 = np.argmax(a.scores, axis=0)  # 第二轮轮密钥
print(round_key_2)

for i in range(4):
    plt.subplot(2,2,i+1)
    plt.bar(range(256), a.scores[:, i])
    plt.bar([round_key_2[i]], a.scores[round_key_2[i], i])
    plt.xlabel('Key guesses')
    plt.ylabel('score')
    plt.title(f'byte {i}')
plt.tight_layout()
plt.show()



round_key = np.vstack((round_key_1, round_key_2))

@scared.attack_selection_function()
def third_sub_bytes(plaintext, guesses):
    r1_out = sm4_round(plaintext, round_key)
    x = r1_out[:, 4:8] ^ r1_out[:, 8:12] ^ r1_out[:, 12:16]  
    res = np.empty((x.shape[0], len(guesses), x.shape[1]), dtype='uint8')
    for guess in guesses:
        res[:, guess, :] = sm4.Constant.SBOX[x ^ guess]
    return res

# 补充攻击函数设置
a = scared.CPAAttack(
        selection_function=first_sub_bytes,
        model=scared.HammingWeight(),
        discriminant=scared.maxabs)

a.run(container)

round_key_3 = np.argmax(a.scores, axis=0)  # 第三轮轮密钥
print(round_key_3)

for i in range(4):
    plt.subplot(2,2,i+1)
    plt.bar(range(256), a.scores[:, i])
    plt.bar([round_key_3[i]], a.scores[round_key_3[i], i])
    plt.xlabel('Key guesses')
    plt.ylabel('score')
    plt.title(f'byte {i}')
plt.tight_layout()
plt.show()



round_key = np.vstack((round_key_1, round_key_2, round_key_3))

round_key

@scared.attack_selection_function()
def fourth_sub_bytes(plaintext, guesses):
    r1_out = sm4_round(plaintext, round_key)
    x = r1_out[:, 4:8] ^ r1_out[:, 8:12] ^ r1_out[:, 12:16]  
    res = np.empty((x.shape[0], len(guesses), x.shape[1]), dtype='uint8')
    for guess in guesses:
        res[:, guess, :] = sm4.Constant.SBOX[x ^ guess]
    return res

# 补充攻击函数设置
a = scared.CPAAttack(
        selection_function=first_sub_bytes,
        model=scared.HammingWeight(),
        discriminant=scared.maxabs)

a.run(container)

round_key_4 = np.argmax(a.scores, axis=0)  # 第四轮轮密钥
print(round_key_4)

for i in range(4):
    plt.subplot(2,2,i+1)
    plt.bar(range(256), a.scores[:, i])
    plt.bar([round_key_4[i]], a.scores[round_key_4[i], i])
    plt.xlabel('Key guesses')
    plt.ylabel('score')
    plt.title(f'byte {i}')
plt.tight_layout()
plt.show()



round_key = np.vstack((round_key_1, round_key_2, round_key_3, round_key_4))

round_key

master_key = sm4.SubFunction.inv_key_schedule(round_key, 0)

''.join([hex(x)[2:].zfill(2) for x in master_key[0]])
