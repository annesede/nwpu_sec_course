import serial
import numpy as np
import time
from osrtoolkit.cipher.sm4 import SM4
from tqdm import tnrange
from osrtoolkit.oscilloscope import Pico3000
from osrtoolkit.trace import ETSStorer

import matplotlib.pyplot as plt

plt.rcParams['figure.figsize'] = 20,6

toe = serial.Serial('COM3', 115200, timeout=1)

def get_meta():
    p = np.random.randint(0, 256, 16, dtype=np.uint8)
    send = p.tobytes()
    toe.write(send)
    recv = toe.read(16)
    c = np.frombuffer(recv, dtype=np.uint8)
    return p, c

# 构建验证函数，在采集过程中对加密结果进行验证(非必须)
def verify(p, c):
    # 补充函数
    c_correct = SM4.encrypt(p, key)
    return (c_correct==c).all()

key = np.array([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f],
              dtype = np.uint8)

p,c = get_meta()
verify(p, c)



pico = Pico3000()  # 声明pico示波器对象
pico.form  # 打开pico示波器配置界面

pico.arm()  # 将示波器置于等待触发模式，等待一次触发时间进行采集

for i in range(pico.sequence_num):
    get_meta()# 控制TOE进行加密，完成对示波器的触发

trs = pico.acquire_samples()  # 从pico示波器取回数据

plt.plot(trs[0][0].T)
plt.grid()
plt.show()



ets = ETSStorer('trace.ets', open_type='w')  # open_type = 'w' or 'a'，如果已经存在文件， 'w' 将覆盖, ‘a’ 将在原文件进行追加 



import time

for i in tnrange(100):
    # 采集一个secquence的数据
    pico.arm()
    plaintext = np.empty((pico.sequence_num, 16), dtype=np.uint8)
    ciphertext = np.empty((pico.sequence_num, 16), dtype=np.uint8)
    flag = True
    for j in range(pico.sequence_num):
        # 进行一次加密
        m = get_meta()
        # 验证开发板加密结果
        if not verify(m[0], m[1]):
            flag = False
            break
        plaintext[j] = m[0]
        ciphertext[j] = m[1]
    if not flag:
        flag = True
        print('ERROR')
        continue
    time.sleep(0.05)  # 这里必须对示波器进行一定的延迟后再进行数据拉取操作，以响应示波器时间
    trs = pico.acquire_samples()
    data = {'meta':{'plaintext': plaintext, 'ciphertext': ciphertext}, 'samples': trs}
    ets.update(data)
ets.finish()

# 关闭接口
toe.close()
