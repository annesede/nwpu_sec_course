import serial
import numpy as np
import time
from osrtoolkit.cipher.sm4 import SM4

# 根据实际情况更改串行接口
toe = serial.Serial('COM3', 115200, timeout=1)

def get_meta():
    p = np.random.randint(0, 256, 16, dtype=np.uint8)
    send = p.tobytes()
    toe.write(send)
    recv = toe.read(16)
    c = np.frombuffer(recv, dtype=np.uint8)
    return p, c



# 与开发板密钥一致
key = np.array([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f],
              dtype = np.uint8)

def verify(p, c):
    c_correct = SM4.encrypt(p, key)
    return (c_correct==c).all()



# 记录循环运行的性能和加密结果
import time

def verify_multiple_runs(num_runs):
    results = []
    for _ in range(num_runs):
        start_time = time.time()
        p, c = get_meta()
        elapsed_time = time.time() - start_time
        is_valid = verify(p, c)
        results.append((p, c, is_valid, elapsed_time))
        sleep(0.05)
    return results


results_100 = verify_multiple_runs(100)
results_1000 = verify_multiple_runs(1000)
results_10000 = verify_multiple_runs(10000)


def save_results_to_file(results, filename='result.dat'):
    with open(filename, 'w') as file:
        for p, c, is_valid, elapsed_time in results:
            file.write(f'p: {p.tolist()}, c: {c.tolist()}, valid: {is_valid}, time: {elapsed_time:.6f}\n')


save_results_to_file(results_100, 'result_100.dat')
save_results_to_file(results_1000, 'result_1000.dat')
save_results_to_file(results_10000, 'result_10000.dat')


# 关闭接口
toe.close()
