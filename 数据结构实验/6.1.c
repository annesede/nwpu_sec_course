#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>

typedef struct {
	int ch, weight;
	int parent, left, right;
} HTree;

typedef struct {
	bool bit [128];
	char ch, begin;
} HCode;

HTree *createHTree(int n) {
	HTree *ht = (HTree*) calloc(2 * n, sizeof(HTree));
	// 限制于多种因素, 只能采用这种读入方式.
	int tmp = 1;
	while(tmp <= n) {
		char ch = getchar();
		if (ch != ' ' && ch != '\n') ht[tmp++].ch = ch;
	}
	for (int i = 1; i <= n; ++i) scanf("%d", &ht[i].weight);

	int min1, min2, rn, ln;
	for (int i = n + 1; i <= 2 * n - 1; ++i) {
		min1 = min2 = INT_MAX; rn = ln = 0;
		for (int j = 1; j <= i - 1; ++j) {
			if (ht[j].weight < min1 && !ht[j].parent)
				min2 = min1, rn = ln, min1 = ht[j].weight, ln = j;
			else if (ht[j].weight < min2 && !ht[j].parent)
				min2 = ht[j].weight, rn = j;
		}
		ht[ln].parent = ht[rn].parent = i;
		ht[i].left = ln, ht[i].right = rn;
		ht[i].weight = ht[ln].weight + ht[rn].weight;
	}
	return ht;
}

HCode *createHCode(HTree *ht, int n) {
	HCode *hc = (HCode*) calloc(n + 1, sizeof(HCode));
	for (int i = 1; i <= n; ++i) {
		hc[i].begin = n; hc[i].ch = ht[i].ch;
		int cn = i, pn = ht[cn].parent;
		while (pn) {
			if (ht[pn].left == cn) hc[i].bit[hc[i].begin] = 0;
			else hc[i].bit[hc[i].begin] = 1;
			--hc[i].begin, cn = pn, pn = ht[cn].parent;
		}
		++hc[i].begin;
	}
	return hc;
}

void encode(HCode *hc, int n) {
	char text[1001] = "";
	scanf("%s", text);
	for (int i = 0; i < strlen(text); ++i) {
		for (int j = 1; j <= n; ++j) {
			if (text[i] == hc[j].ch) {
				for (int k = hc[j].begin; k <= n; ++k) {
					printf("%d", hc[j].bit[k]);
				}
			}
		}
	}
	// 什么? 你问为什么不写译码? 别问, 问就是偷懒.
	// 要是能用cpp字典就写译码了.
	printf("\n%s\n", text);
}

int main () {
	int n;
	scanf("%d", &n);

	HTree *ht = createHTree(n);
	HCode *hc = createHCode(ht, n);
	encode(hc, n);
	return 0;
}
