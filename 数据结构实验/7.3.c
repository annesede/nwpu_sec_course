#include <stdio.h>

#define MAXSIZE 105

int vn = 0;
int dist[MAXSIZE][MAXSIZE] = {};

void init(void) {
	scanf("%d", &vn);
	for (int i = 0; i < vn; ++i)
		for (int j = 0; j < vn; ++j)
			scanf("%d", &dist[i][j]);
}

void floyd(void) {
	for (int n = 0; n < vn; ++n)
		for (int i = 0; i < vn; ++i)
			for (int j = 0; j < vn; ++j)
				if (dist[i][n] + dist[n][j] < dist[i][j])
					dist[i][j] = dist[i][n] + dist[n][j];
}

void printLen(void) {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; ++i) {
		int a, b;
		scanf("%d %d", &a, &b);
		printf("%d\n", dist[a][b]);
	}
}

int main () {
	init();
	floyd();
	printLen();
	return 0;
}
