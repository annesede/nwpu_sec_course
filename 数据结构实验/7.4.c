#include <stdio.h>

#define MAXSIZE 105

int vn = 0;
int dist[MAXSIZE][MAXSIZE] = {};
int path[MAXSIZE][MAXSIZE] = {};

int stack[MAXSIZE] = {};
int top = -1;

void init(void) {
	scanf("%d", &vn);
	for (int i = 0; i < vn; ++i)
		for (int j = 0; j < vn; ++j) {
			scanf("%d", &dist[i][j]);
			path[i][j] = -1;
		}
}

void floyd(void) {
	for (int n = 0; n < vn; ++n)
		for (int i = 0; i < vn; ++i)
			for (int j = 0; j < vn; ++j)
				if (dist[i][n] + dist[n][j] < dist[i][j]) {
					dist[i][j] = dist[i][n] + dist[n][j];
					path[i][j] = n;
				}
}

void findPath(int a, int b) {
	stack[++top] = b;
	if (path[a][b] == -1) {
		stack[++top] = a;
		return;
	}
	findPath(a, path[a][b]);
}

void printPath(void) {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; ++i) {
		int a, b;
		scanf("%d %d", &a, &b);
		top = -1;
		findPath(a, b);
		while (top > -1) printf("%d\n", stack[top--]);
	}
}

int main () {
	init();
	floyd();
	printPath();
	return 0;
}
