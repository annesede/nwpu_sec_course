#include <stdio.h>
#define MAXSIZE 400

int S[MAXSIZE][3] = {};

void transpose(int cnt) {
	for (int i = 0; i < cnt; ++i) {
		int tmp = S[i][0];
		S[i][0] = S[i][1], S[i][1] = tmp;
	}
	// insertion: stable
	for (int i = 1; i < cnt; ++i) {
		int raw = S[i][0], col = S[i][1], val = S[i][2];
		int j = i - 1;
		while ((j >= 0) && raw < S[0][j]) {
			S[j + 1][0] = S[j][0], S[j + 1][1] = S[j][1], S[j + 1][2] = S[j][2];
			--j;
		}
		S[j + 1][0] = raw, S[j + 1][1] = col, S[j + 1][2] = val;
	}
}

int main () {
	int n, m;
	scanf("%d %d", &n, &m);
	int cnt = 0;
	while(scanf("%d %d %d", &S[cnt][0], &S[cnt][1], &S[cnt][2])) {
		if (S[cnt][0] == 0 && S[cnt][1] == 0 && S[cnt][2] == 0) break;
		++cnt;
	}

	transpose(cnt);
	for (int i = 0; i < cnt; ++i)
		printf("%d %d %d\n", S[i][0], S[i][1], S[i][2]);
	return 0;
}
