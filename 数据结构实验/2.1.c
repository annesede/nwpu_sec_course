#include <stdio.h>

int main () {
	int n, m;
	scanf("%d", &n);
	int arr1[n];
	for (int i = 0; i < n; ++i) scanf("%d", &arr1[i]);
	scanf("%d", &m);
	int arr2[m];
	for (int i = 0; i < m; ++i) scanf("%d", &arr2[i]);

	int arr[n + m], head1 = 0, head2 = 0, head = 0;
	while (head1 < n && head2 < m) {
		if (arr1[head1] < arr2[head2]) arr[head++] = arr1[head1++];
		else arr[head++] = arr2[head2++];
	}
	while (head1 < n) arr[head++] = arr1[head1++];
	while (head2 < m) arr[head++] = arr2[head2++];

	for (int i = 0; i < n + m; ++i) printf("%d\n", arr[i]);
	return 0;
}
