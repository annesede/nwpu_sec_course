#include <stdio.h>

#define MAXSIZE 400

int S1[MAXSIZE][3] = {};
int S2[MAXSIZE][3] = {};
int S[MAXSIZE][3] = {};

int main () {
	int r1, c1, r2, c2, t1 = 0, t2 = 0, sum = 0, h = 0;
	scanf("%d %d", &r1, &c1);
	while(scanf("%d %d %d", &S1[t1][0], &S1[t1][1], &S1[t1][2])) {
		if (S1[t1][0] == 0) break;
		++t1;
	}
	scanf("%d %d", &r2, &c2);
	while(scanf("%d %d %d", &S2[t2][0], &S2[t2][1], &S2[t2][2])) {
		if (S2[t2][0] == 0) break;
		++t2;
	}

	for (int i = 1; i <= r1; ++i) {
		for (int j = 1; j <= c2; ++j) {
			for (int h1 = 0; h1 <= t1; ++h1) {
				for (int h2 = 0; h2 <= t2; ++h2) {
					if (S1[h1][0] == i && S2[h2][1] == j && S1[h1][1] == S2[h2][0])
						sum += S1[h1][2] * S2[h2][2];
				}
			}
			if (!sum) continue;
			S[h][0] = i, S[h][1] = j, S[h][2] = sum; sum = 0; ++h;
		}
	}

	for (int i = 0; i < h; ++i)
		printf("%d %d %d\n", S[i][0], S[i][1], S[i][2]);
	return 0;
}
