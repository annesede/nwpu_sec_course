#include <stdio.h>
#include <stdbool.h>

#define MAXSIZE 105
#define INF 10000

int vn = 0;
int dist[MAXSIZE][MAXSIZE] = {};

bool isVisited[MAXSIZE] = {};
int len[MAXSIZE] = {};

void init(void) {
	scanf("%d", &vn);
	for (int i = 0; i < vn; ++i)
		for (int j = 0; j < vn; ++j)
			scanf("%d", &dist[i][j]);
}

void dijkstra(void) {
	for (int i = 0; i < vn; ++i) len[i] = INF;
	isVisited[0] = true; len[0] = 0;
	for (int i = 0; i < vn; ++i)
		if (dist[0][i] != INF) len[i] = dist[0][i];

	for (int i = 0; i < vn - 1; ++i) {
		int minLen = INF, next;
		for (int j = 0; j < vn; ++j)
			if (!isVisited[j] && len[j] < minLen)
				minLen = len[j], next = j;
		isVisited[next] = true;

		for (int j = 0; j < vn; ++j)
			if (!isVisited[j] && dist[next][j] != INF) {
				int tmp = len[next] + dist[next][j];
				len[j] = len[j] < tmp ? len[j] : tmp;
			}

	}
}

void printLen(void) {
	for (int i = 0; i < vn; ++i)
		printf("%d\n", len[i]);
}

int main () {
	init();
	dijkstra();
	printLen();
	return 0;
}
