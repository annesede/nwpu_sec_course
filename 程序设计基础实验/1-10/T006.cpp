// 浮点数输出
#include <stdio.h>

int main(){
    double a = 0.0f;
    scanf("%lf",&a);
    printf("%.6lf,%.2lf,%.8lf",a,a,a);
    return 0;
}