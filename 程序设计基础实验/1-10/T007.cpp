// 动态宽度输出
#include <stdio.h>

int main(){
    int n = 0, m = 0, k = 0;
    scanf("%d %d",&n,&m);
    int tmp = n;
    while (tmp) {
        tmp /= 10;
        ++k;
    }
    for (int i = 0; i < m-k; ++i) {
        printf("%d",0);
    }
    printf("%d",n);
    return 0;
}