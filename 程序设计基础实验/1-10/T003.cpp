// 数据类型大小及范围
#include <stdio.h>
#include <limits.h>

int main(){
    int id = 0;
    scanf("%d",&id);
    switch (id) {
        case 1: // char
            printf("%llu,%d,%d\n",sizeof(char),CHAR_MIN,CHAR_MAX);
            break;
        case 2: // unsigned char
            printf("%llu,%u,%u\n",sizeof(unsigned char),0,UCHAR_MAX);
            break;
        case 3: // short
            printf("%llu,%hd,%hd\n",sizeof(short),SHRT_MIN,SHRT_MAX);
            break;
        case 4: // unsigned short
            printf("%llu,%hu,%hu\n",sizeof(unsigned short),0,USHRT_MAX);
            break;
        case 5: // int
            printf("%llu,%d,%d\n",sizeof(int),INT_MIN,INT_MAX);
            break;
        case 6: // unsigned int
            printf("%llu,%u,%u\n",sizeof(unsigned int),0,UINT_MAX);
            break;
        case 7: //long
            printf("%llu,%ld,%ld\n",sizeof(int),LONG_MIN,LONG_MAX);
            break;
        case 8: // unsigned long
            printf("%llu,%u,%lu\n",sizeof(unsigned long),0,ULONG_MAX);
            break;
        case 9: // long long
            printf("%llu,%lld,%lld\n",sizeof(int),LLONG_MIN,LLONG_MAX);
            break;
        case 10: // unsigned long long
            printf("%llu,%u,%llu\n",sizeof(unsigned long long),0,ULLONG_MAX);
            break;
    }
    return 0;
}