// 风寒指数
#include <stdio.h>
#include <math.h>

int main(){
    double v,T;
    scanf("%lf %lf",&v,&T);
    int chill = 13.12f+0.6215f*T-11.37f*pow(v,0.16f)+0.3965f*T*pow(v,0.16f)+0.5f;
    printf("%d",chill);
    return 0;
}