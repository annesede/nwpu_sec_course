// 左右操作
#include <stdio.h>
#include <string.h>

void quickSort(char str[], int left, int right) {
	if (left >= right) return;
	char flag = str[(left + right) / 2];
	int head = left - 1, tail = right + 1;
	while (head < tail) {
		do head++; while (str[head] > flag);
		do tail--; while (str[tail] < flag);
		if (head < tail) {
			char tmp = str[head];
			str[head] = str[tail], str[tail] = tmp;
		}
	}
	quickSort(str, left, tail);
	quickSort(str, tail + 1, right);
}

void reverse(char str[], int begin, int end) {
	int head = begin, tail = end;
	while (head <= tail) {
		char tmp = str[head];
		str[head] = str[tail], str[tail] = tmp;
		++head, --tail;
	}
}

int main() {
	char str[1005] = "";
	scanf("%s", str);
	int len = strlen(str);
	quickSort(str, 0, len / 2 - 1);
	int mid = (len & 1) ? (len /2 + 1) : (len / 2);
	reverse(str, mid, len - 1);
	printf("%s", str);
	return 0;
}