// 平方根
#include <stdio.h>
#include <math.h>

int main() {
	int n;
	scanf("%d", &n);
	FILE *fp1 = fopen("rr.dat", "w");
	for (int i = 1; i <= n; ++i)
		fprintf(fp1, "%.6lf ", sqrt(i));
	fclose(fp1);
 
	FILE *fp2 = fopen("rr.dat", "r");
	for (int i = 1; i <= n; ++i) {
		double output;
		fscanf(fp2, "%lf", &output);
		printf("%.6lf ", output);
	}
	fclose(fp2);
	return 0;
}