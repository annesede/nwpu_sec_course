// 空中交通管制
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
	char id[64];
	int x;
	int y;
} Plane;

int main() {
	int n;
	scanf("%d", &n);
	Plane *plane[n];
	for (int i = 0; i < n; ++i) {
		plane[i] = (Plane *) malloc(sizeof(Plane));
		scanf("%s %d %d", plane[i]->id, &plane[i]->x, &plane[i]->y);
	}

	double minDist = 1e9;
	int idx1, idx2;
	for (int i = 0; i < n - 1; ++i) {
		for (int j = i + 1; j < n; ++j) {
			double dist = sqrt(pow(plane[i]->x - plane[j]->x, 2) +
				pow(plane[i]->y - plane[j]->y, 2));
			if (dist < minDist) idx1 = i, idx2 = j, minDist = dist;
		}
	}
	printf("%s-%s %.4lf", plane[idx1]->id, plane[idx2]->id, minDist);
	return 0;
}