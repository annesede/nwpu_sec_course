// 零钞
#include <stdio.h>

int main() {
	int s;
	scanf("%d", &s);

	int cnt[4], r = s;
	cnt[0] = r / 10, r -= cnt[0] * 10;
	cnt[1] = r / 5, r -= cnt[1] * 5;
	cnt[2] = r / 2, cnt[3] = r - cnt[2] * 2;

	if (cnt[3]) printf("1=%d\n", cnt[3]);
	if (cnt[2]) printf("2=%d\n", cnt[2]);
	if (cnt[1]) printf("5=%d\n", cnt[1]);
	if (cnt[0]) printf("10=%d\n", cnt[0]);
	return 0;
}