// 机场翻盘显示
#include <stdio.h>
#include <ctype.h>

int cnt(char src, char dest) {
	if (src == dest) return 0;
	if (isupper(src))
		return (src < dest) ? (dest - src) : ('Z' - src + dest - 'A' + 1);
	if ('0' <= src && src <= '9') {
/*		if (src == '0') src = '9' + 1;
		if (dest == '0') dest = '9' + 1;
		return (src > dest) ? (src - dest) : (src - '0' + '9' + 1 - dest + 1);*/
		return (src < dest) ? (dest - src) : ('9' - src + dest - '0' + 1);
	}
	return -1;
}

int main() {
	char id1[10] = "", id2[10] = "";
	scanf("%s %s", id1, id2);
	int num = 0;
	for (int i = 0; id1[i] && id2[i] ; ++i) num += cnt(id1[i], id2[i]);
	printf("%d\n", num);
	return 0;
}