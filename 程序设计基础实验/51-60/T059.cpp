// 分离字符串
#include <stdio.h>
#include <string.h>

void split(char *str, char *sep) {
    while (*str) {
        char *flag = strstr(str, sep);
        if (flag == NULL) break;
        char sub[101] = "";
        int len = flag - str;
        memcpy(sub, str, len);
        puts(sub);
        memmove(str - len - strlen(sep), str, strlen(str) + 1);
    }
    puts(str);
}

int main() {
    char str[2000] = "", sep[2000] = "";
    scanf("%[^\n] %[^\n]", str, sep);
    split(str, sep);
    return 0;
}