// 大小写交换
#include <stdio.h>

void strSwapCase(char str[]) {
    for (int i = 0; str[i] != '\0'; ++i) {
        if ('a' <= str[i] && str[i] <= 'z')
            str[i] = (char) str[i] - 'a' + 'A';
        else if ('A' <= str[i] && str[i] <= 'Z')
            str[i] = (char) str[i] - 'A' + 'a';
    }
}

int main() {
    char input[1000] = "";
    scanf("%[^\n]",input);
    strSwapCase(input);
    puts(input);
    return 0;
}