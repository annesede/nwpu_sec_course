// 字符串后缀
#include <stdio.h>
#include <string.h>

void strEndsWith(char *str, char *suffix) {
    int len = strlen(suffix);
    char *pStr = str + strlen(str) - len, *pSuffix = suffix;
    while (*pSuffix && *pStr) {
        if (*pSuffix != *pStr) {
            printf("No\n");
            return;
        }
        else ++pSuffix, ++pStr;
    }
    printf("Yes\n");
}

int main() {
    char str[1000] = "", suffix[1000] = "";
    scanf("%[^\n] %[^\n]", str, suffix);
    strEndsWith(str, suffix);
    return 0;
}