// 删除前后缀
#include <stdio.h>
#include <string.h>

void strRemovePrefix(char *str, char *prefix) {
    int cnt = 0;
    char *pStr = str, *pPrefix = prefix;
    while (*pPrefix && *pStr && *pStr == *pPrefix) {
        while (*pPrefix && *pStr && *pStr == *pPrefix)
            ++pStr, ++pPrefix, ++cnt;
        pPrefix = prefix;
    }

    int len = strlen(prefix);
    int mov = (cnt / len) * len;
    if (mov) memmove(str - mov, str, strlen(str) + 1);
}

void strRemoveSuffix(char *str, char *suffix) {
    int len = strlen(suffix);
    char *pStr = str + strlen(str) - len, *pSuffix = suffix;
    while (*pSuffix && pStr >= str && *pStr == *pSuffix) {
        int cnt = 0;
        while (*pSuffix && pStr && *pStr == *pSuffix)
            ++pStr, ++pSuffix, ++cnt;
        if (cnt == len) {
            pSuffix = suffix, pStr = pStr - 2 * len;
            *(pStr + len) = '\0';
        } else break;
    }
}

int main() {
    char str1[1000] = "", fix[1000] = "", str2[1000] = "";
    scanf("%[^\n] %[^\n]", str1, fix);
    memcpy(str2, str1, strlen(str1) + 1);

    strRemovePrefix(str1, fix);
    puts(str1);
    strRemoveSuffix(str2, fix);
    puts(str2);
    return 0;
}