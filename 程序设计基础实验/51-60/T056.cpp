// Atol转换
#include <stdio.h>
#include <limits.h>

int atol(char *str) {
    char *pStr = str;
    int sgn = 1;
    long long tmp = 0;
    if (*pStr == '+') ++pStr;
    else if (*pStr == '-') sgn = -1, ++pStr;

    while (*pStr) {
        if (*pStr == ' ') ;
        else if ('0' <= *pStr && *pStr <= '9') {
            tmp = (*pStr - '0') + tmp * 10;
            if ((tmp * sgn) >= INT_MAX) return INT_MAX;
            else if ((tmp * sgn) <= INT_MIN) return INT_MIN;
        }
        else break;
        ++pStr;
    }
    return tmp * sgn;
}

int main() {
    char str[1000] = "";
    scanf("%[^\n]", str);
    printf("%d", atol(str));
    return 0;
}