// 元宇宙A+B
#include <stdio.h>
#include <string.h>

const static char decToMeta[37] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
static char c[100] = "", a[100] = "", b[100] = "";
static int C[100] = {0}, A[100] = {0}, B[100] = {0};

int metaToDec(char m) {
    if ('0' <= m && m <= '9') return m - '0';
    return m - 'A' + 10;
}

void add(void) {
    int lenA = strlen(a), lenB = strlen(b);
    for (int i = 0; i < lenA; ++i) A[i] = metaToDec(a[lenA - i - 1]);
    for (int i = 0; i < lenB; ++i) B[i] = metaToDec(b[lenB - i - 1]);

    int carry = 0;
    int lenC = lenA > lenB ? lenA : lenB;
    for (int i = 0; i < lenC; ++i) {
        C[i] = A[i] + B[i] + carry;
        carry = C[i] / 36;
        C[i] %= 36;
    }
    if (carry != 0) {
        C[lenC] = carry;
        ++lenC;
    }

    for (int i = lenC - 1; i >= 0; --i) c[i] = decToMeta[C[lenC - i - 1]];
    c[lenC] = '\0';
}

int main() {
    scanf("%s %s", a, b);
    add();
    puts(c);
    return 0;
}