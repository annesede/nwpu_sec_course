// 字符串切片
#include <stdio.h>
#include <string.h>

int t, n, len;
char str[1000];

void strSlice(int begin, int end, int step) {
    char slice[1000] = "";
    int pos = 0;
    if (begin < 0) begin += len;
    if (end < 0) end += len;
    if (end >= begin && step > 0) {
        for (int i = begin; i < end; i += step) {
            slice[pos] = str[i];
            ++pos;
        }
    } else if (end < begin && step < 0) {
        for (int i = begin; i > end; i += step) {
            slice[pos] = str[i];
            ++pos;
        }
    }
    puts(slice);
}

void mode(void) {
    len = strlen(str);
    int begin, end, step;
    switch (n) {
        case 3: {
            scanf("%d %d %d", &begin, &end, &step);
            break;
        }
        case 2: {
            scanf("%d %d", &begin, &end);
            step = 1;
            break;
        }
        case 1: {
            scanf("%d", &begin);
            end = len, step = 1;
            break;
        }
    }
    strSlice(begin, end, step);
}

int main() {
    scanf("%[^\n] %d", str, &t);
    for (int i = 0; i < t; ++i) {
        scanf("%d", &n);
        mode();
    }
    return 0;
}