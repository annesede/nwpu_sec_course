// Kids A+B
#include <stdio.h>
#include <string.h>

char ans[30] = "";

int strToNum(char *str) {
    if(strstr(str, "zero")) return 0;
    if(strstr(str, "ten")) return 10;
    if(strstr(str, "eleven")) return 11;
    if(strstr(str, "twelve")) return 12;
    if(strstr(str, "thirteen")) return 13;
    if(strstr(str, "fourteen")) return 14;
    if(strstr(str, "fifteen")) return 15;
    if(strstr(str, "sixteen")) return 16;
    if(strstr(str, "seventeen")) return 17;
    if(strstr(str, "eighteen")) return 18;
    if(strstr(str, "nineteen")) return 19;

    int unit = 0, decade = 0;
    if(strstr(str, "one")) unit = 1;
    if(strstr(str, "two")) unit = 2;
    if(strstr(str, "three")) unit = 3;
    if(strstr(str, "four")) unit = 4;
    if(strstr(str, "five")) unit = 5;
    if(strstr(str, "six")) unit = 6;
    if(strstr(str, "seven")) unit = 7;
    if(strstr(str, "eight")) unit = 8;
    if(strstr(str, "nine")) unit = 9;
    if(strstr(str, "twenty")) decade = 20;
    if(strstr(str, "thirty")) decade = 30;
    if(strstr(str, "forty")) decade = 40;
    if(strstr(str, "fifty")) decade = 50;
    if(strstr(str, "sixty")) decade = 60;
    if(strstr(str, "seventy")) decade = 70;
    if(strstr(str, "eighty")) decade = 80;
    if(strstr(str, "ninety")) decade = 90;
    return unit + decade;
}

void numToStr(int n) {
    switch (n) {
        case 0: {
            strcpy(ans, "zero");
            char *p = ans;
            return;
        }
        case 11: {
            strcpy(ans, "eleven");
            char *p = ans;
            return;
        }
        case 12: {
            strcpy(ans, "twelve");
            char *p = ans;
            return;
        }
        case 13: {
            strcpy(ans, "thirteen");
            char *p = ans;
            return;
        }
        case 14: {
            strcpy(ans, "fourteen");
            char *p = ans;
            return;
        }
        case 15: {
            strcpy(ans, "fifteen");
            char *p = ans;
            return;
        }
        case 16: {
            strcpy(ans, "sixteen");
            char *p = ans;
            return;
        }
        case 17: {
            strcpy(ans, "seventeen");
            char *p = ans;
            return;
        }
        case 18: {
            strcpy(ans, "eighteen");
            char *p = ans;
            return;
        }
        case 19: {
            strcpy(ans, "nineteen");
            char *p = ans;
            return;
        }
        default:
            break;
    }

    int decade = (n / 10) % 10, unit = n % 10;
    switch (decade) {
        case 2: {
            strcpy(ans, "twenty");
            break;
        }
        case 3: {
            strcpy(ans, "thirty");
            break;
        }
        case 4: {
            strcpy(ans, "forty");
            break;
        }
        case 5: {
            strcpy(ans, "fifty");
            break;
        }
        case 6: {
            strcpy(ans, "sixty");
            break;
        }
        case 7: {
            strcpy(ans, "seventy");
            break;
        }
        case 8: {
            strcpy(ans, "eighty");
            break;
        }
        case 9: {
            strcpy(ans, "ninety");
            break;
        }
        default: {
            break;
        }
    }

    if (decade && unit) strcat(ans, "-");

    switch (unit) {
        case 1: {
            strcat(ans, "one");
            break;
        }
        case 2: {
            strcat(ans, "two");
            break;
        }
        case 3: {
            strcat(ans, "three");
            break;
        }
        case 4: {
            strcat(ans, "four");
            break;
        }
        case 5: {
            strcat(ans, "five");
            break;
        }
        case 6: {
            strcat(ans, "six");
            break;
        }
        case 7: {
            strcat(ans, "seven");
            break;
        }
        case 8: {
            strcat(ans, "eight");
            break;
        }
        case 9: {
            strcat(ans, "nine");
            break;
        }
        default: {
            break;
        }
    }
}

int main() {
    char a[30] = "", b[30] = "";
    scanf("%s %s", a, b);

    numToStr(strToNum(a) + strToNum(b));
    puts(ans);
    return 0;
}