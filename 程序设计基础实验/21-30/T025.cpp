// 毕达哥拉斯三元组
#include <stdio.h>

unsigned long long pythagoras(unsigned int sum){
    // a as short catheti, b as long catheti, c as hypotenuse
    unsigned a,b,c;
    for (a = 1; a <= sum/4; ++a){
        for (b = a+1; b <= sum/2; ++b){
            c = sum-a-b;
            if ((a+b > c) & (c-a < b) & (a*a+b*b == c*c)){
                return a*b*c;
            }
        }
    }
}

int main(){
    unsigned int n;
    scanf("%u",&n);
    printf("%d", pythagoras(n));
    return 0;
}