// 最大数字
#include <stdio.h>
#include <stdbool.h>

bool isNonDecr(unsigned int n){
    unsigned int left;
    while(n){
        left = (n/10)%10;
        if (left > (n%10)){
            return false;
        }
        n /= 10;
    }
    return true;
}

int main() {
    unsigned int n, res;
    scanf("%u",&n);
    while(!isNonDecr(n)){
        --n;
    }
    printf("%u",n);
    return 0;
}