// 俄罗斯农夫乘法
#include <stdio.h>

int main(){
    int multi = 0, a, b;
    scanf("%d %d",&a,&b);
    while(a){
        printf("%d %d\n",a,b);
        if (a&1) {
            multi += b;
        }
        a >>= 1;
        b <<= 1;
    }
    printf("%d",multi);
    return 0;
}