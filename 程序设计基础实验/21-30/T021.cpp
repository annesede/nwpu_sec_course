// 余数和
#include <stdio.h>

int main() {
    unsigned int sum = 0, n, k;
    scanf("%u %u",&n,&k);
    for (unsigned int i = 1; i <= n; ++i) {
        sum += k%i;
    }
    printf("%u",sum);
    return 0;
}