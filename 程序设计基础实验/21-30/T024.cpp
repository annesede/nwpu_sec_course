// 好数字
#include <stdio.h>

typedef unsigned long long uint64;
const uint64 mod = 1e9+7;

uint64 power(uint64 a, uint64 e){
    uint64 r = 1;
    while (e){
        if (e&1){
            r = (r*a)%mod;
        }
        a = (a*a)%mod;
        e >>= 1;
    }
    return r;
}

int main(){
    uint64 n, num;
    scanf("%llu",&n);
    num = power(4,n/2)*power(5,n-n/2)%mod;
    printf("%llu",num);
    return 0;
}