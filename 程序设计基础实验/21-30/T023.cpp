// 倒水
#include <stdio.h>
#include <string.h>

#define INF 0x3f3f3f3f

const int maxSize = 10000;
int distTable[10000][10000];
int queue[2][10000] = {0};

int bfs(int n, int m, int d){
    int head = 0, tail = 1;
    memset(distTable, 0x3f, sizeof(distTable));
    distTable[0][0] = 0;

    while(tail != head){
        int a = queue[0][head], b = queue[1][head];
        if (a == d || b == d) {
            return distTable[a][b];
        }

        ++head;
        if(head == maxSize-1) {
            head = 0;
        }

        // full
        if(distTable[a][m] == INF) {
            distTable[a][m] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = a;
            queue[1][tail] = m;
        }
        if(distTable[n][b] == INF) {
            distTable[n][b] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = n;
            queue[1][tail] = b;
        }

        // empty
        if(distTable[a][0] == INF) {
            distTable[a][0] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = a;
            queue[1][tail] = 0;
        }
        if(distTable[0][b] == INF) {
            distTable[0][b] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = 0;
            queue[1][tail] = b;
        }

        // pour
        if(a+b <= n && distTable[a+b][0] == INF){
            distTable[a+b][0] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = a+b;
            queue[1][tail] = 0;
        }
        if(a+b > n && distTable[n][a+b-n] == INF){
            distTable[n][a+b-n] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = n;
            queue[1][tail] = a+b-n;
        }
        if(a+b <= m && distTable[0][a+b] == INF){
            distTable[0][a+b] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = 0;
            queue[1][tail] = a+b;
        }
        if(a+b > m && distTable[a+b-m][m] == INF){
            distTable[a+b-m][m] = distTable[a][b]+1;
            ++tail;
            queue[0][tail] = a+b-m;
            queue[1][tail] = m;
        }

        if (tail == maxSize) {
            tail = 0;
        }
    }

    return 0;
}

int main(){
    int n,m,d;
    scanf("%d %d %d",&n,&m,&d);
    printf("%d", bfs(n,m,d));
    return 0;
}