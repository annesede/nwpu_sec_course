// 竖式乘法
#include <stdio.h>
#include <math.h>

typedef unsigned int uint;

uint digit(uint x){
    uint cnt = 0;
    if (x == 0){
        return 1;
    }
    while(x){
        x /= 10;
        ++cnt;
    }
    return cnt;
}

uint getDigital(uint x, uint n){
    x /= (uint)pow(10,n-1);
    return x%10;
}

int main(){
    uint up, down;
    scanf("%u %u",&up,&down);
    uint ans = up*down, len = digit(ans)+1, downLen = digit(down);

    for (uint i = 0; i < len-digit(up); ++i) {
        printf(" ");
    }
    printf("%u\n",up);
    printf("x");
    for (uint i = 1; i <= len-downLen-1; ++i) {
        printf(" ");
    }
    printf("%u\n",down);

    for (uint i = 1; i <= len; ++i) {
        printf("-");
    }
    printf("\n");

    uint tmp;
    for (uint i = 1; i <= downLen; ++i) {
        tmp = getDigital(down,i)*up;
        if (i == downLen){
            printf("+");
        } else {
            for (uint j = 1; j <= len-digit(tmp)-i+1; ++j) {
                printf(" ");
            }
        }
        printf("%u\n",tmp);
    }

    for (uint i = 1; i <= len; ++i) {
        printf("-");
    }
    printf("\n");

    printf(" %u",ans);

    return 0;
}