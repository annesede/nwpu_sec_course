// 阶乘倍数
#include <stdio.h>
#include <stdbool.h>
 
typedef unsigned long long int64;
 
int64 primeFactNum = 0;
int64 prime[20] = {0}, num[20] = {0};
 
bool isMulti(int64 n){
    int64 primeNum, tmp;
    for (int64 i = 1; i <= primeFactNum; ++i) {
        primeNum = 0;
        tmp = n;
        while (tmp) {
            primeNum += tmp/prime[i];
            tmp /= prime[i];
        }
        if(primeNum < num[i]) {
            return false;
        }
    }
    return true;
}
 
void solveFact(int64 k){
    for (int64 i = 2; i*i <= k; ++i) {
        if(k%i == 0){
            ++primeFactNum;
            prime[primeFactNum] = i;
            while (k%i == 0){
                ++num[primeFactNum];
                k /= i;
            }
        }
    }
    if (k > 1){
        ++primeFactNum;
        prime[primeFactNum] = k;
        ++num[primeFactNum];
    }
}
 
int main(){
    int64 left = 1, right = 1e19, mid, n, k;
    scanf("%lld",&k);
    solveFact(k);
 
    while(left <= right){
        mid = ((right-left)>>1)+left;
        if (isMulti(mid)){
            right = mid-1;
            n = mid;
        } else {
            left = mid+1;
        }
    }
    printf("%lld",n);
    return 0;
}