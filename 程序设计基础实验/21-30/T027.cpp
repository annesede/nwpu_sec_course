// 查找数列
#include <stdio.h>

int main(){
    int cnt = 1, sum = 0, n, res;
    scanf("%d",&n);
    while(n-sum > 0){
        sum += cnt;
        ++cnt;
    }
    sum -= cnt;
    res = n-sum == 0 ? cnt : (n-sum-1);
    printf("%d",res);
    return 0;
}