// 方案数
#include <stdio.h>

int main(){
    int cnt = 0, n;
    scanf("%d",&n);
    for (int i = 1; i*(i+1) <= 2*n; ++i) {
        if ((n-i*(i-1)/2)%i == 0){
            ++cnt;
        }
    }
    printf("%d",cnt);
    return 0;
}