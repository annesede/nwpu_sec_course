// 子数组最大和
#include <stdio.h>
#include <string.h>

int maxSum(int arr[], int n) {
	int dp[n];
	memset(dp, 0, n * sizeof(int));
	int maxi = arr[0];
	dp[0] = arr[0];
	for (int i = 1; i < n; ++i) {
		dp[i] = arr[i] > (dp[i - 1] + arr[i]) ? arr[i] : (dp[i - 1] + arr[i]);
		maxi = dp[i] > maxi ? dp[i] : maxi;
	}
	return maxi;
}

int main() {
	int n;
	scanf("%d", &n);
	int arr[n];
	for (int i = 0; i < n; ++i) scanf("%d", &arr[i]);
	printf("%d\n", maxSum(arr, n));
	return 0;
}