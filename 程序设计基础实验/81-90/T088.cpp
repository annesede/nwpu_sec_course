// 打字机
#include <stdio.h>
#include <string.h>

long long dp[100];
long long seg[100] = {0};

long long method(char str[]) {
	memset(dp, -1, 100 * sizeof(long long));
	char *iter = str;
	int part = 0, ans = 1;
	seg[part] = 1;
	while (*iter) {
		if (*iter == 'm' || *iter == 'w') return 0;
		if (*iter == 'n' && *(iter + 1) == 'n') {
			int cnt = 1;
			dp[0] = 1, dp[1] = 2, iter += 2;
			while(*iter == 'n') {
				++cnt, ++iter;
				dp[cnt] = dp[cnt - 1] + dp[cnt - 2];
			}
			seg[++part] = dp[cnt];
		}
		else if (*iter == 'u' && *(iter + 1) == 'u') {
			int cnt = 1;
			dp[0] = 1, dp[1] = 2, iter += 2;
			while(*iter == 'u') {
				++cnt, ++iter;
				dp[cnt] = dp[cnt - 1] + dp[cnt - 2];
			}
			seg[++part] = dp[cnt];
		}
		else ++iter;
	}
	for (int i = 0; seg[i] ; ++i) ans *= seg[i];
	return ans;
}

int main() {
	char str[1000];
	scanf("%s", str);
	printf("%lld", method(str));
	return 0;
}