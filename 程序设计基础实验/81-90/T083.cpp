// 挑选
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>

void quickSort(long long arr[], long long left, long long right) {
	if (left >= right) return;
	srand(time(NULL));
	long long idx = rand() % (left - right) + left;
	long long flag = arr[idx], head = left - 1, tail = right + 1;
	while (head < tail) {
		do head++; while(arr[head] < flag);
		do tail--; while(arr[tail] > flag);
		if (head < tail) {
			long long tmp = arr[head];
			arr[head] = arr[tail];
			arr[tail] = tmp;
		}
	}
	quickSort(arr, left, tail);
	quickSort(arr, tail + 1, right);
}

long long maxi(long long arr[], long long n) {
	long long max = LLONG_MIN;
	for (long long i = 0; i < n; ++i)
		if (max < arr[i]) max = arr[i];
	return max;
}

long long sumMax(long long arr[], long long n) {
	long long dp[n];
	dp[0] = arr[0];
	for (long long i = 1; i < n; ++i) {
		if (arr[i] == arr[i - 1])
			dp[i] = dp[i - 1] > (arr[i] + dp[i - 1]) ? dp[i - 1] : (arr[i] + dp[i - 1]);
		else {
			long long j = i - 1;
			while (j >= 0 && arr[j] >= arr[i] - 1) --j;
			dp[i] = arr[i] + (j >= 0 ? dp[j] : 0);
		}
	}
	return maxi(dp, n);
}

int main() {
	long long n;
	scanf("%lld", &n);
	long long arr[n];
	for (long long i = 0; i < n; ++i) scanf("%lld", &arr[i]);

	quickSort(arr, 0, n - 1);
	printf("%lld\n", sumMax(arr, n));
	return 0;
}