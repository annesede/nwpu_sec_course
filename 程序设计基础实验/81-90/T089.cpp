// 游乐园
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int dist[12][12];
int n, m;
bool pass[12];
int ans = -1;

bool check(int v, int u) {
	return !pass[u] && dist[v][u] != 0x3f3f3f3f;
}

bool noway(int v) {
	for (int i = 0; i < n; ++i)
		if (check(v, i)) return false;
	return true;
}

void backTracking(int v, int l) {
	if (noway(v)) {
		ans = ans > l ? ans : l;
		return;
	}
	for (int i = 0; i < n; ++i) {
		if (check(v, i) && i != v) {
			pass[i] = true;
			backTracking(i, l + dist[v][i]);
			pass[i] = false;
		}
	}
}

int main() {
	scanf("%d %d", &n, &m);
	memset(dist, 0x3f, sizeof(dist));
	for (int i = 0; i < n; ++i) dist[i][i] = 0;
	while (m) {
		int v, u, l;
		scanf("%d %d %d", &v, &u, &l);
		v -= 1, u -= 1;
		dist[v][u] = dist[v][u] < l ? dist[v][u] : l;
		dist[u][v] = dist[v][u], --m;
	}

	for (int i = 0; i < n; ++i) {
		memset(pass, 0, sizeof(pass));
		pass[i] = true;
		backTracking(i, 0);
	}
	printf("%d\n", ans);
	return 0;
}