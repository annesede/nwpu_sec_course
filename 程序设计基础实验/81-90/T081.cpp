// 上楼梯
#include <stdio.h>
#include <string.h>

int main() {
	int n, m;
	scanf("%d %d", &n, &m);
	long long dp[n + 1];
	memset(dp, -1, (n + 1) * sizeof(long long));
	for (int i = 0; i < m; ++i) {
		int tmp;
		scanf("%d", &tmp);
		dp[tmp] = 0;
	}

	dp[1] = dp[1] ? 1 : 0;
	dp[2] = dp[2] ? (dp[1] ? 2 : 1) : 0;
	for (int i = 3; i <= n; ++i)
		if(dp[i]) dp[i] = (dp[i - 1] + dp [i - 2]) % (long long) (1e9 + 7);
	printf("%lld\n", dp[n]);
	return 0;
}