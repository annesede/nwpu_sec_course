// 绝对差
#include <stdio.h>
#include <limits.h>
#include <time.h>
#include <stdlib.h>

void quickSort(int arr[], int left, int right) {
	if (left >= right) return;
	srand(time(NULL));
	int idx = rand() % (left - right) + left;
	int flag = arr[idx], head = left - 1, tail = right + 1;
	while (head < tail) {
		do head++; while(arr[head] < flag);
		do tail--; while(arr[tail] > flag);
		if (head < tail) {
			int tmp = arr[head];
			arr[head] = arr[tail];
			arr[tail] = tmp;
		}
	}
	quickSort(arr, left, tail);
	quickSort(arr, tail + 1, right);
}

int minAbsSub(int arr[], int n) {
	int min = INT_MAX;
	for (int i = 0; i < n - 1; ++i) {
		int tmp = arr[i + 1] - arr[i];
		if (tmp < min) min = tmp;
	}
	return min;
}

int main() {
	int n;
	scanf("%d", &n);
	int arr[n];
	for (int i = 0; i < n; ++i) scanf("%d", &arr[i]);

	quickSort(arr, 0, n - 1);
	printf("%d", minAbsSub(arr, n));
	return 0;
}