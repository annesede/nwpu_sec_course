// 汤包
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

typedef struct {
	int guest;
	int end;
} Tag;

void quickSort(Tag* arr[], int left, int right) {
	if (left >= right) return;
	srand(time(NULL));
	int idx = rand() % (left - right) + left;
	int flag = arr[idx]->end, head = left - 1, tail = right + 1;
	while (head < tail) {
		do head++; while(arr[head]->end < flag);
		do tail--; while(arr[tail]->end > flag);
		if (head < tail) {
			Tag *tmp = arr[head];
			arr[head] = arr[tail];
			arr[tail] = tmp;
		}
	}
	quickSort(arr, left, tail);
	quickSort(arr, tail + 1, right);
}

void traverse(Tag *arr[], int n) {
	for (int i = 0; i < n; ++i)
		printf("%d ", arr[i]->guest);
}

int main() {
	int n;
	scanf("%d", &n);
	Tag *arr[n];
	for (int i = 0; i < n; ++i) {
		arr[i] = (Tag*)malloc(sizeof(Tag));
		arr[i]->guest = i + 1;
		int begin, duration;
		scanf("%d %d", &begin, &duration);
		arr[i]->end = begin + duration;
	}

	quickSort(arr, 0, n -1);
	traverse(arr, n);
	return 0;
}