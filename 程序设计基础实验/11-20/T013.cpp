// 分数的加、减、乘、除法
#include<stdio.h>

int gcd(int a,int b){
    if (b == 0) return a;
    else return gcd(b,a%b);
}

int main(){
    int aTop, aBot, bTop, bBot;
    scanf("%d",&aTop);
    getchar();
    scanf("%d",&aBot);
    getchar();
    scanf("%d",&bTop);
    getchar();
    scanf("%d",&bBot);
    getchar();

    int addTop, addBot, subTop, subBot, multTop, multBot, divTop, divBot;
    int addGcd, subGcd, multGcd, divGcd;

    addTop = aTop*bBot+aBot*bTop;
    addBot = aBot*bBot;
    addGcd = gcd(addTop,addBot);
    addTop /= addGcd;
    addBot /= addGcd;


    subTop = aTop*bBot-aBot*bTop;
    subBot = aBot*bBot;
    subGcd = gcd(subTop,subBot);
    subTop /= subGcd;
    subBot /= subGcd;

    multTop = aTop*bTop;
    multBot = aBot*bBot;
    multGcd = gcd(multTop,multBot);
    multTop /= multGcd;
    multBot /= multGcd;

    divTop = aTop*bBot;
    divBot = aBot*bTop;
    divGcd = gcd(divTop,divBot);
    divTop /= divGcd;
    divBot /= divGcd;

    printf("(%d/%d)+(%d/%d)=%d/%d\n",aTop,aBot,bTop,bBot,addTop,addBot);
    printf("(%d/%d)-(%d/%d)=%d/%d\n",aTop,aBot,bTop,bBot,subTop,subBot);
    printf("(%d/%d)*(%d/%d)=%d/%d\n",aTop,aBot,bTop,bBot,multTop,multBot);
    printf("(%d/%d)/(%d/%d)=%d/%d\n",aTop,aBot,bTop,bBot,divTop,divBot);

    return 0;
}
