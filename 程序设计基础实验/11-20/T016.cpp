// 比率
#include<stdio.h>
#include<math.h>

int gcd(int a,int b){
    if (b == 0) return a;
    else return gcd(b,a%b);
}

int cntDig(double x){
    int cnt = 0;
    while(x != floor(x)){
        ++cnt;
        x *= 10;
    }
    return cnt;
}

int main(){

    double x;
    scanf("%lf",&x);

    int cnt = cntDig(x), n, m, g;
    m = pow(10,cnt);
    n = (int)(x*m);
    g = gcd(m,n);
    m /= g;
    n /= g;

    printf("%d/%d",n,m);
    return 0;
}