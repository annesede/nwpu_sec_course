// 方阵
#include<stdio.h>

int main(){
    int n,m;
    scanf("%d",&n);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            m = (i-j) > 0 ? (i-j) : (j-i);
            printf("%d  ",m);
        }
        printf("\n");
    }

    return 0;
}