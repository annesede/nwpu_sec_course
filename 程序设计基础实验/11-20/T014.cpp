// 操作数
#include<stdio.h>

int sumDig(int n){
    int sum = 0;
    while(n){
        sum += n%10;
        n /= 10;
    }
    return sum;

}

int main(){
    int n,cnt=0;
    scanf("%d",&n);

    while(n){
        n -= sumDig(n);
        ++cnt;
    }

    printf("%d",cnt);

    return 0;
}
