// 对称数
#include <stdio.h>

int main(){

    int n;
    scanf("%d",&n);

    int a = (n/100)%10;
    int b = (n/10)%10;
    int c = n%10;

    if ((b == 0 || b == 1 || b == 8) && ((a == 6 && c == 9) || (a == 9 && c == 6) || ((a == 0 || a == 1 || a == 8) && (c == 0 || c == 1 || c == 8)))){
        printf("Yes\n");
    } else {
        printf("No\n");
    }

    return 0;
}