// 级数和
#include<stdio.h>
#include<math.h>

double decimal (int n){
    double m = (double)n+1;
    while(floor(m)){
        m /= 10;
    }
    return m+n;
}

int main(){
    int n;
    scanf("%d",&n);
    double sum = 0.0f, term;
    for (int i = 1; i < n; ++i) {
        term = decimal(i);
        sum += term;
        printf("%g+",term);
    }
    term = decimal(n);
    sum += term;
    printf("%g=%g",term,sum);

    return 0;
}