// 倍数和
#include <stdio.h>

int main(){
    unsigned int t = 0;
    unsigned int arrn[100000];
    scanf("%u", &t);
    for(unsigned int i = 0; i < t; i++){
        scanf("%u", &arrn[i]);
    }

    for(unsigned int j = 0; j < t; j++){
        unsigned int res = 0;
        for(unsigned int x = 1; x < arrn[j]; x++){
            if(x % 3 == 0 || x % 5 == 0){
                res += x;
            }
        }
        printf("%u\n", res);
    }

    return 0;
}