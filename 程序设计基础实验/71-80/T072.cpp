// 热能计算
#include <stdio.h>

int main() {
	int Ti, Tf, cL, cV;
	double mL, mV;
	scanf("%d %d %lf %d %lf %d", &Ti, &Tf, &mL, &cL, &mV, &cV);
	double QL = cL * mL * (Tf - Ti);
	double QV = cV * mV * (Tf - Ti);
	double Q = QL + QV;
	printf("%.2lfkJ,%.2lf%%,%.2lf%%\n", Q / 1000, QV / Q, QL / Q);
	return 0;
}

/*
20 80
0.250 4186
0.500 900
*/