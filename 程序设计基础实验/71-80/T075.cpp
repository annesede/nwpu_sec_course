// 中位数
#include <stdio.h>

int arr[1000];

double mid(int n) {
	if (n & 1) return arr[n / 2];
	return (arr[n / 2] + arr[n / 2 - 1]) / 2.0f;
}

int main() {
	int flag, cnt = 0;
	while(1) {
		scanf("%d", &flag);
		if (flag == -1) break;
		while (1) {
			if (flag == 0) {
				for (int i = 0; i < cnt; ++i) printf("%d ", arr[i]);
				printf("%.6lf\n", mid(cnt));
				break;
			}
			arr[cnt++] = flag;
			scanf("%d", &flag);
		}
	}
	return 0;
}