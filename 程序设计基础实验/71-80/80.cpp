// 水下声学定位
#include<stdio.h>
#include<math.h>

int main()
{
	const double pi = 3.1415926f;
	double a, b, c, d, diag;
	scanf("%lf %lf %lf %lf %lf", &a, &b, &c, &d, &diag);
	double p = (a + b + diag) / 2, q = (c + d + diag);
	double s = sqrt(p * (p - a) * (p - b) * (p - diag)) + sqrt(q * (q - c) * (q - d) * (q - diag));
	double angle = atan((4 * s) / (pow(b, 2) + pow(d, 2) - pow(a, 2) - pow(c, 2)));
	angle *= 180 / pi;
	printf("%.6lf %.1lf", s, angle);
}