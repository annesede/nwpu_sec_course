// 原子计数
#include <iostream>
#include <map>
using namespace std;
 
string s;
 
int getnum(int x)
{
    int res = 0;
    for (; s[x]>='0' && s[x]<='9' && s[x]; x ++)
        res = res * 10 + s[x] - '0';
    return res + !res;
}
 
int main()
{
    getline(cin, s);
    map<string, int> mp;
    for (int i = 0; s[i]; i ++)
    {
        if (!(s[i]>='A' && s[i]<='Z')) continue;
        string ele = "";
        ele += s[i];
        
        if (s[i+1]>='a' && s[i+1]<='z')
        {
            ele += s[i+1];
            mp[ele] += getnum(i+2);
        }
        else 
            mp[ele] += getnum(i+1);
    }
 
    for (auto& p : mp)
        cout << p.first << " " << p.second << endl;
        
    return 0;
}