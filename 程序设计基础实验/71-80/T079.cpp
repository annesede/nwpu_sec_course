// 火箭发射模拟
#include <iostream>
#include <iomanip>
 
const double timeStep = 0.1;
 
int main()
{
    double totalMass, rocketMass, burnTime, cE, g;
    std::cin >> totalMass >> rocketMass >> burnTime >> cE >> g;
    double propellantMass = totalMass - rocketMass;
    double massFlow = propellantMass / burnTime;
    double T = massFlow * cE;
    double altitude = 0, V = 0;
    double timeLeft = burnTime + timeStep;
    while(timeLeft >= 0)
    {
        double a = T / totalMass;
        double deltaV = a * timeStep;
        double deltaAltitude = V * timeStep;
        double deltaM = massFlow * timeStep;
        V += deltaV;
        altitude += deltaAltitude;
        totalMass -= deltaM;
        timeLeft -= timeStep;
    }
    std::cout << std::fixed << std::setprecision(3) << altitude / 1000 << "km" << std::endl;
    return 0;
}