// 几何约束
#include <stdio.h>
#include <stdbool.h>

int maxi(int a, int b) {
	return a > b ? a : b;
}

int mini(int a, int b) {
	return a < b ? a : b;
}

int cross(int x1, int y1, int x2, int y2) {
	return x1 * y2 - y1 * x2;
}

bool insert(int line1[4], int line2[4]) {
	if (maxi(line2[0], line2[2]) < mini(line1[0], line1[2]) ||
	maxi(line1[0],line1[2]) < mini(line2[0], line2[2]) ||
	maxi(line2[1], line2[3]) < mini(line1[1], line1[3]) ||
	 maxi(line1[1], line2[3]) < mini(line2[1],line2[3])) return false;
	if (cross(line1[2] - line1[0], line1[3] - line1[1], line2[0] - line1[0], line2[1] - line1[1]) *
		cross(line1[2] - line1[0], line1[3] - line1[1], line2[2] - line1[0], line2[3] - line1[1]) > 0 ||
		cross(line2[2] - line2[0], line2[3] - line2[1], line1[0] - line2[0], line1[1] - line2[1]) *
			cross(line2[2] - line2[0], line2[3] - line2[1], line1[2] - line2[0], line1[3] - line2[1]) > 0) return false;
	return true;
}

int main() {
	int n;
	scanf("%d", &n);
	int line[n][4];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < 4; ++j) scanf("%d", &line[i][j]);

	int cnt = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = i + 1; j < n; ++j) {
			if (insert(line[i], line[j])) {
				printf("X: #%d #%d\n", i + 1, j + 1);
				++cnt;
			}
		}
	}
	printf("n=%d\n", cnt);
	return 0;
}

/*
5
1 5 4 5
2 5 10 1
3 2 10 3
6 4 9 4
7 1 8 1
*/