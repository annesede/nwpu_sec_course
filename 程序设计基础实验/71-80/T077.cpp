// 成绩单
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct tag {
	long long id;
	char name[31];
	int score;
} ST;

void load(ST* arr[], int n) {
	for (int i = 0; i < n; ++i) {
		arr[i] = (ST*) malloc(sizeof(ST));
		scanf("%lld %s %d", &arr[i]->id, arr[i]->name, &arr[i]->score);
	}
}

void sort(ST* arr[], int n) {
	ST* tmp = NULL;
	for (int i = 0; i < n; ++i) {
		bool isSwapped = false;
		for (int j = 0; j < n - 1 - i; ++j)
			if ((arr[j]->score < arr[j + 1]->score) ||
				(arr[j]->score == arr[j + 1]->score && arr[j]->id > arr[j + 1]->id))
				tmp = arr[j], arr[j] = arr[j + 1], arr[j + 1] = tmp, isSwapped = true;
		if (!isSwapped) break;
	}
}

void traverse(ST* arr[], int n) {
	for (int i = 0; i < n; ++i)
		printf("%lld %s %d\n", arr[i]->id, arr[i]->name, arr[i]->score);
}

int main() {
	int n;
	scanf("%d", &n);
	ST* grade[n];
	load(grade, n);
	sort(grade, n);
	traverse(grade, n);
	return 0;
}

/*
6
2001900001 Jerry 88
2001900005 Tom 92
2001900006 Milla 85
2001900002 Alice 80
2001900003 Mickey 85
2001900004 Aladdin 83
*/