// 波士顿房价预测
#include <stdio.h>

double nAvg(double arr[], int n) {
    double sum = 0;
    for (int i = 0; i < n; ++i) {
        sum += arr[i];
    }
    return sum / n;
}

int main() {
    int n;
    scanf("%d", &n);
    double x[n], y[n];
    for (int i = 0; i < n; ++i) {
        scanf("%lf %lf", &x[i], &y[i]);
    }

    double xBar = nAvg(x, n), yBar = nAvg(y, n);
    double sumUp = 0, sumDown = 0;
    for (int i = 0; i < n; ++i) {
        sumUp += (x[i] - xBar) * (y[i] - yBar);
    }
    for (int i = 0; i < n; ++i) {
        sumDown += (x[i] - xBar) * (x[i] - xBar);
    }
    double b = sumUp / sumDown;
    double a = yBar - b * xBar;

    printf("Y=%.4lf+%.4lf*X",a,b);
    return 0;
}