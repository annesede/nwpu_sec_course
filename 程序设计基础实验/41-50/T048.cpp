// 行列式值
#include <stdio.h>

#define MAX_SIZE 10

void swapRows(double matrix[MAX_SIZE][MAX_SIZE], int row1, int row2, int n) {
    for (int i = 0; i < n; i++) {
        double temp = matrix[row1][i];
        matrix[row1][i] = matrix[row2][i];
        matrix[row2][i] = temp;
    }
}

double calculateDeterminant(double matrix[MAX_SIZE][MAX_SIZE], int n) {
    int i, j, k;
    double determinant = 1.0;

    for (i = 0; i < n; i++) {
        if (matrix[i][i] == 0.0) {
            for (j = i + 1; j < n; j++) {
                if (matrix[j][i] != 0.0) {
                    swapRows(matrix, i, j, n);
                    determinant *= -1.0;
                    break;
                }
            }
        }

        if (matrix[i][i] == 0.0) {
            return 0.0;
        }

        double pivot = matrix[i][i];
        determinant *= pivot;
        for (j = i; j < n; j++) {
            matrix[i][j] /= pivot;
        }

        for (j = i + 1; j < n; j++) {
            double factor = matrix[j][i];
            for (k = i; k < n; k++) {
                matrix[j][k] -= factor * matrix[i][k];
            }
        }
    }

    return determinant;
}

int main() {
    int n;
    scanf("%d", &n);

    double matrix[MAX_SIZE][MAX_SIZE];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            scanf("%lf", &matrix[i][j]);
        }
    }

    double determinant = calculateDeterminant(matrix, n);
    printf("%.0lf\n", determinant);

    return 0;
}

/* 递归实现
#include <stdio.h>
#include <stdlib.h>

int** init(int n) {
	int** matrix = (int**)malloc(sizeof(int*) * (n + 1));
	for(int i = 0; i <= n; i++) {
		matrix[i] = (int*)malloc(sizeof(int) * (n + 1));
	}
	return matrix;
}

int det(int **matrix, int n) {
	if (n == 1) return matrix[1][1];
	int sum = 0;
	int **subMatrix = init(n - 1);
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= n - 1; ++j) {
			for (int k = 1; k <= n - 1; ++k) {
				if (k < i) subMatrix[j][k] = matrix[j + 1][k];
				else subMatrix[j][k] = matrix[j + 1][k + 1];
			}
		}
		int sgn = i % 2 == 0 ? -1 : 1;
		sum += sgn * matrix[1][i] * det(subMatrix, n - 1);
	}
	return sum;
}

int main() {
	int n;
	scanf("%d", &n);
	int **matrix = init(n);
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= n; ++j) {
			scanf("%d", &matrix[i][j]);
		}
	}
	printf("%d", det(matrix, n));
	return 0;
}*/