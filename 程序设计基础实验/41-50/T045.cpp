// 回文数之和
#include <stdio.h>
#include <stdbool.h>

int dec[10] = {0}, kSys[32] = {0};

bool isPalindrome(int arr[], int cnt){
    int head = 0, tail = cnt - 1;
    while (head < tail) {
        if (arr[head] != arr[tail]) return false;
        ++head, --tail;
    }
    return true;
}

bool isBiPalindrome(int n, int k){
    int tmp = n, cnt = 0;
    while (tmp) {
        dec[cnt++] = tmp % 10;
        tmp /= 10;
    }
    if (!isPalindrome(dec, cnt)) return false;

    tmp = n, cnt = 0;
    while (tmp) {
        kSys[cnt++] = tmp % k;
        tmp /= k;
    }
    if (!isPalindrome(kSys, cnt)) return false;
    return true;
}

int main() {
    int n, k, sum = 0;
    scanf("%d %d", &n, &k);
    for (int i = 1; i <= n; ++i) {
        if (isBiPalindrome(i, k)) sum += i;
    }
    printf("%d", sum);
    return 0;
}