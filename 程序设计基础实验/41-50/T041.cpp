// 航空旅行
#include<stdio.h>
#include<stdbool.h>

void pass(int a, int b, int c, int d, int e){
    bool flag = false;
    if (a <= e && (b + c) <= d) flag = true;
    if (b <= e && (a + c) <= d) flag = true;
    if (c <= e && (a + b) <= d) flag = true;
    if (flag) printf("YES\n");
    else printf("NO\n");
}

int main(){
    int n, a, b, c, d, e;
    scanf("%d",&n);
    while (n--){
        scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);
        pass(a, b, c, d, e);
    }
    return 0;
}