// 稀疏矩阵
#include <stdio.h>

int main () {
    int raw, col, n, num = 0;
    scanf("%d %d", &raw, &col);
    for (int i = 0; i < raw; ++i) {
        for (int j = 0; j < col; ++j) {
            scanf("%d", &n);
            if (n) ++num;
        }
    }
    double ratio = (double)num / (raw * col);
    if (num == raw || num == col || (ratio - 0.05) <= 1e-9)
        printf("Yes\n");
    else printf("No\n");
    return 0;
}
