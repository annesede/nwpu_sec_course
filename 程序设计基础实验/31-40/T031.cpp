// 哈沙德数
#include <stdio.h>

int HarshadNumber(int n){
    int t = n, s = 0;
    while (t) {
        s += t%10;
        t /= 10;
    }
    if ((s == 0) || (n%s != 0)) return 0;
    if (s == 1) return 1;
    return n/s;
}

int main(){
    int cnt = 0, n;
    scanf("%d",&n);
    if (n == 1) cnt = 1;
    while ((n != 0) && (n != 1)) {
        n = HarshadNumber(n);
        if (n) ++cnt;
    }
    printf("%d",cnt);
    return 0;
}