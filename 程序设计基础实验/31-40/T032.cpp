// 素数
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

typedef unsigned long long uint64;

uint64 primeNum(uint64 a, uint64 b){
    bool isPrime[b+1];
    memset(isPrime,1,b+1);
    uint64 cnt = 0;
    for (uint64 i = 2; i <= b; ++i){
        if (isPrime[i]){
            for (uint64 j = 2; j*i <= b; ++j){
                isPrime[j*i] = false;
            }
        }
    }
    for (uint64 i = a; i <= b; ++i){
        cnt += isPrime[i];
    }
    return cnt;
}

int main(){
    uint64 a,b,num;
    scanf("%llu %llu",&a,&b);
    num = primeNum(a,b);
    printf("%llu",num);
    return 0;
}