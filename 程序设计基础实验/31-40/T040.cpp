// 可变参数平均
#include <stdio.h>
#include <stdarg.h>

double avg(int num,...){
    va_list vaList;
    double sum = 0.0f;
    va_start(vaList,num);
    for (int i = 0; i < num; ++i) {
        sum += va_arg(vaList,int);
    }
    va_end(vaList);
    return sum/num;
}

int main() {
    int a,b,c,d,e;
    scanf("%d %d %d %d %d",&a,&b,&c,&d,&e);
    double avgMinus = avg(2,a,b) - avg(3,c,d,e);
    printf("%.4f",avgMinus);
    return 0;
}