// 可变参数累加
#include <stdio.h>
#include <stdarg.h>

int sum(int start,...){
    va_list vaList;
    int sum = 0;
    int curr = start;
    va_start(vaList,start);
    while (start){
        sum += start;
        start = va_arg(vaList,int);
    }
    va_end(vaList);
    return sum;
}

int main() {
    int a,b,c,d,e,f;
    scanf("%d %d %d %d %d %d",&a,&b,&c,&d,&e,&f);
    int sumMinus = sum(a,b,0) - sum(c,d,e,f,0);
    printf("%d",sumMinus);
    return 0;
}