// 二进制表示
#include <stdio.h>
#include <stdbool.h>

void binary(int a){
    bool flag = false;
    for (int i = 15; i >= 0 ; --i) {
        if ((a>>i)&1) {
            if (flag) printf("+");
            if (i >= 2){
                printf("2(");
                binary(i);
                printf(")");
            }
            if (i == 1) printf("2");
            if (i == 0) printf("2(0)");
            flag = true;
        }
    }
}

int main() {
    int a;
    scanf("%d",&a);
    binary(a);
    return 0;
}