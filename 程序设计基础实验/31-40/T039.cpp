// 运动会
#include <stdio.h>
#include <stdbool.h>

int phiEuler(int n){
    int phi[n+1],prime[n+1];
    bool isSieved[n+1];
    int sum = 0,cnt = 1, comp;
    prime[0] = 1;
    phi[1] = 1;
    for (int i = 2; i < n; ++i){
        if (!isSieved[i]){
            prime[cnt++] = i;
            phi[i] = i-1;
        }
        for (int j = 1; i*prime[j] <= n; ++j){
            comp = i*prime[j];
            isSieved[comp] = true;
            if (i%prime[j] == 0){
                phi[comp] = prime[j]*phi[i];
                break;
            } else{
                phi[comp] = (prime[j]-1)*phi[i];
            }
        }
    }
    for (int i = 1; i <= n-1; ++i) {
        sum += phi[i];
    }
    return sum;
}

int main() {
    int n, num;
    scanf("%d",&n);
    num = n == 1 ? 0 : (2*phiEuler(n)+1);
    printf("%d",num);
    return 0;
}