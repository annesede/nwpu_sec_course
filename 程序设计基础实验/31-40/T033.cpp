// 基思数
#include <stdio.h>

int arr[8] = {0};

int init(int n){
    int cnt = 0;
    while (n) {
        arr[cnt++] = n%10;
        n /= 10;
    }
    return cnt;
}

void isKeith(int n, int len){
    int i = len - 1;
    while (arr[i] < n){
        int sum = 0;
        for (int j = 0; j < len; ++j) {
            sum += arr[(i-j+len)%len];
        }
        arr[i] = sum;
        i = (i-1+len)%len;
    }
    if (arr[i] == n) printf("Yes");
    else printf("No");
}

int main() {
    int n;
    scanf("%d",&n);
    isKeith(n,init(n));
    return 0;
}