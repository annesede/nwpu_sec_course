// 光线追踪
#include <stdio.h>

unsigned int gcd(unsigned int a, unsigned int b){
    if (b == 0) return a;
    return gcd(b,a%b);
}

int main(){
    unsigned int n,x,l;
    scanf("%u %u",&n,&x);
    l = 3*(n-gcd(n,x));
    printf("%u",l);
    return 0;
}