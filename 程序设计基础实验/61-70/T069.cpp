// PID控制
#include <stdio.h>

typedef struct PIDController {
	double Kp, Ki, Kd;
	double preError, integral;
} PIDData;

double PIDCalculate(PIDData *pid, double setPoint, double measuredValue) {
	double error = setPoint - measuredValue;
	pid->integral += error;
	double differential = error - pid->preError;
	double output = pid->Kp * error + pid->Ki * pid->integral + pid->Kd * differential;
	pid->preError = error;
	return output;
}

int main() {
	double setPoint, measuredValue;
	int time;
	PIDData pid = {0};
	scanf("%lf %lf %lf", &pid.Kp, &pid.Ki, &pid.Kd);
	scanf("%lf %lf %d", &setPoint, &measuredValue, &time);
	for (int i = 1; i <= time; ++i) {
		double output = PIDCalculate(&pid, setPoint, measuredValue);
		measuredValue += output;
		printf("%d %.6lf\n", i, measuredValue);
	}
	return 0;
}