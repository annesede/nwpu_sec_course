// 加密字串
#include <stdio.h>

static int freq[26] = {0};

int main() {
	char plain[8000] = "";
	int x;
	scanf("%s %d", plain, &x);
	for (int i = 0; plain[i]; ++i) ++freq[plain[i] - 'a'];
	char cipher[8000] = "";
	for (int i = 0; plain[i]; ++i) {
		if (freq[plain[i] - 'a'] & 1)
			cipher[i] = (char) (((plain[i] - 'a' - x) % 26 + 26) % 26 + 'a');
		else
			cipher[i] = (char) ((plain[i] - 'a' + x) % 26 + 'a');
	}
	puts(cipher);
	return 0;
}
