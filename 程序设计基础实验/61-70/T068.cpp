// DNA双螺旋结构
#include <stdio.h>

void putsDna1(){
	printf("   AT   \n");
	printf("  T--A  \n");
	printf(" A----T \n");
	printf("T------A\n");
	printf("T------A\n");
	printf(" G----C \n");
	printf("  T--A  \n");
	printf("   GC   \n");
}

void putsDna2(){
	printf("   CG   \n");
	printf("  C--G  \n");
	printf(" A----T \n");
	printf("A------T\n");
	printf("T------A\n");
	printf(" A----T \n");
	printf("  A--T  \n");
	printf("   GC   \n");
}

void putsDna3(){
	printf("   AT   \n");
	printf("  C--G  \n");
	printf(" T----A \n");
	printf("C------G\n");
	printf("C------G\n");
	printf(" T----A \n");
	printf("  G--C  \n");
	printf("   AT   \n");
}

int main() {
	int n;
	scanf("%d", &n);
	for (int i = 1; i <= n/2; ++i) {
		if (i % 3 == 1) putsDna1();
		else if (i % 3 == 2) putsDna2();
		else putsDna3();
	}
	return 0;
}