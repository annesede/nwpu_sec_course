// 三元搜索
#include <stdio.h>

int terSearch(int arr[], int n, int k) {
	int left = 0, right = n - 1, mid1 = (n - 1) / 3, mid2 = n - mid1;
	while(mid1 != mid2) {
		if (k > arr[right] || k < arr[left]) return -1;
		if (k == arr[mid1]) return mid1;
		if (k == arr[mid2]) return mid2;
		if (mid1 == mid2) break;
		if (k < arr[mid1]) right = mid1 - 1;
		else if (k > arr[mid2]) left = mid2 + 1;
		else left = mid1 + 1, right = mid2 - 1;
		mid1 = left + (right - left) / 3, mid2 = right - (right - left) / 3;
	}
	return -1;
}

int main() {
	int n, k;
	scanf("%d", &n);
	int arr[n];
	for (int i = 0; i < n; ++i) scanf("%d", &arr[i]);
	scanf("%d", &k);
	printf("%d in [%d]", k, terSearch(arr, n, k));
	return 0;
}