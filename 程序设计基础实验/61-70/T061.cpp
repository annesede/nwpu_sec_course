// 时钟A-B
#include <stdio.h>
#include <time.h>

int main(){
	struct tm begin = {0}, end = {0};
	scanf("%d %d %d", &begin.tm_year, &begin.tm_mon, &begin.tm_mday);
	scanf("%d %d %d", &end.tm_year, &end.tm_mon, &end.tm_mday);

	begin.tm_year -= 1900, begin.tm_mon -= 1;
	end.tm_year -= 1900, end.tm_mon -= 1;

	time_t tmBegin =  mktime(&begin);
	time_t tmEnd = mktime(&end);
	printf("%.6lf", difftime(tmBegin, tmEnd));
	return 0;
}