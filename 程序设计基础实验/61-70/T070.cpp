// 循环排序
#include <stdio.h>

void swap(int *a, int *b) {
	int tmp = *a;
	*a = *b, *b = tmp;
}

void cycleSort(int arr[], int n) {
	for (int i = 0; i < n - 1; ++i) {
		int item = arr[i], pos = i;
		for (int j = i + 1; j < n; ++j) if (arr[j] < item) ++pos;
		if (pos == i) continue;

		swap(&arr[pos], &item);
		while(pos != i) {
			pos = i;
			for (int j = i + 1; j < n; ++j) if (arr[j] < item) ++pos;
			while (item == arr[pos]) ++pos;
			swap(&arr[pos], &item);
		}
	}
}

int main() {
	int n;
	scanf("%d", &n);
	int arr[n];
	for (int i = 0; i < n; ++i) scanf("%d", &arr[i]);
	cycleSort(arr, n);
	for (int i = 0; i < n; ++i) printf("%d ", arr[i]);
	return 0;
}