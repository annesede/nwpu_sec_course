// 有效表达式
#include <stdio.h>

int main() {
	long long n;
	scanf("%lld", &n);
	long long cnt = 1;
	for (long long i = n + 2; i <= 2 * n; ++i) cnt *= i;
	for (long long i = 1; i <= n; ++i) cnt /= i;
	printf("%lld", cnt);
	return 0;
}