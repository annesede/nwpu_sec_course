// Arduino显示
#include <stdio.h>

static const int digit[10] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};

int getUnit(int num) {
	int cnt = 0;
	do {
		cnt += digit[num % 10];
		num /= 10;
	} while (num);
	return cnt;
}

int main() {
	int n;
	scanf("%d", &n);
	n -= 4;
	if (n <= 0) printf("0");
	else {
		int cnt = 0;
		for (int i = 0; i <= 1111; ++i) {
			for (int j = 0; j <= 1111; ++j) {
				if (getUnit(i) + getUnit(j) + getUnit(i + j) == n) ++cnt;
			}
		}
		printf("%d", cnt);
	}
	return 0;
}