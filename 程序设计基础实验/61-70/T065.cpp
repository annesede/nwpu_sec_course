// 长安
#include <stdio.h>

int bx, by, px, py, cnt;

void dfs(int x, int y) {
	if ((x == px && y == py) || x > bx || y > by) return;
	if (x == bx && y == by) {
		++cnt;
		return;
	}
	dfs(x + 1, y);
	dfs(x, y + 1);
}

int main() {
	while (1) {
		fflush(stdin);
		scanf("%d %d %d %d", &bx, &by, &px, &py);
		if (bx <= 0 || by <= 0 || px <= 0 || py <= 0) break;
		cnt = 0;
		dfs(1, 1);
		printf("%d\n", cnt);
	}
	return 0;
}