
软件：Protues 8专业版。
信号标识：蓝色 - 低电平；红色 - 高电平；黄色 - 信号冲突；灰色 - 无信号。
常用编辑模式：元件模式，连线标号模式，总线模式，终端模式。
终端模式：INPUT，OUTPUT，POWER，GROUND。

# Lab1 运算器实验

元件：7SEG-BCD-GRN (数码管)，LED-GREEN，LED-RED，LED-YELLOW，4078 (8位输入或门)，74LS04 (非门)，74LS194 (双向移位寄存器)，74LS273 (数据/地址锁存器)，74LS244 (三态门), 74LS181 (4位运算功能发生器)，SW-SPDT (单刀双掷开关)，DIPSWC_8 (8位DIP拨码开关)，RESPACK-8 (8位排阻)。

实验步骤：
① $\rm\overline{ALU\_OE}=1$，$\rm\overline{SW\_OE}=0$，$\rm ALU\_EN=1$，开始仿真；
② 拨码写入8位数字，拨动 $\rm\overline{DRA\_CLK}$ 写入A寄存器；重新拨码再拨动 $\rm\overline{DRB\_CLK}$ 写入B寄存器；
③ 调整 $\rm M,S_1,S_2,S_3,S_4$ 开关位置来选择 ALU 工作模式 (详见 74LS181 功能表)；
④ $\rm\overline{ALU\_OE}=0$，$\rm\overline{SW\_OE}=1$，$\rm ALU\_EN=0$，记录数码显示 (运算结果) 和LED情况 (标志寄存器)；
⑤ 由于标志寄存器前存在锁存器，在调整到下一个工作模式后需要重启运算来更新标志。

# Lab2 存储器实验

元件：7SEG-BCD，7SEG-BCD-GRN；74LS04（非门），74LS08（与门），74LS32（或门）；74LS138（3-8译码器），74LS244，74LS273；2764（ROM）；6116（SRAM）；DIPSWC_8，RESPACK-8；SW-SPDT。

实验步骤：
① 编写80C31汇编并编译，烧录数据到ROM；
② $\rm\overline{RAM\_OE}=\overline{RAM\_WE}=1$，$\rm\overline{ROM\_OE}=\overline{SW\_BUS}=0$，开始仿真；
③拨码写入12位内存地址，拨动 $\rm\overline{ROM\_CLK}$ 将地址传给ROM，此时红色数码显示内存位置的数据值；
③拨码写入另一个12位内存地址，拨动 $\rm\overline{RAM\_CLK}$ 将地址传给RAM，$\rm\overline{RAM\_WE}=0$ 写入数据，而后 $\rm\overline{RAM\_WE}=1$ 结束写入。
④暂停仿真，在调试中可查看ROM和RAM中的数据。

地址空间范围计算：存储器接入地址线决定了寻址大小；而后，可由3-8译码器接入逻辑倒推存储器工作时（即CE端输入为0时）高位地址固定情况（即片选）。

e.g. ROM接入9条地址线，寻址大小为 $2^9$ bits；$\rm\overline{CE}=0$ 时要求3-8译码器输出 $\rm Y_0=0\vee Y_1=1$ ，即输入端 $\rm E_1=E_2=E_3=Q_3=0\wedge C=Q_2=0\wedge B=Q_1=0$ ；故得到有效地址为 0B000XXXXXXXXX（X为1或0），即地址空间范围为 0x000~0x1FF。

![3-8 译码器逻辑表](../img/107CC95ABF9ED5C6.png)

# Lab3 微程序控制器实验
无电路图；依照指令执行过程及数据通路设计即可。

