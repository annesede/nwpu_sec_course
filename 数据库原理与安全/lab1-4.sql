USE 学生成绩管理
GO

-- 创建表
CREATE TABLE Student
(
  Sno CHAR(5) PRIMARY KEY,
  Sname VARCHAR(8) NOT NULL,
  Ssex BIT NOT NULL,
  Sage SMALLINT CHECK (Sage >= 15 AND Sage <= 30) NOT NULL,
  Sbirthday SMALLDATETIME,
  Sdept VARCHAR(30) NOT NULL,
  Sbirthplace VARCHAR(50),
  Sgpa NUMERIC(2,1) CHECK(Sgpa >= 0 AND Sgpa <= 4) DEFAULT '0'
)

CREATE TABLE Course
(
  Cno CHAR(2) PRIMARY KEY,
  Cname VARCHAR(30) NOT NULL,
  Cpno CHAR(2),
  Ccredit NUMERIC(2,1) NOT NULL,
  Cweek TINYINT,
  Croom CHAR(8)
)

CREATE TABLE Report
(
  Sno CHAR(5) FOREIGN KEY REFERENCES Student(Sno) ON DELETE CASCADE ON UPDATE CASCADE,
  Cno CHAR(2) FOREIGN KEY REFERENCES Course(Cno) ON DELETE NO ACTION,
  Score DECIMAL(4,1) CHECK(Score >=0 AND Score <= 100) DEFAULT '0'
)

-- 修改表结构
CREATE TABLE test
(
  Tno CHAR(8) PRIMARY KEY,
  Tnum INT,
  Tscore NUMERIC(2,1)
)

ALTER TABLE test
  ADD Tnam VARCHAR(20) UNIQUE NOT NULL

ALTER TABLE test
  DROP COLUMN Tnum

ALTER TABLE test
  ALTER COLUMN Tscore FLOAT

EXEC sp_rename 'test.Tscore', 'Tn', 'column' 

DROP TABLE test

-- 输入数据
INSERT INTO Student(Sno, Sname, Ssex, Sage, Sdept)
VALUES
  ('23001', '张三', 1, 18, '网络安全'),
  ('23002', '张思', 0, 19, '信息安全'),
  ('23003', '李四', 1, 18, '网络安全'),
  ('23004', '李武', 1, 17, '计算机科学'),
  ('23005', '李六', 0, 19, '信息安全'),
  ('23006', '张贰', 0, 19, '网络安全'),
  ('23007', '张毅', 1, 22, '软件设计'),
  ('23008', '李戚', 0, 20, '软件设计'),
  ('23009', '李琦', 1, 20, '计算机科学'),
  ('23010', '李明', 1, 19, '计算机科学'),
  ('23011', '王小明', 1, 18, '网络安全'),
  ('23012', '张小红', 0, 19, '信息安全'),
  ('23013', '李小明', 1, 18, '网络安全'),
  ('23014', '李平安', 1, 17, '计算机科学'),
  ('23015', '李平安', 0, 19, '信息安全'),
  ('23016', '张平安', 0, 19, '网络安全'),
  ('23017', '张小明', 1, 22, '软件设计'),
  ('23018', '李小红', 0, 20, '软件设计'),
  ('23019', '李小明', 1, 20, '计算机科学'),
  ('23020', '李明', 1, 19, '计算机科学')

INSERT INTO Course
VALUES
  ('01', '高等数学（上）', NULL, 4.0, 18, 'JXA105'),
  ('02', '高等数学（下）', '01', 5.0, 18, 'JXA103'),
  ('03', '线性代数', NULL, 3.0, 16, 'JXD101'),
  ('04', '概率论与数理统计', '02', 4.0, 16, 'JXB204'),
  ('05', '离散数学', NULL, 2.0, 12, 'JXD105'),
  ('06', '信息安全数学基础', '05', 2.0, 12, 'JDB203'),
  ('07', '复变函数与积分变换', '02', 2.0, 12, 'JDD304'),
  ('08', '密码学', '06', 5.5, 18, 'LAB206'),
  ('11', '程序设计基础', NULL, 3.0, 15, 'LAB301'),
  ('12', '数据结构', '11', 3.0, 15, 'LAB203'),
  ('13', '算法设计', '12', 2.0, 10, 'LAB303'),
  ('14', '计算机系统基础', '11', 4.0, 18, 'LAB101'),
  ('15', '数字逻辑电路', NULL, 3.0, 12, 'LAB405'),
  ('16', '操作系统', '14', 4.0, 17, 'LAB105'),
  ('17', '计算机网络', '14', 4.0, 17, 'LAB201'),
  ('18', '计算机组成原理', '14', 5.0, 18, 'LAB106'),
  ('19', '编译原理', '14', 3.0, 16, 'LAB103'),
  ('1A', '汇编与接口', '11', 2.0, 12, 'LAB202'),
  ('1B', '数据库', NULL, 3.0, 15, 'LAB104'),
  ('1C', '通信原理', 07, 3.0, 15, 'LAB105')

INSERT INTO Report
VALUES
  ('23001', '01', 90.0),
  ('23001', '02', 88.0),
  ('23001', '11', 94.0),
  ('23001', '1A', 67.0),
  ('23002', '01', 92.0),
  ('23002', '03', 96.0),
  ('23002', '02', 85.0),
  ('23002', '11', 62.0),
  ('23002', '12', 98.0),
  ('23002', '13', 99.0),
  ('23003', '01', 90.0),
  ('23004', '02', 88.0),
  ('23010', '11', 94.0),
  ('23005', '1A', 67.0),
  ('23006', '01', 92.0),
  ('23007', '03', 96.0),
  ('23011', '02', 85.0),
  ('23012', '11', 62.0),
  ('23020', '12', 98.0),
  ('23017', '13', 99.0),
  ('23013', '01', 90.0),
  ('23015', '02', 88.0),
  ('23016', '11', 94.0),
  ('23014', '1A', 67.0),
  ('23014', '01', 92.0),
  ('23013', '03', 96.0),
  ('23011', '02', 85.0),
  ('23014', '11', 62.0),
  ('23019', '12', 98.0),
  ('23020', '13', 99.0),
  ('23005', '01', 90.0),
  ('23007', '02', 88.0),
  ('23008', '11', 94.0),
  ('23009', '1A', 67.0),
  ('23009', '01', 92.0),
  ('23004', '03', 96.0),
  ('23003', '02', 85.0),
  ('23007', '11', 62.0),
  ('23007', '12', 98.0),
  ('23019', '13', 99.0)

-- 表复制
SELECT * INTO StudentData from Student
SELECT * INTO StudentStruct from Student WHERE 1=2

-- 查询
SELECT Cno
  FROM Course
UNION
SELECT Cno
  FROM Report

SELECT Cno
  FROM Course
INTERSECT
SELECT Cno
  FROM Report

SELECT Cno
  FROM Course
EXCEPT
SELECT Cno
  FROM Report

SELECT S.Sno, S.Sname, C.Cno, C.Cname
  FROM Student AS S JOIN Report AS R
    ON S.Sno = R.Sno
  INNER JOIN Course AS C
    ON R.Cno = C.Cno

SELECT S.Sno, S.Sname, C.Cno, C.Cname
  FROM Student AS S LEFT JOIN Report AS R
    ON S.Sno = R.Sno
  RIGHT JOIN Course AS C
    ON R.Cno = C.Cno

SELECT Sno, Sname, Cno, Cname
  FROM Student CROSS JOIN Course