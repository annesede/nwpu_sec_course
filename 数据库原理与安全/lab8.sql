-- 存储过程
-- 1 创建一个带输入参数的存储过程：输入系别及性别后，查询该系相应性别学生的选课情况。
CREATE PROCEDURE proc_s_sc_select_1
@Dept NVARCHAR(50), @Sex NVARCHAR(1)
AS SELECT SC.Cno FROM SC, S WHERE S.Sno = SC.Sno AND S.Sdept = @Dept AND S.SSex = @Sex
GO
EXECUTE proc_student_select_1 @Dept = '信息安全系', @Sex = '女'

-- 2 创建一个带输入参数带默认值的存储过程：输入系别及性别后，查询该系相应性别学生的选课情况。如果不输入学生的性别，则默认查询该系男生的选课情况。
CREATE PROCEDURE proc_s_sc_select_2
@Dept NVARCHAR(50), @Sex NVARCHAR(1) = '男'
AS SELECT SC.Cno FROM SC, S WHERE S.Sno = SC.Sno AND S.Sdept = @Dept AND S.SSex = @Sex
GO
EXECUTE proc_student_select_2 @Dept = '信息安全系'

-- 3 创建一个带输入参数的存储过程：完成输入一个学号，如果该学号存在，则显示：该学号存在，同时显示该学生的学号、该学生所在系的学生人数。否则显示：该学号“XXXXX”不存在；同时显示全部有效的学号清单。
CREATE PROCEDURE proc_student_select
@Sno NVARCHAR(10)
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @s_ex INT
  SELECT @s_ex = COUNT(*) FROM S WHERE Sno = @Sno
  IF @s_ex > 0
  BEGIN
    SELECT '该学号存在' AS message, S.Sno, (SELECT COUNT(*) FROM S WHERE Sdept = S.Sdept) FROM S WHERE S.Sno = @Sno
  END
  ELSE
  BEGIN
    PRINT '该学号“' + @Sno + '”不存在';
    SELECT Sno FROM S
  END
END
GO
EXECUTE proc_student_select @Sno = 'S03'
EXECUTE proc_student_select @Sno = 'S99'

-- 4 创建一个存储过程，且该存储过程带输入参数：完成向表SC插入：学号、课号，分数。
--   ①如果学号在学生表中不存在，则提示：所输入学号XXX在学生表中不存在；
--   ②如果课号在课程表中不存在，则提示：所输入课号XXX在课程表中不存在；
--   ③如果所输入学号与课号均不存在，则提示：所输入学号XXX在学生表中不存在且所输入课号XXX在课程表中也不存在。
--   ④如果学号与课号均存在，且成功插入，则显示：所输入的信息：《学号XXX、课程号XXX、分数XX》已插入到表中。
CREATE PROC proc_sc_insert
@Sno NVARCHAR(10), @Cno NVARCHAR(10), @Grade NUMERIC(4,1)
AS
BEGIN
  DECLARE @s_ex INT
  DECLARE @c_ex INT
  SELECT @s_ex = COUNT(*) FROM S WHERE Sno = @Sno
  SELECT @c_ex = COUNT(*) FROM C WHERE Cno = @Cno
  IF @student_exists = 0 AND @course_exists = 0
  BEGIN
    PRINT '所输入学号 ' + @Sno + ' 在学生表中不存在且所输入课号 ' + @Cno + ' 在课程表中也不存在';
  END
  ELSE IF @student_exists = 0
  BEGIN
    PRINT '所输入学号 ' + @Sno + ' 在学生表中不存在';
  END
  ELSE IF @course_exists = 0
  BEGIN
    PRINT '所输入课号 ' + @Cno + ' 在课程表中不存在';
  END
  ELSE
  BEGIN
    INSERT INTO SC (Sno, Cno, Grade) VALUES (@Sno, @Cno, @Grade)
	PRINT '所输入的信息：《学号 ' + @Sno + '、课程号 ' + @Cno + '、分数 ' + @Grade + '》已插入到表中'
  END
END
GO
EXECUTE proc_sc_update @Sno = 'S04', @Cno = 'C05'
EXECUTE proc_sc_update @Sno = 'S01', @Cno = 'C99'
EXECUTE proc_sc_update @Sno = 'S99', @Cno = 'C01'

-- 5 创建一个存储过程，输入学生的学号，将该学生所选的所有课程的分数增加1分。
CREATE PROC proc_sc_update
@Sno NVARCHAR(10)
AS UPDATE SC SET Grade = Grade + 1 WHERE Sno = @Sno
GO
EXECUTE proc_sc_update @Sno = 'S02'

-- 6 创建一个加密存储过程，且该存储过程带输入参数：完成输入一个学号，如果该学号存在，则显示：该学号存在，并显示该学生的学号、姓名、所选修课程的课程数、所选课程的平均成绩。否则显示：该学号不存在（显示出学号）。
CREATE PROCEDURE proc_s_sc_select_encrypt_1 WITH ENCRYPTION
@Sno NVARCHAR(10)
AS
BEGIN SET NOCOUNT ON
  DECLARE @s_ex INT
  SELECT @s_ex = COUNT(*) FROM S WHERE Sno = @Sno
  IF @s_ex > 0
  BEGIN
    SELECT '该学号存在' AS message, S.Sno, S.Sname, (SELECT COUNT(Cno), AVG(Grade) FROM SC WHERE SC.Sno = @Sno) FROM S WHERE S.Sno = @Sno
  END
  ELSE
  BEGIN
    PRINT '该学号“' + @Sno + '”不存在';
  END
END
GO
EXECUTE proc_student_select @Sno = 'S02'
EXECUTE proc_student_select @Sno = 'S99'

-- 7 创建一个带输入参数的存储过程：完成输入一个学号，如果该学号存在，则显示：该学号存在，并显示该学生的学号、所选修课程的课程总数及课程清单、所选课程的平均成绩、所选课程的最高分及最低分、所选课程不及格的课程数及课程清单（含各课程分数）。否则显示：该学号不存在（显示出学号）。
CREATE PROCEDURE proc_s_sc_select_3
@Sno NVARCHAR(10)
AS
BEGIN SET NOCOUNT ON
  DECLARE @s_ex INT
  SELECT @s_ex = COUNT(*) FROM S WHERE Sno = @Sno
  IF @s_ex > 0
  BEGIN
    SELECT '该学号存在' AS message, S.Sno, (SELECT COUNT(Cno), AVG(Grade), MAX(Grade), MIN(Grade) FROM SC WHERE SC.Sno = @Sno), (SELECT COUNT(Cno) FROM SC WHERE SC.Sno = @Sno AND Grade < 60) FROM S WHERE S.Sno = @Sno
	SELECT Cno FROM SC WHERE Sno = @Sno
	SELECT Cno FROM SC WHERE Sno = @Sno AND Grade < 60
  END
  ELSE
  BEGIN
    PRINT '该学号“' + @Sno + '”不存在';
  END
END
GO
EXECUTE proc_student_select @Sno = 'S02'
EXECUTE proc_student_select @Sno = 'S99'

-- 8 创建一个存储过程，输入学生的学号，删除该学生的选课信息及学生表中的记录信息。
CREATE PROCEDURE proc_s_delete
@Sno NVARCHAR(10)
AS
BEGIN
  DELETE FROM SC WHERE Sno = @Sno
  DELETE FROM S WHERE Sno = @Sno
END

-- 9 创建一个加密存储过程，且该存储过程带输入参数：完成输入一个学号，如果该学号存在，则显示：该学号存在，并显示该学生所在系的所有学生的学号、姓名、所选修课程的课程数、所选课程的平均成绩，并按学号排序。否则显示：该学号不存在（显示出学号）。
CREATE PROCEDURE proc_s_sc_select_encrypt_2 WITH ENCRYPTION
@Sno NVARCHAR(10)
AS
BEGIN SET NOCOUNT ON
  DECLARE @s_ex INT
  SELECT @s_ex = COUNT(*) FROM S WHERE Sno = @Sno
  IF @s_ex > 0
  BEGIN
    PRINT '该学号存在'
    SELECT  S.Sno, S.Sname, COUNT(SC.Cno), AVG(Grade) FROM S, SC WHERE S.Sno = SC.Sno AND Sdept = (SELECT Sdept FROM S WHERE Sno = @Sno) GROUP BY Sno ORDER BY Sno
  END
  ELSE
  BEGIN
    PRINT '该学号“' + @Sno + '”不存在';
  END
END
GO
EXECUTE proc_student_select @Sno = 'S02'
EXECUTE proc_student_select @Sno = 'S99'