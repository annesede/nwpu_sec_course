-- 单表查询
-- 1.1 查询数学系（MA）全体学生的名单。
SELECT * FROM S WHERE Sdept = 'MA'

-- 1.2 查询考试成绩有不及格的学生的学号，且结果按学号降序排列。
SELECT Sno FROM SC WHERE Grade < 60 ORDER BY Sno DESC

-- 1.3 查询所有学生考试成绩有不及格的课程的情况，输出结果包括学号，课号和成绩。查询结果按学号降序排列，同一学生多门不及格课程的，按分数降序排列。
SELECT Sno, Cno, Grade FROM SC WHERE Grade < 60 ORDER BY Sno DESC, Grade DESC

-- 1.4 查询所有年龄在20岁以下的学生姓名及其年龄。查询结果按年龄降序排列，同一年龄有多个学生的，按学号降序排列。
SELECT Sname, Sage FROM S WHERE Sage < 20 ORDER BY Sage DESC, Sno DESC

-- 1.5 查询年龄在20~23岁(包括20岁和23岁)之间的学生的姓名、系别和年龄。
SELECT Sname, Sdept, Sage FROM S WHERE Sage BETWEEN 20 AND 23

-- 1.6 查询年龄不在18~23岁（包括18岁和23岁）之间的学生的姓名、系别和年龄，且查询结果按年龄降序排列，年龄相同的按性别降序排列，性别相同的按序号降序排列。
SELECT Sname, Sdept, Sage FROM S WHERE Sage NOT BETWEEN 18 AND 23 ORDER BY Sage DESC, Ssex DESC, Sno DESC

-- 1.7 查询数学系（MA）和计算机科学系（CS）学生的姓名和性别。且查询结果先按性别升序排列，性别相同的按学号降序排列。
SELECT Sname, Ssex FROM S WHERE Sdept IN ('MA', 'CS') ORDER BY Ssex, Sno DESC

-- 1.8 查询既不是数学系，也不是计算机科学系的学生的姓名和性别。
SELECT Sname, Ssex FROM S WHERE Sdept <> 'MA' AND Sdept <> 'CS'

-- 1.9 查询所有姓“薛”，且全名为三个汉字的学生的姓名、学号和性别。
SELECT Sname, Sno, Ssex FROM S WHERE Sname LIKE '薛__'

-- 1.10 查询所有姓“欧”，第二个字不是“阳”，且全名为四个汉字的学生的姓名。
SELECT Sname FROM S WHERE Sname LIKE '欧[^阳]__'

-- 1.11 查询“数据库”课程的课程号和学分。
SELECT Cno, CCredit FROM C WHERE Cname = '数据库'

-- 1.12 查询课程名以“DB_”开头，且倒数第3个字符为“%”的课程的详细情况。
SELECT * FROM C WHERE Cname LIKE 'DB\_%\%__' ESCAPE '\'

-- 1.13 查询课程名以字符串“Sys”结尾、以从 “B” 到 “G”的任何单个字母开头的所有课程情况。
SELECT * FROM C WHERE Cname LIKE '[B-G]%Sys'

-- 1.14 查询课程名中，所有以字符串：“C”、“M”和“T”开头，且倒数第三个字母为“_”的课程情况。查询结果先以课程名降序排列。
SELECT * FROM C WHERE Cname LIKE '[CMT]%\___' ESCAPE '\' ORDER BY Cname DESC

-- 1.15 查询课程名中，以字母“M”开头，并且第二个字母不是“a”，且有先修课的所有课程情况，且先按课程名降序排列。
SELECT * FROM C WHERE Cname LIKE 'M[^a]%' AND Cpno IS NOT NULL ORDER BY Cname DESC

-- 1.16 查询自控系（AC）、数学系（MA）和计算机科学系（CS）学生的姓名和性别。
SELECT Sname, Ssex FROM S WHERE Sdept IN ('AC', 'MA', 'CS')

-- 1.17 查询选修了“C03”号课程的学生的学号及其成绩，查询结果按分数降序排列。
SELECT Sno, Grade FROM CS WHERE Cno = 'C03' ORDER BY Grade DESC

-- 1.18 查询全体学生情况，查询结果按所在系的系号升序排列，同一系中的学生按年龄降序排列。
SELECT * FROM S ORDER BY Sdept, Sage DESC

-- 2.1 查询学生总人数。
SELECT COUNT(*) FROM S

-- 2.2 查询选修了课程的学生总人数。
SELECT COUNT(DISTINCT Sno) FROM SC

-- 2.3 计算选修了“C02”号课程的学生人数。
SELECT COUNT(DISTINCT Sno) FROM SC WHERE Cno = 'C02'

-- 2.4 计算“C02”号课程的学生平均成绩。
SELECT AVG(Grade) FROM SC WHERE Cno = 'C02'

-- 2.5 查询学生中男生和女生的人数，并将结果按人数降序排列。
SELECT COUNT(Ssex) AS SexNo FROM S GROUP BY Ssex ORDER BY SexNo DESC

-- 2.6 查询各课程平均成绩>85分的课程号及课程成绩。结果按平均成绩降序排列，平均成绩相同的按课程号升序排列。
SELECT Cno, AVG(Grade) FROM SC GROUP BY Cno HAVING AVG(Grade) >= 85 ORDER BY AVG(Grade) DESC, Cno

-- 2.7 查询选修了“C02”号课程的学生最高分数。如果最高分有多个学生，按学号降序排列。
SELECT MAX(Grade) FROM SC WHERE Cno = 'C02' ORDER BY Sno DESC

-- 2.8 求各个课程成绩在90分以上的学生人数。查询结果按人数降序排列。如果人数有相同情况的，按平均成绩降序排列。
SELECT COUNT(Sno) AS num FROM SC WHERE Grade >= 90 GROUP BY Cno ORDER BY num DESC, AVG(Grade) DESC

-- 2.9 求各个课程号及相应的选课人数。查询结果按选课人数降序排列。如果课程的选课人数有相同的情况，按平均成绩降序排列。
SELECT COUNT(Sno) AS num FROM SC GROUP BY Cno ORDER BY num DESC, AVG(Grade) DESC

-- 2.10 查询选修了2门以上课程的学生学号。查询结果按所选课程数降序排列。如果有课程数相同的，按平均成绩升序排列。
SELECT Sno FROM SC GROUP BY Sno HAVING COUNT(Cno) >=2 ORDER BY COUNT(Cno) DESC, AVG(Grade)

-- 2.11 查询有2门以上课程在80分以上的学生的学号及80分以上的课程数。查询结果按80分以上的课程数降序排列。如果有课程数相同的，按所选全部课程数升序排列。
SELECT Sno, COUNT(Cno) AS num FROM SC GROUP BY Sno HAVING COUNT(Cno) >=2 AND Grade >= 80 ORDER BY num DESC, COUNT(Cno)



-- 多表查询
-- 1.1.1 查询所选课程中含有“Creidt>5”的课程的学生的基本信息。
SELECT S.* FROM S,C,SC WHERE S.Sno = SC.Sno And C.Cno = SC.Cno AND C.CCredit > 5

-- 1.1.2 查询课程成绩含有未及格（<60分）的课程信息。
SELECT C.* FROM C,SC WHERE C.Cno = SC.Cno AND C.Grade < 60

-- 1.1.3 查询每个学生的学号、姓名，以及选修的课号、课名及成绩。
SELECT S.Sno, S.Sname, C.Cno, C.Cname, SC.Grade FROM S,C,SC WHERE S.Sno = SC.Sno And C.Cno = SC.Cno

-- 1.2.1 查询具有间接先行课的课程。
SELECT first.Cno, second.Cpno from C first, C second WHERE first.Cpno = second.Cno 

-- 1.2.2 查询所有与学号为“S01”学生为同一系的学生的学号和姓名。
SELECT Sno, Sname FROM S WHERE Sdept IN (SELECT Sdept FROM S WHERE Sno = 'S01')

-- 1.3.1 解释说明外连接定义和种类，并以S表和C表进行各种外连接的验证。
SELECT S.*, C.* FROM S LEFT JOIN SC ON S.Sno = SC.Sno RIGHT JOIN C ON C.Cno = SC.Cno
SELECT S.*, C.* FROM S RIGHT JOIN SC ON S.Sno = SC.Sno LEFT JOIN C ON C.Cno = SC.Cno
SELECT S.*, C.* FROM S CROSS JOIN C

-- 1.4.1 查找“高等数学”成绩大于90分的陕西籍女生的学号、姓名、分数。
SELECT S.Sno, S.Sname, SC.Grade FROM SC JOIN S ON S.Sno = SC.Sno JOIN C ON C.Cno = SC.Cno WHERE C.Cname = '高等数学' AND SC.Grade > 90 AND S.Sbirthplace = '陕西'

-- 2.1.1 查询“数学系”学生高等数学的成绩。
SELECT Grade FROM SC WHERE Sno IN (SELECT Sno FROM S WHERE Sdept = '数学系')

-- 2.1.2 查询选修了“S02”号课程的学生姓名。
SELECT Sname FROM S WHERE Sno IN (SELECT Sno FROM SC WHERE Cno = 'S02')

-- 2.1.3 查询与“张柬之”在同一个系学习的学生。
SELECT * FROM S WHERE Sdept IN (SELECT Sdept FROM S WHERE Sname = '张柬之')

-- 2.1.4 查询出生日期和陕西籍学生出生日期相同的学生信息。
SELECT * FROM S WHERE Sbirthdate IN (SELECT Sbirthdate FROM S WHERE Sbirthplace = '陕西')

-- 2.1.5 查询选修了课程名为“信息系统”的学生学号和姓名。
SELECT Sno, Sname FROM S WHERE Sno IN (SELECT SC.Sno FROM SC JOIN C ON C.Cno = SC.Cno WHERE C.Cname = '信息系统')

-- 2.1.6 查询未被“太平公主”选修的其他课程的选修情况。
SELECT Sno, Cno FROM SC WHERE Cno NOT IN (SELECT SC.Cno FROM SC JOIN S ON S.Sno = SC.Sno WHERE S.name = '太平公主')

-- 2.2.1 假设一个学生只可能在一个系学习，并且必须属于一个系。查询与“上官婉儿”在同一个系学习的学生。
SELECT * FROM S WHERE Sdept = (SELECT Sdept FROM S WHERE Sname = '上官婉儿')

-- 2.2.2 查询学号为“S01”的学生所选课程中超过选修课平均成绩的课程信息。
SELECT C.* FROM SC JOIN C ON C.Cno = SC.Cno, (SELECT Sno, AVG(Grade) FROM SC GROUP BY Sno) AS Avg_sc(avg_sno, avg_grade) WHERE SC.Sno = Avg_sc.avg_sno AND SC.Grade >= Avg_sc.avg_grade AND SC.Sno = 'S01'

-- 2.3.1 查询非陕西籍的学生中比陕西籍任意一个学生年龄小的学生学号和姓名。
SELECT Sno, Sname FROM S WHERE Sage < ALL(SELECT Sage FROM S WHERE Sbirthplace = '陕西')

-- 2.3.2 查询其他系中比“信息安全系”某一学生年龄小的学生学号、姓名和年龄。
SELECT Sno, Sname, Sange FROM S WHERE Sage < ANY(SELECT Sage FROM S WHERE Sdept = '信息安全系')

-- 2.3.3 查询其他系中比“信息安全系”所有学生年龄都小的学生学号、姓名及年龄。
SELECT Sno, Sname, Sange FROM S WHERE Sage < ALL(SELECT Sage FROM S WHERE Sdept = '信息安全系')

-- 2.3.4 查询选课关系中所选课程平均成绩最高的学生的基本信息。
SELECT S.* FROM S, SC WHERE S.Sno = SC.Sno GROUP BY Sno HAVING AVG(Grade) >= ALL(SELECT AVG(Grade) FROM SC GROUP BY Sno)

-- 2.3.1 查询选修了课程的学生姓名。
SELECT Sname FROM S WHERE EXISTS (SELECT * FROM SC WHERE S.Sno = SC.Sno)

-- 2.3.2 查询选修了全部课程的学生姓名。
SELECT Sname FROM S WHERE NOT EXISTS (SELECT * FROM C WHERE NOT EXISTS (SELECT * FROM SC WHERE SC.Sno = S.Sno AND SC.Cno = C.Cno))

-- 2.3.3 查询至少选修了学号为“S01”学生选修的全部课程的学生学号、姓名、所在系。
SELECT Sno, Sname, Sdept FROM S WHERE NOT EXISTS (SELECT * FROM C WHERE NOT EXISTS (SELECT * FROM SC WHERE SC.Sno = S.Sno AND SC.Cno = C.Cno AND SC.Sno = 'S01'))

-- 2.3.4 查询所有存在不及格情况（<60）的学生的学号和姓名。
SELECT Sno, Sname FROM S WHERE NOT EXISTS (SELECT * FROM C WHERE NOT EXISTS (SELECT * FROM SC WHERE SC.Sno = S.Sno AND SC.Cno = C.Cno AND SC.Grade < 60))

-- 3.1.1 查询学生关系表中，学号以8结尾和以0结尾的学生的基本信息。
SELECT * FROM S WHERE Sno LIKE '%0' UNION SELECT * FROM S WHERE Sno LIKE '%8'

-- 3.1.2 查询选课关系表中，成绩是100分或者成绩是0分的记录。
SELECT * FROM SC WHERE Grade = 100 UNION SELECT * FROM SC WHERE Grade = 0

-- 3.2.1 查询系别为“信息安全系”的姓张的所有男性同学的基本信息。
SELECT * FROM S WHERE Sdept = '信息安全系' INTERSECT SELECT * FROM S WHERE Sname LIKE '张%' AND Ssex = '男'

-- 3.2.2 查询课程表中学分大于3分并且课号是以0结尾的课程。
SELECT * FROM C WHERE CCredit > 3 INTERSECT SELECT * FROM C WHERE Cno LIKE '%0'

-- 3.3.1 查询“信息安全系”所有除去陕西出生地的学生的基本信息。
SELECT * FROM S WHERE Sdept = '信息安全系' EXCEPT SELECT * FROM S WHERE Sbirthplace = '陕西'

-- 3.3.2 查询选课关系表中成绩未及格（<60分）的学生学号。
SELECT Cno FROM SC EXCEPT SELECT Cno FROM SC WHERE Grade < 60



-- 视图
-- 1.1 建立计算机系学生的视图CS_Stud1（Sno, Sname, Sage）。
CREATE VIEW CS_Stud1 AS SELECT Sno, Sname, Sage FROM S WHERE Sdept = '计算机系'

-- 1.2 建立计算机系学生的视图，并要求透过该视图所进行的更新操作，只涉及计算机系学生CS_Stud_2（Sno, Sname, Sage）。
CREATE VIEW CS_Stud_2 AS SELECT Sno, Sname, Sage FROM S WHERE Sdept = '计算机系' WITH CHECK OPTION

-- 1.3 建立数学系选修了C02号课程的学生视图MA_Stud1(Sno, Sname, Grade)。
CREATE VIEW MA_Stud1(Sno, Sname, SGrade) AS SELECT S.Sno, Sname, Grade FROM S, SC WHERE S.Sno = SC.Sno AND Sdept = '数学系' AND SC.Cno = 'C02'

-- 1.4 建立数学系选修了C02号课程且成绩在90分以上的学生的视图MA_Stud_90(Sno, Sname, Grade)。
CREATE VIEW MA_Stud1_90 AS SELECT Sno, Sname, Grade FROM MA_Stud1 WHERE Grade >= 90

-- 1.5 将学生的学号及其平均成绩定义为一个视图。S_Gavg1(Sno, SGavg)。
CREATE VIEW S_Gavg1(Sno, SGavg) AS SELECT Sno, AVG(Grade) FROM SC GROUP BY Sno

-- 1.6 将平均成绩>=90分的学生及其平均成绩定义为一个视图。S_Gavg2(Sno, SGavg)。
CREATE VIEW S_Gavg2 AS SELECT Sno, SGavg FROM S_Gavg1 WHERE SGavg >= 90

-- 1.7 将平均成绩>=90分，选修课程累计达到30学分，且选修课程数达到10门以上的学生定义为一个视图。S_Gavg3(Sno, SumCredit, SumC, SGavg)。
CREATE VIEW S_Gavg3(Sno, SumCredit, SumC, SGavg) AS SELECT Sno, SUM(CCredit), COUNT(SC.Cno), AVG(Grade) FROM C, SC WHERE SC.Cno = C.Cno GROUP BY Sno HAVING AVG(C.Grade) >= 90 AND SUM(C.CCredit) >= 30 AND COUNT(SC.Cno) >= 10

-- 1.8 选修学生所覆盖的系别达到3个以上，且将选修人数最多的课程，定义为一个视图。C_Survey1 (Cno, SumDept, MaxSumStud)
CREATE VIEW C_Survey1(Cno, SumDept, MaxSumStud) AS SELECT TOP 1 Cno, COUNT(DISTINCT Dept), COUNT(Sno) FROM S, SC WHERE SC.Sno = S.Sno GROUP BY SC.Cno HAVING COUNT(DISTINCT S.Dept) >= 3 ORDER BY COUNT(SC.Cno) DESC

-- 1.9 将平均成绩介于80~90分，选修人数达到15人以上，且选修学生所覆盖的系别达到3个以上的课程，定义为一个视图。C_Survey2 (Cno, SumStud, SumDept, CGavg)
CREATE VIEW C_Survey2(Cno, umStud, SumDept, CGavg) AS SELECT Cno, COUNT(Sno), COUNT(DISTINCT Dept), AVG(Grade) FROM S, SC WHERE SC.Sno = S.Sno GROUP BY SC.Cno HAVING AVG(SC.Grade) BETWEEN 80 AND 90 AND COUNT(SC.Cno) >= 15 AND COUNT(DISTINCT S.Dept) >= 3

-- 1.10 将平均成绩介于80~90分，选修人数达到20人以上，不及格学生不超过3人，且选修学生所覆盖的系别超过3个以上的课程，定义为一个视图。C_Survey3 (Cno, SumStud, SumDept, CGavg)
CREATE VIEW C_Survey3(Cno, umStud, SumDept, CGavg) AS SELECT Cno, COUNT(Sno), COUNT(DISTINCT Dept), AVG(Grade) FROM S, SC WHERE SC.Sno = S.Sno GROUP BY SC.Cno HAVING AVG(SC.Grade) BETWEEN 80 AND 90 AND COUNT(SC.Cno) >= 20 AND SUM(CASE WHEN S.Grade < 60 THEN 1 ELSE 0 END) <= 3 AND COUNT(DISTINCT S.Dept) >= 3

-- 1.11 将选修人数超过30以上，且课程选修学生覆盖的系别最多的课程，定义为一个视图。C_Survey4 (Cno, MaxSumStud, SumDept)
CREATE VIEW C_Survey4(Cno, MaxSumStud, SumDept) AS SELECT TOP 1 Cno, COUNT(Cno), COUNT(DISTINCT Dept) FROM S, SC WHERE SC.Sno = S.Sno GROUP BY SC.Cno HAVING COUNT(Cno) >= 30 ORDER BY COUNT(DISTINCT S.Dept) DESC

-- 2.1 利用计算机系学生的视图CS_Stud1中查询：①年龄小于20岁的学生；②选修了C02号课程的学生学号和姓名。
SELECT * FROM CS_Stud1 WHERE Sage < 20
SELECT CS_Stud1.Sno, Sname FROM CS_Stud1, SC WHERE SC_Stud1.Sno = SC.Sno AND SC,Cno = 'C02'

-- 2.2 利用数学系学生的视图MA_Stud1中查询：①成绩不及格的人数；②C02号课程的：课程号、课程名、平均成绩、最高分、最低分、最高分的学生学号和姓名、最低分的学生学号和姓名。
SELECT COUNT(Sno) FROM MA_Stud1 WHERE SGrade < 60
SELECT 'C02' AS no, (SELECT Sname FROM SC WHERE Cno = 'C02'), AVG(Grade) AS grade FROM MA_Stud1, SC WHERE MA_Stud1.Sno = SC.Sno AND SC.Cno = 'C02' UNION SELECT TOP 1 Sno, Sname, SGrade FROM MA_Stud1 ORDER BY SGrade DESC UNION  SELECT TOP 1 Sno, Sname, SGrade FROM MA_Stud1 ORDER BY SGrade

-- 2.3 利用S_Gavg1视图中查询：①平均成绩在90分以上的学生学号和平均成绩；②平均成绩在90分以上的学生的选课情况，包括：学号、姓名、课程名、成绩。
SELECT * FROM S_Savg1 WHERE SGavg >= 90
SELECT S.Sno, Sname, Cname, Grade FROM S_Savg1 WHERE SGavg >= 90 JOIN SC ON SGavg.Sno = SC.Sno JOIN C ON C.Cno = SC.Cno JOIN S ON S.Sno = SGavg.Sno 

-- 2.4 利用S_Gavg3视图中查询：①平均成绩在95分以上的学生学号和平均成绩；②平均成绩在95分以上的学生的选课情况，包括：学号、姓名、课程名、成绩（隐含：选修课程累计达到30学分，且选修课程数达到10门以上）。
SELECT Sno, SGavg FROM S_Gavg3 WHERE SGavg >= 95
SELECT S.Sno, Sname, Cname, Grade FROM S_Gavg3 JOIN SC ON S_Gavg3.Sno = SC.Sno JOIN C ON C.Cno = SC.Cno JOIN S.Sno = S_Gavg3.Sno

-- 2.5 利用C_Survey1视图中查询：课程的选课情况，包括：课号、课程名、学号、姓名、成绩（隐含：选修人数最多，且选修学生所覆盖的系别达到3个以上）
SELECT C.Cno, Cname, S.Sno, Sname, Grade FROM C_Survey1 JOIN C ON C_Survey1.Cno = C.Cno JOIN SC ON C.Cno = SC.Cno JOIN ON S.Sno = SC.Sno

-- 2.6 利用C_Survey3视图中查询：课程的选修情况，包括：课号、课程名、学号、姓名、成绩（隐含：平均成绩介于80~90分，选修人数20人以上，不及格学生不超过3人，且选修学生所覆盖的系别超过3个）。
SELECT C.Cno, Cname, S.Sno, Sname, Grade FROM C_Survey3 JOIN C ON C_Survey3.Cno = C.Cno JOIN SC ON C.Cno = SC.Cno JOIN ON S.Sno = SC.Sno

-- 2.7 利用视图消解法，将上述视图（3）~（6）中，基于视图的查询，转化为基于基表的查询
SELECT Sno, AVG(Grade) FROM SC GROUP BY Sno HAVING AVG(Grade) >= 90
SELECT S.Sno, Sname, Cname, Grade FROM SC GROUP BY Sno HAVING AVG(Grade) >= 90 JOIN SC ON SGavg.Sno = SC.Sno JOIN C ON C.Cno = SC.Cno JOIN S ON S.Sno = SGavg.Sno 
SELECT Sno, AVG(Grade) FROM C, SC WHERE SC.Cno = C.Cno GROUP BY Sno HAVING AVG(C.Grade) >= 95 AND SUM(C.CCredit) >= 30 AND COUNT(SC.Cno) >= 10
SELECT S.Sno, Sname, Cname, Grade FROM SC, C, S WHERE SC.Cno = C.Cno AND SC.Sno = S.Sno GROUP BY Sno HAVING AVG(C.Grade) >= 95 AND SUM(C.CCredit) >= 30 AND COUNT(SC.Cno) >= 10 
SELECT C.Cno, Cname, S.Sno, Sname, Grade FROM S, SC, C WHERE SC.Sno = S.Sno AND SC.Sno = C.Cno GROUP BY SC.Cno HAVING COUNT(DISTINCT S.Dept) >= 3 ORDER BY COUNT(SC.Cno) DESC
SELECT C.Cno, Cname, S.Sno, Sname, Grade FROM S, SC, C WHERE SC.Sno = S.Sno AND SC.Sno = C.Cno GROUP BY SC.Cno HAVING AVG(SC.Grade) BETWEEN 80 AND 90 AND COUNT(SC.Cno) >= 20 AND SUM(CASE WHEN S.Grade < 60 THEN 1 ELSE 0 END) <= 3 AND COUNT(DISTINCT S.Dept) >= 3

-- 3.1 利用计算机系学生视图CS_Stud1（Sno, Sname, Sage）中，将学号为“S01”的学生姓名更改为“则天女皇”（假设学生表S中，存在学号为“S01”的学生，且学生的系别是“CS”）。
UPDATE CS_Stud1 SET Sname = '则天女皇' WHERE Sno = 'S01'

-- 3.2 利用计算机系学生视图CS_Stud1（Sno, Sname, Sage）中，将学号为“S02”的学生姓名更改为“仁杰宰相”（假设学生表S中，存在学号为“S02”的学生，且学生的系别是“MA”）。
UPDATE CS_Stud1 SET Sname = '仁杰宰相' WHERE Sno = 'S02'

-- 3.3 利用计算机系学生视图CS_Stud1（Sno, Sname, Sage）中，将学号为“S01”学生记录删除（假设学生表S中，存在学号为“S01”的学生，且学生的系别是“CS”）。
DELETE FROM CS_Stud1 WHERE Sno = 'S01'

-- 3.4 利用计算机系学生视图CS_Stud1（Sno, Sname, Sage）中，将学号为“S02”学生记录删除（假设学生表S中，存在学号为“S02”的学生，且学生的系别是“MA”）。
DELETE FROM CS_Stud1 WHERE Sno = 'S02'

-- 3.5 向计算机系学生视图CS_Stud1（Sno, Sname, Sage）中，插入一条新的学生记录，其中学号为“S51”，姓名为“如燕”。如果能插入，插入后视图是否可以查询该记录？
-- 不能插入, Sage不能为NULL

-- 3.6 向计算机系学生视图CS_Stud1（Sno, Sname, Sage）中，插入一条新的学生记录，其中学号为“S52”，姓名为“姚崇”，年龄为60岁。如果能插入，插入后视图是否可以查询该记录？
INSERT INTO CS_Stdu1 VALUES('S52', '姚崇', '60')

-- 3.7 利用计算机系学生视图CS_Stud_2（Sno, Sname, Sage），将学号为“S61”记录的年龄更改为43岁（假设学生表中，存在学号为“S61”的学生，且学生的系别是“CS”）。如不能修改，为什么？
UPDATE CS_Stud_2 SET Sage = 43 WHERE Sage = 43

-- 3.8 利用计算机系学生视图CS_Stud_2（Sno, Sname, Sage），将学号为“S62”学生的年龄更改为54岁（假设学生表中，存在学号为“S62”的学生，且学生的系别是“MA”）。如不能修改，为什么？
-- 不能插入，超出视图范围

-- 3.9 向计算机系学生视图CS_Stud_2（Sno, Sname, Sage）中，插入一新的学生记录，其中学号为“S63”，姓名为“宋璟”。 如果能插入，插入后视图是否可以查询该记录？如不能插入，为什么？
-- 不能插入，Sage不能为NULL

-- 3.10 向计算机系学生视图CS_Stud_2（Sno, Sname, Sage）中，插入一新的学生记录，其中学号为“S64”，姓名为“李元芳”，年龄为30岁。 如果能插入，插入后视图是否可以查询该记录？如不能插入，为什么？

INSERT INTO CS_Stdu2 VALUES('S64', '李元芳', '30')

-- 3.11 利用计算机系学生视图CS_Stud_2（Sno, Sname, Sage），将学号为“S65”记录删除（假设学生表中，存在学号为“S65”的学生，且学生的系别是“CS”）。如不能删除，为什么？
DELETE FROM CS_Stud2 WHERE Sno = 'S65'

-- 3.12 利用计算机系学生视图CS_Stud_2（Sno, Sname, Sage），将学号为“S66”记录删除（假设学生表中，存在学号为“S66”的学生，且学生的系别是“MA”）。如不能删除，为什么？
-- 不能删除，超出视图范围