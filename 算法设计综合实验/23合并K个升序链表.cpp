class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        return mergeDivide(lists, 0, lists.size() - 1);
    }

    ListNode* mergeDivide(vector<ListNode*>& lists, int begin, int end) {
        if (begin == end) return lists[begin];
        if (begin > end) return nullptr;
        int mid = (begin + end) >> 1;
        return mergeList(mergeDivide(lists, begin, mid), mergeDivide(lists, mid + 1, end));
    }

    ListNode* mergeList(ListNode* list1, ListNode* list2) {
        ListNode* dummy = new ListNode(-1);
        ListNode* curr = dummy;
        ListNode* curr1 = list1;
        ListNode* curr2 = list2;
        while (curr1 && curr2) {
            if (curr1->val <= curr2->val) {
                curr->next = curr1;
                curr1 = curr1->next;
            } else {
                curr->next = curr2;
                curr2 = curr2->next;
            }
            curr = curr->next;
        }
        if (curr1) curr->next = curr1;
        if (curr2) curr->next = curr2;
        ListNode* merged = dummy->next;
        delete(dummy);
        return merged;
    }   
};
