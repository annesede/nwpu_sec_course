class Solution {
public:
    void reorderList(ListNode* head) {
        if (!head) return;
        ListNode* list1 = head;
        ListNode* mid = midNode(head);
        ListNode* list2 = mid->next;
        mid->next = nullptr;
        list2 = reverseList(list2);
        mergeList(list1, list2);
    }

    ListNode* midNode(ListNode* head) {
        ListNode* slow = head;
        ListNode* fast = head;
        while(fast->next && fast->next->next) {
            slow = slow->next;
            fast = fast->next->next;
        }
        return slow;
    }

    ListNode* reverseList(ListNode* head) {
        ListNode* prev = nullptr;
        ListNode* curr = head;
        ListNode* next = nullptr;
        while (curr) {
            next = curr->next;
            curr->next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }

    void mergeList(ListNode* list1, ListNode* list2) {
        ListNode* curr1 = nullptr;
        ListNode* curr2 = nullptr;
        while (list1 && list2) {
            curr1 = list1->next;
            curr2 = list2->next;
            list1->next = list2;
            list1 = curr1;
            list2->next = list1;
            list2 = curr2;
        }
    }
};
