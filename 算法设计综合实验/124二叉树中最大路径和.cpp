class Solution {
public:
    int maxPath = INT_MIN;

    int maxPathSum(TreeNode* root) {
        backtracking(root);
        return maxPath;
    }

    int backtracking(TreeNode* root) {
        if (!root) return 0;
        int maxLeft = max(backtracking(root->left), 0);
        int maxRight = max(backtracking(root->right), 0);
        maxPath = max(maxPath, root->val + maxLeft + maxRight);
        return root->val + max(maxLeft, maxRight);
    }
};
