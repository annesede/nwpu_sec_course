class Solution {
public:
    int minReorder(int n, vector<vector<int>>& connections) {
        vector<vector<pair<int, int>>> edge(n);
        for (int i = 0; i < connections.size(); i++) {
            edge[connections[i][0]].push_back(make_pair(connections[i][1], 1));
            edge[connections[i][1]].push_back(make_pair(connections[i][0], 0));
        }
        return dfs(0, -1, edge);
    }

    int dfs(int curr, int prev, vector<vector<pair<int, int>>>& edge) {
        int cnt = 0;
        for (int i = 0; i < int(edge[curr].size()); i++)
            if (edge[curr][i].first != prev)
                cnt = cnt + edge[curr][i].second + dfs(edge[curr][i].first, curr, edge);
        return cnt;
    }
};
