class Solution {
public:
    ListNode* sortList(ListNode* head) {
        return mergeSort(head);
    }

    ListNode* mergeSort(ListNode* head) {
        if (!head || !head->next) return head;
        ListNode* head1 = head;
        ListNode* mid = midNode(head);
        ListNode* head2 = mid->next;
        mid->next = nullptr;
        return mergeList(mergeSort(head1), mergeSort(head2));
    }

    ListNode* midNode(ListNode* head) {
        ListNode* slow = head;
        ListNode* fast = head;
        while (fast->next && fast->next->next) {
            slow = slow->next;
            fast = fast->next->next;
        }
        return slow;
    }

    ListNode* mergeList(ListNode* head1, ListNode* head2) {
        ListNode* dummy = new ListNode(-1);
        ListNode* curr = dummy;
        ListNode* curr1 = head1;
        ListNode* curr2 = head2;
        while (curr1 && curr2) {
            if (curr1->val <= curr2->val) {
                curr->next = curr1;
                curr1 = curr1->next;
            } else {
                curr->next = curr2;
                curr2 = curr2->next;
            }
            curr = curr->next;
        }
        if (curr1) curr->next = curr1;
        if (curr2) curr->next = curr2;
        ListNode* merged = dummy->next;
        delete(dummy);
        return merged;
    }    
};
