class Solution {
public:
    vector<int> findMinHeightTrees(int n, vector<vector<int>>& edges) {
        if (n == 1) return {0};
        vector<int> degree(n);
        vector<vector<int>> edge(n);
        queue<int> queue;
        vector<int> root;
        for (auto &i : edges) {
            edge[i[0]].push_back(i[1]);
            edge[i[1]].push_back(i[0]);
            degree[i[0]]++;
            degree[i[1]]++;
        }
        for (int i = 0; i < n; i++) 
            if (degree[i] == 1) queue.push(i);
        int remains = n;
        while (remains > 2) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                int curr = queue.front();
                for (auto &j : edge[curr]) {
                    degree[j]--;
                    if (degree[j] == 1) queue.push(j);
                }
                queue.pop();
            }
            remains -= size;
        }
        while (!queue.empty()) {
            root.push_back(queue.front());
            queue.pop();
        }
        return root;
    }
};
