class Solution {
public:
    int uniquePaths(int m, int n) {
        long long cnt = 1;
        for (int i = n, j = 1; j < m; i++, j++) {
            cnt = cnt * i / j;
        }
        return cnt;
    }
};
