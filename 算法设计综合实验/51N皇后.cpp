class Solution {
public:
    vector<vector<string>> allOrder;

    vector<vector<string>> solveNQueens(int n) {
        allOrder.clear();
        vector<string> order(n, string(n, '.'));
        backtracking(n, 0, order);
        return allOrder;
    }

    void backtracking(int n, int raw, vector<string>& order) {
        if (raw == n) {
            allOrder.push_back(order);
            return;
        }
        for (int col = 0; col < n; col++) {
            if (isValid(n, raw, col, order)) {
                order[raw][col] = 'Q';
                backtracking(n, raw + 1, order);
                order[raw][col] = '.';
            }
        }
    }

    bool isValid(int n, int raw, int col, vector<string>& order) {
        for (int i = 0; i < raw; i++) if(order[i][col] == 'Q') return false;
        for (int i = raw - 1, j = col - 1; i >= 0 && j >=0; i--, j--)
            if (order[i][j] == 'Q') return false;
        for (int i = raw - 1, j = col + 1; i >= 0 && j <= n - 1; i--, j++)
            if (order[i][j] == 'Q') return false;
        return true;
    }
};
