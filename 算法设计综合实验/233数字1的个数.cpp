class Solution {
    const vector<long long> mag = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000};
public:
    int countDigitOne(int n) {
        int cnt = 0;
        for (int i = 0; n >= mag[i]; ++i)
            cnt += (n / mag[i + 1]) * mag[i] + min(max(n % mag[i + 1] - mag[i] + 1, 0LL), mag[i]);
        return cnt;
    }
};
