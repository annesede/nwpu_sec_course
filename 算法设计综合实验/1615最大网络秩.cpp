class Solution {
public:
    int maximalNetworkRank(int n, vector<vector<int>>& roads) {
        vector<vector<bool>> connected(n, vector<bool>(n, false));
        vector<int> degree(n, 0);
        for (int i = 0; i < roads.size(); i++) {
            connected[roads[i][0]][roads[i][1]] = true;
            connected[roads[i][1]][roads[i][0]] = true;
            degree[roads[i][0]]++; degree[roads[i][1]]++;
        }
        int maxRank = 0;
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                maxRank = max(maxRank, degree[i] + degree[j] - int(connected[i][j]));
        return maxRank;
    }
};
