class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int prev = 0, maxCurr = nums[0];
        for (int i = 0; i < nums.size(); i++) {
            prev = prev < 0 ? nums[i] : prev + nums[i];
            maxCurr = max(maxCurr, prev);
        }
        return maxCurr;
    }
};
