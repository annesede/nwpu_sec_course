class Solution {
public:
    int leastInterval(vector<char>& tasks, int n) {
        vector<int> cnt(26, 0);
        for (int i = 0; i < tasks.size(); i++)
            cnt[tasks[i] - 'A']++;
        sort(cnt.begin(), cnt.end());
        int len = n * (cnt[25] - 1) + cnt[25];
        for (int i = 0; i < 25; i ++)
            if (cnt[i] == cnt[25]) len++;
        return max(len, static_cast<int>(tasks.size()));
    }
};
