class Solution {
public:
    vector<vector<int>> allPath;
    vector<int> path;

    vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph) {
        path.push_back(0);
        dfs(graph, 0, graph.size() - 1);
        return allPath;
    }

    void dfs(vector<vector<int>>& graph, int curr, int end) {
        if (curr == end) {
            allPath.push_back(path);
            return;
        }
        for (auto& next: graph[curr]) {
            path.push_back(next);
            dfs(graph, next, end);
            path.pop_back();
        }
    }
};
