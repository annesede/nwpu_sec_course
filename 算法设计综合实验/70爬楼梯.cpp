class Solution {
public:
    int climbStairs(int n) {
        vector<int> dp(3);
        dp[0] = 0, dp[1] = 0, dp[2] = 1;
        for (int i = 0; i < n; i++) {
            dp[0] = dp[1], dp[1] = dp[2];
            dp[2] = dp[0] + dp[1];
        }
        return dp[2];
    }
};
