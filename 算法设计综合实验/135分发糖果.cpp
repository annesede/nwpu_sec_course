class Solution {
public:
    int candy(vector<int>& ratings) {
        int cnt = 1, incSeq = 1, decSeq = 0, prev = 1;
        for (int i = 1; i < ratings.size(); i++) {
            if (ratings[i] >= ratings[i - 1]) {
                decSeq = 0; prev = ratings[i] == ratings[i - 1] ? 1 : prev + 1;
                cnt += prev; incSeq = prev;
            } else {
                decSeq = decSeq + 1 == incSeq ? decSeq + 2 : decSeq + 1;
                cnt += decSeq; prev = 1;
            }
        }
        return cnt;
    }
};
