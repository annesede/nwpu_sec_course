class Solution {
public:
    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        vector<int> stack;
        vector<TreeNode*> tree(int(nums.size()));
        for (int i = 0; i < nums.size(); ++i) {
            tree[i] = new TreeNode(nums[i]);
            // 单调递减栈, 数组下标入栈
            // 出栈至左边最大值作为当前节点左孩子
            while (!stack.empty() && nums[i] > nums[stack.back()]) {
                tree[i]->left = tree[stack.back()];
                stack.pop_back();
            }
            // 栈顶最终将右边最大值作为右孩子
            if (!stack.empty()) tree[stack.back()]->right = tree[i];
            // 当前结点入栈
            stack.push_back(i);
        }
        // 栈底一定为最大值(即根节点)
        return tree[stack[0]];
    }
};
