class Solution {
public:
    int countRangeSum(vector<int>& nums, int lower, int upper) {
        long sum = 0;
        vector<long> preSum{0};
        for (int i = 0; i < nums.size(); i++) {
            sum += nums[i];
            preSum.push_back(sum);
        }
        return mergeCnt(preSum, lower, upper, 0, preSum.size() - 1);
    }

    int mergeCnt(vector<long>& preSum, int lower, int upper, int begin, int end) {
        if (begin == end) return 0;
        int mid = (begin + end) >> 1;
        int cnt = mergeCnt(preSum, lower, upper, begin, mid) + 
                mergeCnt(preSum, lower, upper, mid + 1, end);

        int left = mid + 1, right = mid + 1;
        for (int i = begin; i <= mid; i++) {
            while (left <= end && preSum[left] - preSum[i] < lower) left++;
            while (right <= end && preSum[right] - preSum[i] <= upper) right++;
            cnt += right - left;
        }

        vector<long> merged;
        left = begin, right = mid + 1;
        while (left <= mid && right <= end) {
            if (preSum[left] < preSum[right]) merged.push_back(preSum[left++]);
            else merged.push_back(preSum[right++]);
        }
        while (right <= end && left > mid) merged.push_back(preSum[right++]);
        while (left <= mid && right > end) merged.push_back(preSum[left++]);
        for (int i = 0; i < merged.size(); i++) preSum[begin + i] = merged[i];
        
        return cnt;
    }
};
