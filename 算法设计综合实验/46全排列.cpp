class Solution {
public:
    vector<vector<int>> allOrder;
    vector<int> order;

    vector<vector<int>> permute(vector<int>& nums) {
        allOrder.clear();
        order.clear();
        vector<bool> selected(nums.size(), false);
        backtracking(nums, selected);
        return allOrder;
    }

    void backtracking(vector<int>& nums, vector<bool>& selected) {
        if (order.size() == nums.size()) {
            allOrder.push_back(order);
            return;
        }
        for (int i = 0; i < nums.size(); i++) {
            if (selected[i]) continue;
            selected[i] = true;
            order.push_back(nums[i]);
            backtracking(nums, selected);
            order.pop_back();
            selected[i] = false;
        }
    }
};
