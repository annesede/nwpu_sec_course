class Solution {
public:
    vector<TreeNode*> generateTrees(int n) {
        if (!n) return {};
        return backtracking(1, n);
    }

    vector<TreeNode*> backtracking(int begin, int end) {
        if (begin > end) return {nullptr};
        vector<TreeNode*> subtree;
        for (int i = begin; i <= end; i++) {
            vector<TreeNode*> left = backtracking(begin, i - 1);
            vector<TreeNode*> right = backtracking(i + 1, end);
            for (int jl = 0; jl < left.size(); jl++) {
                for (int jr = 0; jr < right.size(); jr++) {
                    TreeNode* root = new TreeNode(i);
                    root->left = left[jl];
                    root->right = right[jr];
                    subtree.push_back(root);
                }
            }
        }
        return subtree;
    }
};
