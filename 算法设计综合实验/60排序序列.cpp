class Solution {
    const vector<int> factorial = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
public:
    string getPermutation(int n, int k) {
        string order;
        string seq = string("123456789").substr(0, n);
        --k;
        while(k) {
            unsigned int remain = k / factorial[n - 1];
            order.push_back(seq[remain]);
            seq.erase(seq.begin() + remain);
            k %= factorial[n - 1];
            --n;
        }
        return order + seq;
    }
};
