class Solution {
public:
    unordered_map<string, priority_queue<string, vector<string>, greater<string>>> edge;
    vector<string> stack;

    vector<string> findItinerary(vector<vector<string>>& tickets) {
        for (int i = 0; i < tickets.size(); i++)
            edge[tickets[i][0]].push(tickets[i][1]);
        hierholzer("JFK");

        reverse(stack.begin(), stack.end());
        return stack;
    }

    void hierholzer(string curr) {
        while(edge.count(curr) && edge[curr].size()) {
            string next = edge[curr].top();
            edge[curr].pop();
            hierholzer(next);
        }
        stack.push_back(curr);
    }

};
