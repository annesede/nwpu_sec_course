class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int remains = 0, maxRemains = 0, start = 0;
        for (int i = gas.size() - 1; i >= 0; i--) {
            remains += gas[i] - cost[i];
            if (remains >= maxRemains) {
                maxRemains = remains;
                start = i;
            }
        }
        return remains < 0 ? -1 : start;
    }
};
