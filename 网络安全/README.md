# Rep 抓包分析
报告基本需要通过抓包分析，主要掌握过滤方法。windows 推荐使用 wireshark，[wireshark官方文档](https://www.wireshark.org/docs/)；linux 还可使用 tcpdump，[tcpdump参考手册](https://www.tcpdump.org/manpages/tcpdump.1.html)。

# Lab1 网络迷踪
根据图片推断地理位置，详细教程：[网络迷踪入门指南](https://zhuanlan.zhihu.com/p/555606058)，送分题水平。

# Lab2 IPsec over Kerberos
①安装 windows server 虚拟机和另一个 windows 虚拟机，推荐 windows  2012 serverR2 和 windows 7 专业版；
② Server 升级为域控制器，配置 DNS 服务，启动 Kerberos 服务（配置域后默认开启），客户端加入域；详细教程：[Windows Server 2012 域搭建教程](https://zhuanlan.zhihu.com/p/555606058)，[Windows环境下Kerberos的配置](https://blog.csdn.net/Charonmomo/article/details/88957820)；
③配置 IPsec （默认使用 Kerberos 协议）；详细教程：[配置IPsec安全策略](https://blog.51cto.com/yttitan/1571117)。

# Lab3 端口扫描
安装并使用nmap进行端口扫描，[nmap参考指南](https://nmap.org/man/zh/)。

# Lab4 防火墙
使用 Linux IPtables 分别添加 input（入站）/ output（出站）/ forward（转发）规则，并通过另一主机 Ping 进行验证：[IPtables原理](https://blog.csdn.net/shiner_chen/article/details/132104300)；[IPtables参考手册](https://linux.die.net/man/8/iptables)。

# Lab5 灰鸽子远程控制木马
①安装 windows xp 虚拟机（家庭版/专业版）；
②主机启动远控软件，生成木马文件；
③关闭虚拟机防火墙，粘贴木马文件并运行；
④主机验证远控成功。

# Proj 简易端口扫描软件开发
推荐直接使用Python调用scapy包完成：[Python使用Scapy实现端口探测](https://www.lyshark.com/post/1a03a021.html)。
