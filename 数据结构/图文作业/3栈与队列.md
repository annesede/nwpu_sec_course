## 3.4

(1) 栈中的数据元素逆置。
(2) 若栈中存在元素 $e$, 则将其从栈中清除。

## 3.5

任何前 $n$ 个序列中 $S$ 的个数一定大于 $X$ 的个数。
假定前 $n$ 个操作都相同，从第 $n+1$ 个操作开始，为序列不同的起始操作点。由于前$n$ 个操作相同，故此时两个栈的存储情况完全相同，假设此时栈顶元素均为 $a$。第 $n+1$ 个操作不同，不妨假设第一个序列的第 $n+1$ 个操作为 $S$，第二个序列的第 $n+1$ 个操作为 $X$。第一个序列为入栈操作，假设将 $b$ 压栈，则第一个序列的输出顺序一定是先 $b$ 后 $a$；而第二个序列将 $a$ 退栈，则其输出顺序一定是先 $a$ 后 $b$。由此说明两个不同的合法栈操作序列的输出元素的序列一定不同。

## 3.15

```cpp
class DStack {
    ElemType *top[2];
    ElemType *p;
    int stacksize;
    int di;

public:
    DStack(int m) {
        p=new ElemType[m];
        if(!p) exit(OVERFLOW);
        top[0]=p+m/2;
        top[1]=top[0];
        stacksize=m;
    }

    ~DStack() {delete p;}

    void Push(int i,ElemType x) {
        di=i;
        if(di==0){
            if(top[0]>=p) *top[0]--=x;
            else cerr<<"Stack overflow!";
        }
        else{
            if(top[1]<p+stacksize-1) *++top[1]=x;
            else cerr<<"Stack overflow!";
        }
    }

    ElemType Pop(int i) {
        di=i;
        if(di==0){
            if(top[0]<top[1]) return *++top[0];
            else cerr<<"Stack empty!";
        }else{
            if(top[1]>top[0]) return *top[1]--;
            else cerr<<"Stack empty!";
        }
        return OK;
        }
};
```

```c
typedef struct NodeType {
    ElemType data;
    NodeType *next;
}NodeType,*LinkType;

typedef struct {
    LinkType top;
    int size;
}Stack;

void InitStack(Stack &s) {
    s.top=NULL;
    s.size=0;
}

void DestroyStack(Stack &s) {
    LinkType p;
    while(s.top) {
        p=s.top;
        s.top=p->next;
        delete p;
        s.size--;
    }
}

void ClearStack(Stack &s) {
    LinkType p;
    while(s.top) {
        p=s.top;
        s.top=p->next;
        delete p;
        s.size--;
    }
}

int StackLength(Stack s) {
    return s.size;
}

Status StackEmpty(Stack s) {
 if(s.size==0) return TRUE;
 else return FALSE;
}

Status GetTop(Stack s,ElemType &e) {
    if(!s.top) return ERROR;
    else{
        e=s.top->data;
        return OK;
    }
}

Status Push(Stack &s,ElemType e) {
    LinkType p;
    p=new NodeType;
    if(!p) exit(OVERFLOW);
    p->next=s.top;
    s.top=p;
    p->data=e;
    s.size++;
    return OK;
}

Status Pop(Stack &s,ElemType &e) {
    LinkType p;
    if(s.top){
        e=s.top->data;
        p=s.top;
        s.top=p->next;
        delete p;
        s.size--;
    }
    return OK;
}

void StackTraverse(Stack s,Status (*Visit)(ElemType e)) {
    LinkType p;
    p=s.top;
while(p) Visit(p->data);
}
```

面向过程是一种自顶向下的编程。
优点：性能比面向对象高，因为类调用时需要实例化，开销比较大，比较消耗资源；比如单片机、嵌入式开发、 Linux/Unix等一般采用面向过程开发，性能是最重要的因素。
缺点：没有面向对象易维护、易复用、易扩展。

面向对象是将事物高度抽象化。面向对象必须先建立抽象模型，之后直接使用模型就行了。
优点：易维护、易复用、易扩展，由于面向对象有封装、继承、多态性的特性，可以设计出低耦合的系统，使系统 更加灵活、更加易于维护。面向对象技术具有程序结构清晰，自动生成程序框架，实现简单，可有效地减少程序的维护工作量，代码重用率高，软件开发效率高等优点。
缺点：因为类调用时需要实例化，开销比较大，比较消耗资源，性能比面向过程低。

## 3.21

```c
char calc[1005] = "";
char output[1005] = "";

char stack[1005] = {};
int top = -1;

bool isPrior(char curr, char topCh) {
	return (((curr == '*') || (curr == '/')) && ((topCh == '+') || (topCh == '-'))) ||
		(topCh == '(') || (curr == '(');
}

void suffix(void) {
	char *ch = calc;
	while (*ch) {
		if (*ch == '\n') break;
		// 字母跳过
		if (isalpha(*ch)) strncat(output, ch, 1);
		// 入栈: 栈空; 优先级严格大于栈顶; 栈顶为左括号; 左括号
		else if (top == -1 || isPrior(*ch, stack[top]))
			stack[++top] = *ch;
		// 出栈: 遇到右括号时出栈到左括号
		else if (*ch == ')') {
			while (stack[top] != '(') {
				strncat(output, &stack[top], 1);
				--top;
			}
			// 左括号出栈
			--top;
		// 出栈: 优先级等于或小于栈顶时, 出栈到严格大于或左括号或栈空
		} else {
			while (top != -1 && !isPrior(*ch, stack[top])) {
				strncat(output, &stack[top], 1);
				--top;
			}
			// 入栈
			stack[++top] = *ch;
		}
		++ch;
	}
	// 栈中剩余符号全部出栈
	while (top != -1) {
		strncat(output, &stack[top], 1);
		--top;
	}
}

int main() {
	fgets(calc, 1000, stdin);
	suffix();
	printf("%s", output);
	return 0;
}
```

## 3.28

```c
typedef int ElemType;

typedef struct NodeType{
    ElemType data;
    NodeType *next;
}QNode,*QPtr;

typedef struct{
    QPtr rear;
    int size;
}Queue;

Status InitQueue(Queue& q) {
    q.rear=NULL;
    q.size=0;
    return OK;
}

Status EnQueue(Queue& q,ElemType e) {
    QPtr p;
    p=new QNode;
    if(!p) return FALSE;
    p->data=e;
    if(!q.rear){
        q.rear=p;
        p->next=q.rear;
    }
    else{
        p->next=q.rear->next;
        q.rear->next=p;
        q.rear=p;
    }
    q.size++;
    return OK;
}

Status DeQueue(Queue& q,ElemType& e) {
    QPtr p;
    if(q.size==0)return FALSE;
    if(q.size==1){
        p=q.rear;
        e=p->data;
        q.rear=NULL;
        delete p;
    }
    else{
        p=q.rear->next;
        e=p->data;
        q.rear->next=p->next;
        delete p;
    }
    q.size--;
    return OK;
}
```

## 3.29

```c
#define MaxQSize 4

typedef int ElemType;

typedef struct{
    ElemType *base;
    int front;
    int rear;
    Status tag;
} Queue;

Status InitQueue(Queue& q) {
    q.base=new ElemType[MaxQSize];
    if(!q.base) return FALSE;
    q.front=0;
    q.rear=0;
    q.tag=0;
    return OK;
}

Status EnQueue(Queue& q,ElemType e) {
    if(q.front==q.rear&&q.tag) return FALSE;
    else{
        q.base[q.rear]=e;
        q.rear=(q.rear+1)%MaxQSize;
        if(q.rear==q.front)q.tag=1;
    }
    return OK;
}

Status DeQueue(Queue& q,ElemType& e) {
    if(q.front==q.rear&&!q.tag)return FALSE;
    else{
        e=q.base[q.front];
        q.front=(q.front+1)%MaxQSize;
        q.tag=0;
    }
    return OK;
}
```

设标志节省存储空间，但运行时间较长。不设标志则正好相反。