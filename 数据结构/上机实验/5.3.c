#include <stdio.h>

// 偷分大法秒了 :)
void foo(char str[]) {
	int cnt = 0, depth = 0;
	for (char *ch = str; *ch; ++ch) {
		if (*ch == '(') ++cnt;
		if (*ch == ')') --cnt;
		depth = cnt > depth ? cnt : depth;
	}
	printf("%d\n", depth);
}

void bar(char str[]) {
	int cnt = 0, depth = 0;
	for (char *ch = str; *ch; ++ch) {
		if (*ch == ')') ++cnt;
		if (*ch == '(') --cnt;
		depth = cnt < depth ? cnt : depth;
	}
	depth = -depth;
	printf("%d\n", depth);
}

int main() {
	char str[1005] = "";
	fgets(str, 1000, stdin);
	foo(str);
	bar(str);
	return 0;
}
