// 本题只能使用 fgets, 使用 scanf 和 getchar 均会 WA
#include <stdio.h>
#include <stdbool.h>

char str[1005] = "";
char stack[1005] = {};
int top = -1;

bool isMatched(void) {
	fgets(str, 1000, stdin);
	char *ch = str;
	while(*ch) {
		switch (*ch) {
			case '(': case '[': case '{':{
				stack[++top] = *ch;
				break;
			}
			case ')': {
				if (stack[top--] != '(')
					return false;
				break;
			}
			case ']': {
				if (stack[top--] != '[')
					return false;
				break;
			}
			case '}': {
				if (stack[top--] != '{')
					return false;
				break;
			}
			default: break;
		}
		++ch;
	}
	if (top == -1) return true;
	return false;
}

int main () {
	if (isMatched()) printf("yes");
	else printf("no");
	return 0;
}
