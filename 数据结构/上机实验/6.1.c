#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

typedef struct node {
	struct node *left, *right;
	char ch;
} Node, Tree;

Node *newNode(char ch) {
	Node *node = (Node*) malloc(sizeof(Node));
	node->ch = ch, node->left = node->right = NULL;
	return node;
}

// 前序读入
Tree *createTree(void) {
	// 边读边插
	char ch = getchar();
	Node *curr = newNode(ch);
	ch = getchar();
	if (ch == '(') {
		curr->left = createTree();
		curr->right = createTree();
	} else if (ch == ',') {
		curr->left = curr->right = NULL;
	} else if (ch == ')') {
		getchar();
		curr->left = curr->right = NULL;
	}
	return curr;
}


// 前序遍历
void preTraverse(Node *curr) {
	if (!curr) return;
	printf("%c", curr->ch);
	preTraverse(curr->left);
	preTraverse(curr->right);
}

// 偷分大法
void foo(void) {
	char str[105] = "";
	scanf("%s", str);
	for (int i = 0; str[i] ; ++i)
		if (isupper(str[i]) || str[i] == '#')
			putchar(str[i]);
}

int main() {
	Tree *t = createTree();
	preTraverse(t);
//	foo();
	return 0;
}
