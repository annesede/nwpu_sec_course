#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	char ch;
	struct node *next, *prev;
	int freq;
} Node, List;

List *init(int n) {
	List *s = (List*) malloc(sizeof(List));
	Node *head = s; int tmp = 1;
	s->next = s, s->prev = s, s->ch = ' ', s->freq = 0;
	while (tmp <= n) {
		char ch = getchar();
		if (ch != ' ' && ch != '\n') {
			Node *node = (Node*) malloc(sizeof(Node));
			node->ch = ch, node->freq = 0;
			node->prev = head->prev, node->next = head;
			head->prev->next = node, head->prev = node;
			++tmp;
		}
	}
	return s;
}

void update(List *s, int m) {
	int tmp = 1;
	while(tmp <= m) {
		char ch = getchar();
		if (ch != ' ' && ch != '\n') {
			for (Node *curr = s->next; curr != s; curr = curr->next)
				if (ch == curr->ch) {
					++curr->freq; break;
				}
			++tmp;
		}
	}
}

// 插入排序(稳定)
void sort(List *s) {
	Node *curr = s->next->next;
	while (curr != s) {
		Node *tail = curr->prev, *tmp = curr->next;
		while (tail != s && curr->freq > tail->freq) tail = tail->prev;
		curr->prev->next = curr->next, curr->next->prev = curr->prev;
		curr->prev = tail, curr->next = tail->next;
		tail->next->prev = curr, tail->next = curr;
		curr = tmp;
	}
}

void traverse(List *s) {
	for (Node *curr = s->next; curr != s; curr = curr->next)
		printf("%c ", curr->ch);
	printf("\n");
}

int main () {
	int n, m;
	scanf("%d %d", &n, &m);
	List *s = init(n);
	update(s, m);
	sort(s);
	traverse(s);
	return 0;
}
