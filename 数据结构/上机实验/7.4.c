#include <stdio.h>
#include <stdbool.h>

#define MAXSIZE 100
#define INF 0x3f3f3f3f

int vn = 0, en = 0;
int dist[MAXSIZE + 1][MAXSIZE + 1] = {};

bool isVisited[MAXSIZE + 1] = {};
int len[MAXSIZE + 1] = {};

void init(void) {
	scanf("%d %d", &vn, &en);
	for (int i = 1; i <= vn; ++i)
		for (int j = 1; j <= vn; ++j) dist[i][j] = INF;
	for (int i = 1; i <= en; ++i) {
		int v1, v2, e;
		scanf("%d %d %d", &v1, &v2, &e);
		dist[v1][v2] = e;
	}
}

void dijkstra(void) {
	for (int i = 1; i <= vn; ++i) len[i] = dist[1][i];
	isVisited[1] = true;
	for (int i = 1; i <= vn - 1; ++i) {
		int minLen = INF, v = i + 1;
		for (int j = 1; j <= vn; ++j)
			if (!isVisited[j] && len[j] < minLen)
				minLen = len[j], v = j;
		isVisited[v] = true;
		// 输出时将 INF 按照题目要求换为 -1
		int lenAns = len[v] == INF ? -1 : len[v];
		printf("%d %d %d\n", 1, v, lenAns);
		for (int j = 1; j <= vn; ++j)
			if (!isVisited[j] && dist[v][j] != INF && len[j] > len[v] + dist[v][j])
				len[j] = len[v] + dist[v][j];
	}
}

int main () {
	init();
	dijkstra();
	return 0;
}
