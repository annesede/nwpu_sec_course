#include <stdio.h>
#include <stdlib.h>

int arr[1005] = {};

typedef struct node {
	int val;
	struct node *next;
} Node, List;

List *init(int n) {
	List *s = (List*) malloc(sizeof(List));
	Node *tail = s;
	s->next = NULL, s->val = -1;
	for (int i = 0; i < n; ++i) {
		Node *node = (Node*) malloc(sizeof(Node));
		scanf("%d", &arr[i]);
		node->val = arr[i], node->next = NULL;
		tail->next = node, tail = node;
	}
	return s;
}

void reverse(List *s, int n) {
	// 三指针迭代
	Node *prev = NULL, *curr = s->next;
	while (curr) {
		Node *next = curr->next;
		curr->next = prev;
		prev = curr, curr = next;
	}
	s->next = prev;

	int begin = 0, end = n - 1;
	while (begin < end) {
		int tmp = arr[begin];
		arr[begin] = arr[end], arr[end] = tmp;
		++begin, --end;
	}
}

void traverse(List *s, int n) {
	// 会尾判空格
	for (Node *curr = s->next; curr; curr = curr->next) {
		if (!curr->next) printf("%d", curr->val);
		else printf("%d ", curr->val);
	}
	printf("\n");
	for (int i = 0; i < n; ++i) {
		if (i == n - 1) printf("%d", arr[i]);
		else printf("%d ", arr[i]);
	}
	printf("\n");
}

int main () {
	int n;
	scanf("%d", &n);
	List *s = init(n);
	reverse(s, n);
	traverse(s, n);
	return 0;
}
