#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	struct node *left, *right;
	char ch;
} Node, Tree;

int cnt = 0;

Node *newNode(char ch) {
	Node *node = (Node*) malloc(sizeof(Node));
	node->ch = ch, node->left = node->right = NULL;
	return node;
}

Tree *createTree(void) {
	char ch = getchar();
	Node *curr = NULL;
	if (ch != '#') {
		curr = newNode(ch);
		curr->left = createTree();
		curr->right = createTree();
	}
	return curr;
}

void foo(void) {
	char str[105] = "";
	scanf("%s", str);
	for (int i = 0; str[i]; ++i)
		if (str[i] == '#' && str[i + 1] == '#') ++cnt, ++i;
	printf("%d\n", cnt);
}

void preTraverse(Node *curr) {
	if (!curr) return;
	// 遍历并判断
	if (!curr->left && !curr->right) ++cnt;
	preTraverse(curr->left);
	preTraverse(curr->right);
}

int main() {
	Tree *t = createTree();
	preTraverse(t);
	printf("%d\n", cnt);
//	foo();
	return 0;
}
