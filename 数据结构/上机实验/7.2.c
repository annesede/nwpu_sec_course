#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXSIZE 100

typedef struct node {
	int v;
	struct node *next;
} Node, Graph;

bool isVisited[MAXSIZE] = {};

int queue[MAXSIZE] = {};
int front = 0, rear = 0;

Graph *createGraph(int n, int m) {
	Graph *g = (Graph*) malloc(sizeof(Graph) * (n + 1));
	g[0].v = -1, g[0].next = NULL;
	for (int i = 1; i <= n; ++i) {
		scanf("%d", &g[i].v);
		g[i].next = NULL;
	}
	for (int i = 1; i <= m; ++i) {
		int v1, v2;
		scanf("%d %d", &v1, &v2);
		Node* newNode = (Node*) malloc(sizeof(Node));
		newNode->v = v2, newNode->next = NULL;
		Node *tail = &g[v1];
		while (tail->next) tail = tail->next;
		tail->next = newNode;
	}
	return g;
}

bool bfs(Graph *g, int v1, int v2) {
	queue[rear++] = v1; isVisited[v1] = true;
	while (front != rear) {
		v1 = queue[front++];
		// 循环队列
		front = front == (MAXSIZE - 1) ? 0 : front;
		Node *curr = &g[v1];
		while (curr) {
			if (curr->v == v2) return true;
			if (!isVisited[curr->v]) {
				queue[rear++] = curr->v;
				// 循环队列
				rear = rear == (MAXSIZE - 1) ? 0 : rear;
				isVisited[curr->v] = true;
			}
			curr = curr->next;
		}
	}
	return false;
}

int main() {
	int n, m;
	scanf("%d %d", &n, &m);
	Graph *g = createGraph(n, m);
	int v1, v2;
	scanf("%d %d", &v1, &v2);
	if (bfs(g, v1, v2)) puts("yes");
	else puts("no");
	return 0;
}
