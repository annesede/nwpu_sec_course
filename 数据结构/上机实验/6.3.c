#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	struct node *left, *right;
	char ch;
} Node, Tree;

Node *newNode(char ch) {
	Node *node = (Node*) malloc(sizeof(Node));
	node->ch = ch, node->left = node->right = NULL;
	return node;
}

Tree *createTree(void) {
	char ch = getchar();
	Node *curr = NULL;
	if (ch != '#') {
		curr = newNode(ch);
		curr->left = createTree();
		curr->right = createTree();
	}
	return curr;
}

// 中序输出
void preTraverse(Node *curr) {
	if (!curr) return;
	preTraverse(curr->left);
	printf("%c", curr->ch);
	preTraverse(curr->right);
}

int main() {
	Tree *t = createTree();
	preTraverse(t);
	return 0;
}
