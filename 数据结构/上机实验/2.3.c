#include <stdio.h>
#include <stdlib.h>

int n, m1, m2;
int arr1[1005] = {};
int arr2[1005] = {};

typedef struct node {
	int val;
	struct node *next;
} Node, List;

List *init(void) {
	scanf("%d %d %d", &n, &m1, &m2);
	List *s = (List*) malloc(sizeof(List));
	Node *tail = s;
	s->next = NULL, s->val = -1;
	for (int i = 0; i < n; ++i) {
		Node *node = (Node*) malloc(sizeof(Node));
		scanf("%d", &node->val);
		node->next = NULL, tail->next = node, tail = node;
	}
	for (int i = 0; i < m1; ++i)
		scanf("%d", &arr1[i]);
	for (int i = 0; i < m2; ++i)
		scanf("%d", &arr2[i]);
	return s;
}

void delete(List *s) {
	// 三指针遍历
	int h1 = 0, h2 = 0;
	Node *curr = s->next;
	while (h1 < m1 && h2 < m2) {
		if (arr1[h1] < arr2[h2]) ++h1;
		else if (arr1[h1] > arr2[h2]) ++h2;
		else {
			int val = arr1[h1];
			while (curr->next) {
				if (curr->next->val == val) {
					Node* tmp = curr->next;
					curr->next = tmp->next;
					free(tmp);
				} else if (curr->next->val < val)
					curr = curr->next;
				else break;
			}
			++h1, ++h2;
		}
	}
}

void traverse(List *s) {
	for (Node *curr = s->next; curr; curr = curr->next)
		printf("%d ", curr->val);
	printf("\n");
}

int main () {
	List *s = init();
	delete(s);
	traverse(s);
	return 0;
}
