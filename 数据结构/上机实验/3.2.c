#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

char calc[1005] = "";
char output[1005] = "";

char stack[1005] = {};
int top = -1;

bool isPrior(char curr, char topCh) {
	return (((curr == '*') || (curr == '/')) && ((topCh == '+') || (topCh == '-'))) ||
		(topCh == '(') || (curr == '(');
}

void suffix(void) {
	char *ch = calc;
	while (*ch) {
		if (*ch == '\n') break;
		// 字母跳过
		if (isalpha(*ch)) strncat(output, ch, 1);
		// 入栈: 栈空; 优先级严格大于栈顶; 栈顶为左括号; 左括号
		else if (top == -1 || isPrior(*ch, stack[top]))
			stack[++top] = *ch;
		// 出栈: 遇到右括号时出栈到左括号
		else if (*ch == ')') {
			while (stack[top] != '(') {
				strncat(output, &stack[top], 1);
				--top;
			}
			// 左括号出栈
			--top;
		// 出栈: 优先级等于或小于栈顶时, 出栈到严格大于或左括号或栈空
		} else {
			while (top != -1 && !isPrior(*ch, stack[top])) {
				strncat(output, &stack[top], 1);
				--top;
			}
			// 入栈
			stack[++top] = *ch;
		}
		++ch;
	}
	// 栈中剩余符号全部出栈
	while (top != -1) {
		strncat(output, &stack[top], 1);
		--top;
	}
}

int main() {
	fgets(calc, 1000, stdin);
	suffix();
	printf("%s", output);
	return 0;
}
