#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

char calc[1005] = "";
char output[1005] = "";

// 栈, 何尝不是一种有向无环图?
char stack[1005] = {};
int top = -1;

bool isPrior(char curr, char topCh) {
	return (((curr == '*') || (curr == '/')) && ((topCh == '+') || (topCh == '-'))) ||
		(topCh == '(') || (curr == '(');
}

void suffix(void) {
	char *ch = calc;
	while (*ch) {
		if (*ch == '\n') break;
		if (isalpha(*ch)) strncat(output, ch, 1);
		else if (top == -1 || isPrior(*ch, stack[top]))
			stack[++top] = *ch;
		else if (*ch == ')') {
			while (stack[top] != '(') {
				strncat(output, &stack[top], 1);
				--top;
			}
			--top;
		} else {
			while (top != -1 && !isPrior(*ch, stack[top])) {
				strncat(output, &stack[top], 1);
				--top;
			}
			stack[++top] = *ch;
		}
		++ch;
	}
	while (top != -1) {
		strncat(output, &stack[top], 1);
		--top;
	}
}

int main() {
	fgets(calc, 1000, stdin);
	suffix();
	printf("%s", output);
	return 0;
}

