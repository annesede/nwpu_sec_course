#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
	struct node *left, *right;
	char ch;
} Node, Tree;

Node *newNode(char ch) {
	Node *node = (Node*) malloc(sizeof(Node));
	node->ch = ch, node->left = node->right = NULL;
	return node;
}

char pre[105] = "";
char in[105] = "";

Tree *createTree (int fl, int fr, int il, int ir) {
	if (fl > fr || il > ir) return NULL;
	char rootCh = pre[fl];
	// 每次前序的头部为根节点 (后序则为尾部)
	Node *root = newNode(rootCh);
	int idx = 0;
	for (int i = il; i <= ir; ++i)
		if (in[i] == rootCh) {
			idx = i;
			break;
		}
	int lsSize = idx - il;
	root->left = createTree(fl + 1, fl + lsSize, il, idx - 1);
	root->right = createTree(fl + lsSize + 1, fr, idx + 1, ir);
	return root;
}

// 后序遍历
void postTraverse(Tree *t) {
	if (!t) return;
	postTraverse(t->left);
	postTraverse(t->right);
	printf("%c", t->ch);
}

int main() {
	scanf("%s", pre);
	scanf("%s", in);
	int fLen = strlen(pre);
	int iLen = strlen(in);
	Tree *t = createTree(0, fLen - 1, 0, iLen - 1);
	postTraverse(t);
	return 0;
}
