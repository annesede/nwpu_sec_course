#include <stdio.h>

int arr[105] = {};

void reverse(int left, int right) {
	while (left < right) {
		int tmp = arr[left];
		arr[left] = arr[right], arr[right] = tmp;
		++left, --right;
	}
}

int main() {
	int n, k;
	scanf("%d %d", &n, &k);
	for (int i = 0; i < n; ++i)
		scanf("%d", &arr[i]);
	// 循环移动 = 三次翻转
	reverse(0, n - 1);
	reverse(0, k - 1);
	reverse(k, n - 1);
	for (int i = 0; i < n; ++i)
		printf("%d ", arr[i]);
	printf("\n");
	return 0;
}
