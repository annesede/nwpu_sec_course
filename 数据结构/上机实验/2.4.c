#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int val;
	struct node *next;
} Node, List;

List *init(int n) {
	List *s = (List*) malloc(sizeof(List));
	Node *tail = s;
	s->next = NULL, s->val = -1;
	for (int i = 0; i < n; ++i) {
		Node *node = (Node*) malloc(sizeof(Node));
		scanf("%d", &node->val);
		node->next = NULL, tail->next = node, tail = node;
	}
	return s;
}

List* merge(List* src1, List* src2) {
	if (!src1->next) return src2;
	if (!src2->next) return src1;
	List* dest = (List*) malloc(sizeof(List));
	dest->next = NULL, dest->val = -1;
	Node *curr1 = src1->next, *curr2 = src2->next, *head = dest, *tmp = NULL;
	// 头插逆序, 尾插正序, 以下采用头插法
	while (curr1 && curr2) {
		if (curr1->val < curr2->val) {
			tmp = curr1->next, curr1->next = head->next;
			head->next = curr1, curr1 = tmp;
		} else {
			tmp = curr2->next, curr2->next = head->next;
			head->next = curr2, curr2 = tmp;
		}
	}
	while (curr1) {
		tmp = curr1->next, curr1->next = head->next;
		head->next = curr1, curr1 = tmp;
	}
	while (curr2) {
		tmp = curr2->next, curr2->next = head->next;
		head->next = curr2, curr2 = tmp;
	}
	src1->next = NULL, src2->next = NULL;
	return dest;
}

void traverse(List *s) {
	for (Node *curr = s->next; curr; curr = curr->next)
		printf("%d ", curr->val);
	printf("\n");
}

int main () {
	int n, m;
	scanf("%d %d", &n, &m);
	List *s1 = init(n);
	List *s2 = init(m);
	List *s = merge(s1, s2);
	traverse(s);
	return 0;
}
