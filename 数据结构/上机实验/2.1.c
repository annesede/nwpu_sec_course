#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int val;
	struct node *next;
} Node, List;

List *init(void) {
	List *s = (List*) malloc(sizeof(List));
	Node *tail = s; int n;
	s->next = NULL, s->val = -1;
	scanf("%d", &n);
	for (int i = 0; i < n; ++i) {
		Node *node = (Node*) malloc(sizeof(Node));
		scanf("%d", &node->val);
		node->next = NULL, tail->next = node, tail = node;
	}
	return s;
}

void insert(List *s) {
	Node *node = (Node*) malloc(sizeof(Node)), *curr = s;
	scanf("%d", &node->val);
	while (curr->next) {
		if (node->val > curr->next->val) curr = curr->next;
		else break;
	}
	node->next = curr->next, curr->next = node;
}

void traverse(List *s) {
	for (Node *curr = s->next; curr; curr = curr->next)
		printf("%d ", curr->val);
	printf("\n");
}

int main () {
	List *s = init();
	insert(s);
	traverse(s);
	return 0;
}
