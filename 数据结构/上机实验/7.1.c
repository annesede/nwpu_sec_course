#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXSIZE 1000

typedef struct node {
	int v;
	struct node *next;
} Node, Graph;

// 记录访问情况，避免有环时死循环
bool isVisited[MAXSIZE] = {};
// 外部记录是否能遍历到
bool isPath = false;

Graph *createGraph(int n, int m) {
	// 指针数组记录邻接表
	Graph *g = (Graph*) malloc(sizeof(Graph) * (n + 1));
	g[0].v = -1, g[0].next = NULL;
	for (int i = 1; i <= n; ++i) {
		scanf("%d", &g[i].v);
		g[i].next = NULL;
	}
	for (int i = 1; i <= m; ++i) {
		int v1, v2;
		scanf("%d %d", &v1, &v2);
		Node* newNode = (Node*) malloc(sizeof(Node));
		newNode->v = v2, newNode->next = NULL;
		Node *tail = &g[v1];
		while (tail->next) tail = tail->next;
		tail->next = newNode;
	}
	return g;
}

void dfs(Graph* g, int v1, int v2) {
	Node *curr = &g[v1];
	while (curr) {
		if (curr->v == v2) isPath = true;
		if (!isVisited[curr->v]) {
			isVisited[curr->v] = true;
			dfs(g, curr->v, v2);
		}
		curr = curr->next;
	}
	return;
}

int main() {
	int n, m;
	scanf("%d %d", &n, &m);
	Graph *g = createGraph(n, m);
	int v1, v2;
	scanf("%d %d", &v1, &v2);
	dfs(g, v1, v2);
	if (isPath) puts("yes");
	else puts("no");
	return 0;
}
