#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int val;
	struct node *next;
} Node, List;

List *init(int n) {
	List *s = (List*) malloc(sizeof(List));
	Node *tail = s;
	s->next = NULL, s->val = -1;
	for (int i = 0; i < n; ++i) {
		Node *node = (Node*) malloc(sizeof(Node));
		scanf("%d", &node->val);
		node->next = NULL, tail->next = node, tail = node;
	}
	return s;
}

void delete(List *s, List *s1, List *s2) {
	// 三指针遍历
	Node *h = s->next, *h1 = s1->next, *h2 = s2->next;
	while (h1->next && h2->next) {
		if (h1->val < h2->val) h1 = h1->next;
		else if (h1->val > h2->val) h2 = h2->next;
		else {
			int val = h1->val;
			while (h->next) {
				if (h->next->val == val) {
					Node* tmp = h->next;
					h->next = tmp->next;
					free(tmp);
				} else if (h->next->val < val)
					h = h->next;
				else break;
			}
			h1 = h1->next, h2 = h2->next;
		}
	}
}

void traverse(List *s) {
	for (Node *curr = s->next; curr; curr = curr->next)
		printf("%d ", curr->val);
	printf("\n");
}

int main () {
	int n, m1, m2;
	scanf("%d %d %d", &n, &m1, &m2);
	List *s = init(n);
	List *s1 = init(m1);
	List *s2 = init(m2);
	delete(s, s1, s2);
	traverse(s);
	return 0;
}
