复习自用, 仅供参考~
# 网络安全
- **网络** [76-1]: 由计算机, 其他信息终端, 相关设备组成的按照一定规则, 程序对信息进行**收集**, **存储**, **传输**, **交换**, **处理**的系统.
  1969 美国国防部-阿帕网 $\to$ 1980s TCP/IP协议-因特网 $\to$ 1990s 万维网 $\to$ 移动通信.
  1960s 美军通信保密"信息安全" $\to$ 1990s 网络安全是核心, 内涵有交集, **密码+协议**.
   - 网络运营者 [76-3]: 网络的**所有者**, **管理者**, 网络**服务提供者**.
  - 网络数据 [76-4]: **收集**, **存储**, **传输**, **处理**, **产生**的电子数据.
- **网络安全** [76-2]: 通过采取必要措施, 防范对网络的**攻击**, **侵入**, **干扰**, **破坏**, **非法使用**, **意外事故**, 使网络处于稳定可靠运行的状态, 保障网络数据的**完整性**, **保密性**, **可用性**的能力.
  战略: 安全是发展前提, 发展是安全保障; 没有网络安全就没有国家安全, 没有信息化就没有现代化.
  社会资源: 资产财富, 基础设施, 国家主权面临威胁
  技术: **图灵计算原理** (计算科学, 少攻防理念); **冯诺依曼架构** (体系结构, 缺防护部件); **重大工程应用** (计算模式, 无安全服务).
  - 特征: 逻辑上**保密性**, **完整性**, **可用性**, **可控性**, **可审查性**; 现实上**高风险性**, **难防范性**, **隐蔽性**.
  - 目标: 国家层面**检测**, **防御**, **处置**, **惩罚**; 社会层面齐抓共管, **健康发展**; 个人层面遵纪守法, **为网络行为负责**.
- 网络安全法律体系: **演绎**或归纳
  - 法律: 综合性法律 (《刑法》, 《民法典》, 《著作权法》, 《未成年保护法》, 《治安管理处罚法》); 专门性规范 (《关于维护互联网安全的决定》, 《关于加强网络信息保护的决定》); 2016.11.7 十二届人大常委会第二十四次会议通过《网络安全法》, 2017.6.1实施, 7章79条.
  - 法规: 国务院颁布
  - 规章: 《计算机信息网络系统安全保密管理暂行规定》, 《计算机病毒防治管理办法》, 《公用计算机互联网国际联网管理办法》, 《信息安全等级保护管理办法》.
  - 标准: **技术与机制类**, **管理类**, **评估类**.
- 网络安全与社会联系
  - 网络发展速度快, 但防御能力弱
  - 外部威胁: **网络攻击破坏**, **网络情报窃密**, **网络意识形态渗透**, **网络战争**.
  - 内部威胁: **木马和僵尸网络**, **个人信息泄露**, **移动互联网恶意程序**, **拒绝服务攻击**, **网络安全漏洞**, **网页仿冒**, **网页篡改**.
  - 重大机遇: 信息传播的**新渠道**, 生产生活的**新空间**, 经济发展的**新引擎**, 文化繁荣的**新载体**, 社会治理的**新平台**, 交流合作的**新纽带**, 国家主权的**新疆域**.
  - 严峻挑战: 网络渗透危害**政治安全**, 网络攻击威胁**经济安全**, 网络有害信息侵蚀**文化安全**, 网络恐怖和违法犯罪破坏**社会安全**.
- 维护网络安全意义
  - 国际形式: 国家和区域间**网络安全合作**不断加强, 网络空间**战略和政策升级**调整, 网络冲突和攻击成为**国家间对抗的主要形式**, 注重**安全保障与攻击能力**双向提升, 加强对**数据资源跨境传输**的管控.
  - 国家层面: 关系**国家安全和主权**, **社会的稳定**, **民族文化的继承和发扬**的重要问题.
  - 社会层面: 对**社会经济发展**有重要实际价值.
  - 个人层面: 矛盾突出为个人**隐私泄露**, 个人**财产损失**.
# 国家网络安全战略格局
 - 美国网络战争法典: **《塔林手册》**"95条"-"谁制定规则谁就掌握主动权".
   - 主要内容: 由国家发起的网络攻击行为必须避免敏感的民用目标, 允许通过**常规打击**来反击造成人员死亡和重大财产损失的网络攻击行为.
   - 意义: 网络攻击**国际法典**的首次尝试, **建议性指南**, 抢占网络战争**规则制定权**.
- 《网络安全法》起草原则: "**第二类**"生存空间, "**第五大**"作战领域.
  - **坚持从国请出发**: 确立保障网络安全的**基本制度框架**, 对网络自身安全作出**制度性安排**, 对信息内容作出**规范性规定**.
  - **坚持问题导向**: 网络安全管理的**基础性法律**, 将成熟好做法作为**制度确定**, 对确有必要但尚缺乏实践经验的制度安排作出**原则性规定**.
  - **坚持安全与发展并重**.
- 《网络安全法》战略格局: 以总体国家安全观为指导; **和平** (**防止信息技术滥用**); **安全** (**核心技术装备安全可控**); **开放** (**成果共享**); **合作** (**网络空间命运共同体**); **有序**(**法律体系规范标准逐步建立**). 
- 《网络安全法》基本原则
  - 尊重维护**网络空间主权** [1].
  - 和平利用网络空间: 《联合国宪章》相互尊重, 平等相待, 求同存异, 包容互信.
  - 依法治理网络空间: 依法治网, 依法办网, 依法上网; **防御, 控制, 惩治三位一体**; 积极防御, 程序控制, 司法补救.
  - 统筹网络安全与发展: **基础设施**, **技术创新**驱动; **安全体系**, **保护能力**两翼.
- 《网络安全法》亮点
  - 明确**网络空间主权**.
  - 明确**网络运营者**安全义务 (**对刑法补充**).
  - 明确**网络产品和服务提供者**的安全义务 (**个人信息保护**).
  - 进一步完善**个人信息保护**规则.
  - 建立**关键信息基础设施**安全保护制度 (**2+8**): 广电, 电信; 银行, 证券, 保险, 民航, 铁路, 电力, 海关, 税务.
  - 确立了关键信息基础设施**重要数据跨境传输**规则 (**跨国公司**, **海外银行**).
- 《国家网络空间安全战略》: 2011 英国, 2013 日本 俄罗斯, 2014 美国, 2016.12.27 我国.
   详细, 系统, 全面, 完整; **对内总指导**, **对外总纲领**.
   - 对**网络空间**进行了系统阐述.
   - 明确了**发展与安全**的关系.
   - 明确了**网络空间主权**的内涵.
   - 明确了和平, 安全, 开放, 合作, 有序的目标.
   - 阐明了**保护关键信息基础设施**的相关主张.
   - 树立了**依法治网**理念.
   - 表明了强化网络空间**国际合作**的立场和责任感.
   - 向国际社会展示出合作开放的**安全理念**.
- 网络安全战略任务: 整体性, 协调性.
  - 坚定捍卫网络空间主权; 防止网络颠覆 (**底线**).
  - 坚决维护国家安全; 防渗透, 防分裂 (**底线**).
  - 保护关键信息基础设施; 关系国计民生 (**保障**).
  - 加强网络文化建设; 践行核心价值观 (**辐射力 感染力**).
  - 打击网络恐怖和违法犯罪; 防恐, 防骗, 防谍, 防黑客 (**惩治**).
  - 完善网络治理体系; 科学化, 规范化 (**根本保证**).
  - 夯实网络安全基础; 互联网+, 大数据, 云计算 (**保障**).
  - 提升网络空间防护能力; 主动防御 (**基本需要**).
  - 强化网络空间国际合作; 和平, 安全, 开放, 合作, 有序 (**必由之路**).
# 网络安全法
- 立法必要性: 网络空间可分为**物理层**, **逻辑层**, **行为体层**; **网络安全问题**日益凸显.
  - 网络入侵, 网络攻击等非法活动猖獗.
  - 非法获取, 泄露, 倒卖公民个人信息.
  - 宣扬恐怖主义, 极端主义, 煽动颠覆国家政权.
  - 党十八届四中全会决定要求完善网络安全保护方面的法律法规.
- 立法指导思想: **坚持从国情出发**, **坚持问题导向**, **坚持安全与发展并重**.
- 出台过程
  - 2015.6 社会公开征求意见.
  - 2016.6.27 增加关键信息基础设施安全保护三大制度: 关键信息基础设施运行**安全保护**制度, 重要数据**境内留存**制度, 网络产品和服务的**安全审查**制度.
  - 2016.10.31 增加三大规定: 推进网络基础设施互联互通, 支持培养网络安全人才; 支持研究机构, 高等学校参与网络安全标准制定; 保障未成年人上网安全.
- 管理制度 [**第二章**]: 国家法律形式; " 1 (**国家网信办**) + X"的监管体制.
- 关键信息基础设施运行安全 [**第三章第二节**]: 国家网络空间**基本制度**.
  - 对公共通信和信息服务, 能源, 交通, 水利, 金融, 公共服务, 电子政务等重要行业和领域实行**重点保护**.
  - 责任划分和追责方式: **国家**, **行业**, **运营者**三个层面.
  - 搭建制度框架: 网络安全**检测预警**, **应急制度**建设.,
  - 配套法规: 《关键信息基础设施安全保护条例》2021.9.1施行; 包括关键信息基础设施**认定**, 运营者**责任义务**, **保障和促进**, **法律责任**.
- 网络信息安全 [**第四章**]
  - 内容: 规范了相关网络安全监管部门的**责权范围**; 明确了个人信息保护相关主体的**法律责任**; 提高了个人对隐私信息的**管控程度**; 增强了**针对侵犯个人信息权益行为的威慑**.
  - 个人信息界定标准: 源自于对信息主体**生物特征**或**社会属性**的描述; 被主要用于对信息主体的行为或其他状况**进行评价**; 使信息主体与他人**相互区别**.
  - 核心价值: **保护人格尊严**, 维护个人在独立和自由 (**拒绝权**), 保护个人隐私 (**控制权**), 维护个人在真实形象 (**维护准确性**); **优化信息资源配置效率**.
- 监测预警与应急处置 [**第五章**]: **制度化**, **法制化**; 网络安全风险**评估**和**应急**工作机制.
  - 三种状态: 网络正常运行, 网络安全风险增大, **发生网络安全事件**.
  - 差距分析框架: 检测与感知, 安全审计, 风险评估; 敏感数据保护, 安全技术, 安全管理.
- 法律责任 [**第六章**]
  - 一般性主体 (**一般责任**): 安全**等级保护**制度 [21], **实名制** [24-1], 网络安全事件**应急预案**及安全**风险处置** [25], **持续安全维护** [22], 禁止设置**恶意程序** [22-1], 禁止从事或协助实施**危害网络安全活动** [27].
  - 关键信息基础设施运营者 (**特别责任**): **更严格**的安全等级保护制度 [34], **数据跨境**转移限制 [37], 采购行为的**安全审查**[35], 采购需签订**安全保密协议**[36].
  - 个人和组织 [46, 47, 48, 67, 68]: 处罚1万起步最高50万, 责任人拘留5日至15日.
# 网络安全法技术解析
- 国家采取措施, **监测**, **防御**, **处置**境内外网络安全风险和威胁.
- 基于**协议**的网络攻击: OSI 7层模型, 物理层, 数据链路层, 网络层, 传输层, 会话层, 表示层, 应用层.
  - TCP SYN 洪范攻击: 1996 首次出现, 待处理队列请求每隔 60s 被清空 (**缓存变体**); 攻击者保持队列填满状态, 导致合法连接请求被拒绝.
  - Ping of Death 攻击 (数据爆头): 攻击者故意发送一个或多个数据容量超过**65535字节的IP数据包**, 实现拒绝服务攻击.
  - SMURF 攻击 (网络堵塞): 攻击者通过将 **ICMP 回应请求和对该请求的回复**塞满网络.
- 基于**系统漏洞**的攻击: 每个运行的服务都是潜在的**系统入口**.
  - 应用程序和应用层的 **DNS 欺骗**; **Internet Explorer 漏洞**; **IIS 服务漏洞**; **Web应用漏洞**.
  - **破坏或修改**重要操作系统文件; **更改**文件内容; 造成操作系统**无法正常运行甚至瘫痪**; 扰乱操作系统正常的**安全性和访问控制**; 植入程序**信息窃取**.
- 网络安全环境: 构建全面的**信息安全和网络安全体系**, **完善安全协议**, **应用访问控制**, **隐私保护**.
- 信息安全体系 (ISO)
  - **安全服务**: 认证服务, 数据保密服务, 访问控制, 抗抵赖服务, 数据完整性服务.
  - **安全机制**: 加密机制, 访问控制机制, 数据签名机制, 数据完整性机制, 业务流填充机制, 鉴别交换机制, 路由控制机制, 公证机制.
  - **安全管理**: 安全服务管理, 安全机制管理.
- 网络安全体系结构: 物联网, 5G 网络, CPS; **层次化**, **虚拟化**, **服务化**; **设计安全协议**, **建立访问控制**, **攻击防护**, **隐私保护**.
  - 传统: 防火墙, 病毒查杀, 入侵监测.
  - **主动免疫可信计算**: 计算运算的同时进行安全防护, **以密码为基因**实施身份识别, 状态度量, 保密存储等功能, 及时**识别“自己”和“非己”**成分, 为网络信息系统培育**免疫能力**.
  - 安全防护效果: 攻击者无法进入, 非授权信息无法获取, 系统和信息无法篡改, 系统工作无法瘫痪, 攻击行为无法抵赖.
  - 发展方向: 移动设备终端安全, 网络设备安全, SDN 安全, CPS 安全, 5G 网络安全, 空天地海无线网络安全.
- 保障网络安全技术手段: **密码学**, **身份认证**, **防火墙**, **安全扫描**.
- 密码学发展: 古典密码 $\to$ 近代 机械密码, 电报通信 $\to$ 现代 分组密码, 序列密码, 公钥密码, 无线通信, 有线通信, 计算机网络.
  密码技术应用: 加密信息, 消息验证, 数字签名, 身份识别.
- 网络安全策略: 制定网络安全事件**应急预案**, 及时处置**系统漏洞**, **计算机病毒**, **网络攻击**, **网络侵入**等安全风险; 发生时立即启动应急预案, 采取相应的**补救措施**, 并按照规定**向有关主管部门报告**.
  开展网络安全**认证**, **检测**, **风险评估**等活动, 发布**系统漏洞**, **计算机病毒**, **网络攻击**, **网络侵入**等网络安全信息应当遵守有关规定.
# 网络安全法律关系
- 法律体系: **宪法**; **实体法**; **程序法**.
- 法律地位: "集合" 的概念; 并非简单的部门法, 部分规定体现在《刑法修正案（九）》《国家安全法》《反恐怖主义法》.
- 与《宪法》关系
  - 《宪法》是**根本大法**, 法律制定和修改以宪法为**依据**, 既是**制定基础**, 又规范和约束法律的**主旨和内容**.
  - 《宪法》是**立法的根本原则**.
  - 《网络安全法》是《宪法》的**基本原则在互联网环境下的体现和表现**.
- 与《刑法》关系
  - 《刑法》规定**犯罪**, **刑事责任**, **刑罚**; 相互衔接, 规范网络犯罪行为.
  - 《刑法》[285, 286, 287, 291] 与 《网络安全法》条款**既有区别**, **又有联系**.
- 与《行政法》关系
  - 国家**网络监管**行为本质属于《行政法》调整范围; 侧重点不同, 行政法规范重点是**国家职权行为**.
  - **网络安全**是《网络安全法》落脚点, **社会效应相同**.
  - 调整的社会关系与调整手段不同: 行政法调整不平等主体间的权利义务关系, 其中一方为国家机关; 《网络安全法》还包括平等主体, 与民法相似, 坚持**平等**, **自愿**, **公平**, **等价有偿原则**, 兼有行政制裁和民事裁判手段.
- 与《民法典》关系
  - 《网络安全法》作为新兴法律, 调整的社会关系独特, 个人信息保护与隐私保护有部分重叠.
  - 调整的社会关系与调整手段不同: 《民法典》调整平等主体间的权利义务关系; 《网络安全法》还包括不平等主体, 行政法律关系由法律直接规定, 不因主体意愿而设立或改变, 通过**命令性**, **权利性行政手段**要求行为主体必须履行的**义务**.
- 与《经济法》关系
  - 信息产业成为促进社会发展的中坚力量, 使得两者相互联系
  - 《经济法》调整不平等主体间的经济关系, 具有行政法特点, 以行为手段为主.
  - 《经济法》调整对象是社会经济关系; 《网络安全法》通过调整网络安全法律关系, 维护互联网交易秩序与安全, 从而间接保护社会经济关系.
- 与《信息法》关系: 是否为独立法律部门有待商榷.
  - 《信息法》调整**以信息为核心**引起的社会关系, 主要包括信息的产生, 采集, 处理, 传输, 利用, 存储; **信息处理工作与互联网已经密不可分**.
  - 两者**交叉部分越来越大**, 法律集合间关系日益紧密.
- 与《科技法》关系: 法律地位并不明确.
  - 互联网技术属于科学技术, 重叠内容较多; 数据管理, 网络安全, 信息基础设施保护均为《网络安全法》重点内容.
  - **技术权属**, **技术成果转化**, **国际技术合作**也经常被互联网行为涉及.
- 法律保障体系
  - 《网络安全法》作为**基本法**; 《宪法》 [40] "通信自由和通信秘密" 为**立法原则和依据**; 《刑法修正案（九）》增加互联网传播虚假信息处罚规定.
  - 专门法: 2000 《关于维护互联网安全的决定》; 2004 《电子签名法》; 2012 《关于加强网络信息保护的决定》.
  - 法律: 《治安管理处罚法》《侵权责任法》《保守国家秘密法》《标准法》《商标法》《专利法》《著作权法》.
  - 法规: 《计算机信息系统安全保护条例》《计算机信息网络国际联网管理暂行规定》《计算机信息网络国际联网安全保
护管理办法》《互联网信息服务管理办法》《计算机软件保护条例》.
- 法律关系: 法律为前提, 调整**网络用户网络行为**过程中形成**的特定权利义务关系**.
  - 构成要素: 主体-**参与者**, **权利享有者**, **义务承担者**, **首要要素**; 客体-权利义务**共同指向的对象**, **主体秩序安全**, **网络系统安全**, **网络数据安全**, **网络信道安全**.
  - 分类: **平权型**-平等主体; **隶属型**-不平等主体.
# 国内外网络安全法律现状
- 美国网络安全法律发展史
  - 1966 全世界第一次入侵银行计算机系统安全事件.
  - 1977 《联邦计算机系统保护法案》, 首次将计算机系统纳入法律保护.
  - 1978 《弗罗里达州计算机犯罪法》, 首次为计算机犯罪定罪量刑.
  - 1981 全美计算机安全中心
  - 1985 《伪造网络信息存取手段及计算机欺诈与滥用法》《联邦禁止利用电子计算机犯罪法》, 加大对计算机犯罪重视程度.
  - 1986 《计算机诈骗和滥用法》, 进一步明确了两类针对信息系统的网络安全犯罪行为.
  - 立法文件类型: **关键基础设施专门法律**; 基本性综合性法律; 行政令总统令形式规定**关键基础设施保护具体内容**.
  - 立法主要内容: 从**定义网络空间安全**入手, 对**保护范围**, **责任**, **具体要求**进行规定, 法律上提出要加强**信息共享**, **标准制定**, **教育培训**, **技术队伍建设**.
- 欧盟网络安全法律发展史
  - 1992 《信息安全框架决议》, 给**存储电子信息**提供有效切实安全保护.
  - 1995-1999 《关于合法拦截电子通讯的决议》《关于采取通过打击全球互联网上的非法和有害内容以促进更安全使用
互联网的多年度共同体行动计划的第276/ 1999/ EC号决定》《关于打击计算机犯罪协议的共同宣言（1999/364/JHA）》
  - 2002 《打击信息系统犯罪的框架决议》《关于打击信息系统犯罪的欧盟委员会框架决议》
  - 2004 《建立欧洲网络和信息安全机构的规则》
  - 2006 《关于建立欧洲信息安全社会的战略—“对话、合作和授权”》
  - 2007 《关于建立欧洲信息安全社会战略的决议》
  - 立法特点: 形成**立法**, **战略**, **实践**的网络安全体系.
    - 制定主体: **超国家主体**-2003 ENISA (欧洲网络信息安全局); **成员国主体**-遵守欧盟统一法律规范, 有权制定本国内部法律.
    - 规范效力: "**硬法**"-强制性规定, **条例**具有**最高法律效力**; "**软法**"-建设性指导性, **决定**, **推荐意见**, **建议**, **不具有法律约束力**.
    - 规范内容: **一般性规定**与**特殊性规定**相结合; "**适当保护**"原则; 协调好**公共利益与私人利益**关系.
  - 英国网络安全法律发展史
    - 早期侧重保护关键性信息基础设施.
    - 2000 《通信监控权法》
    - 2001 《调查权管理法》
    - 2010 《战略防务与安全审查-“在不确定的时代下建立一个安全的英国”》, 将**恶意网络攻击**与国际恐怖主义, 重大事故, 自然灾害, 涉及英国国际军事危机共同列入**安全威胁的最高级别**.
    - 2011 《网络安全战略》
    - 2014 授权提供网络安全专家硕士学位
    - 2015 "网络安全学徒计划"
  - 德国网络安全法律发展史
    - 2001 加入《关于计算机犯罪的协定》
    - 欧盟颁布《欧盟电子签名指令》; 德国随后制定《电子签名框架条件法》《阻碍网页登录法》《电信服务数据保护法》.
  - 俄罗斯网络安全法律发展史: 互联网安全风险最高国家之一.
    - 1995 《宪法》**首次**将网络信息安全纳入法律保护范畴
    - 1997 《俄罗斯国家安全构想》
    - 1999 《俄罗斯网络立法构想》, 加强**个人信息保护**
    - 2000 《国家信息安全学说》
    - 2001 《电子数字签名法》
    - 2009 修正《保护青少年免受对其健康和发展有害信息干扰法》
    - 2012 通过《防止儿童接触有害其健康和发展的信息法》修正案 ("**网络黑名单法**")
    - 2013 总统令-建立国家计算机信息安全机制用来监测, 防范, 消除信息安全隐患, 建立**电脑攻击资料库**.
 - 新加坡网络安全法律发展史
   - 主要涉及**网络内容安全**, **垃圾邮件管控**, **个人信息保护**.
   - 《国内安全法》规定禁止性文件和禁止性出版物, 国家机关拥有**调查权和执法权**.
   - 《广播法》规定网站禁止发布内容.
   - 2012 《个人信息保护法案》收集, 使用, 披露个人资料时**必须征得同意**, **必须提供可接触或修改其信息的渠道**.
   - 《垃圾邮件控制法》《网络行为法》《互联网操作规则》
 - 日本网络安全法律发展史
   - 2011 《刑法》修正, 保存用户 30 天上网通信记录, 视情况可再延长 30 天.
   - 2013 《日本网络安全战略》, 实现"**网络安全立国**"目标.
   - 2014 《网络安全基础法》, **重要基础设施运营商**, **相关企业**, **地方自治**有义务配合; 加强政府于民间在**网络安全领域的协调和运作**.
   - 完善信息安全机构.
 - 印度网络安全法律发展史
   - 2000 《信息技术法》规范**电子商务活动**, 防范与打击**针对计算机和网络的犯罪**; "**母法**"-以其**专门立法为中心**.
   - 2006 增加**新型网络犯罪**规定.
   - 2008 重点规定**网络恐怖主义**.