import os
import datetime
import copy
import random
import time
import math
from collections import defaultdict
import matplotlib.pyplot as plt


def channel_test(sym_num, code_len, code_err_rate, epsilon):
    # output
    test_times = 500
    msg_len = 1000
    test_num = []
    for i in range(test_times):
        test_num.append(i + 1)
    code_eff = []
    avg_bit_err_rate = []
    max_bit_err_rate = []

    # plot picture
    def plot(sgn):
        plt.figure(figsize=(10, 10), dpi=300)
        plt.scatter(test_num, code_eff, c='green', label='code efficiency')
        plt.scatter(test_num, max_bit_err_rate, c='blue', label='max bit error rate')
        plt.scatter(test_num, avg_bit_err_rate, c='red', label='average bit error rate')
        if sgn:
            plt.title(f'JT: M={sym_num}, n={code_len}, p={code_err_rate}, {chr(949)}={epsilon}', fontdict={'size': 20})
        else:
            plt.title(f'MLE: M={sym_num}, n={code_len}, p={code_err_rate}', fontdict={'size': 20})
        plt.yticks(range(0, 110, 10))
        plt.legend()

        # save files
        save_path = r'D:\downloads\lab\fig'
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        mk_file_time = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H%M%S')
        plt.savefig(os.path.join(save_path, mk_file_time))
        plt.show()
        time.sleep(1)

    # test
    def one_test():
        # decimal to binary
        def dec_to_bin(pos):
            px_dec = freq_next[pos]
            bin_bit = ''
            while len(bin_bit) < shannon_code_len[pos]:
                px_dec *= 2
                bin_bit += ('1' if px_dec >= 1 else '0')
                px_dec -= int(px_dec)
            return bin_bit

        # joint distribution
        def hxy_i(x_cnt, y_cnt):
            try:
                xi, yi = x_cnt / code_len, y_cnt / code_len
                est_err_rate = (yi - xi) / (1 - 2 * xi)
                pxy_i = [[xi * (1 - est_err_rate), xi * est_err_rate],
                         [(1 - xi) * est_err_rate, (1 - xi) * (1 - est_err_rate)]]
                return - 1 / code_len * math.log2(pxy_i[0][0] * pxy_i[0][1] * pxy_i[1][0] * pxy_i[1][1])
            except (ValueError, ZeroDivisionError):
                return float('-inf')

        # bit error rate
        def bit_err_rate(decode_msg):
            avg = 0
            err_dict = {}
            err_list = []
            for m in range(msg_len):
                if msg[m] != decode_msg[m]:
                    avg += 1
                    err_cnt = err_dict.get(msg[m])
                    err_dict[msg[m]] = (0 if err_cnt is None else err_cnt) + 1
            for m in err_dict.keys():
                err_list.append(err_dict[m] / freq[m])
            return max(err_list) / msg_len * 100, avg / msg_len * 100

        # uniform distribution generate message
        msg = []
        for j in range(msg_len):
            msg.append(random.randint(0, sym_num - 1))

        # shannon encoding
        # symbol frequency
        freq = {}
        for j in set(msg):
            freq[j] = msg.count(j) / msg_len
        # probability decreasing
        freq_sorted = sorted(freq.values(), reverse=True)
        sym_sorted = [int(j[0]) for j in sorted(freq.items(), reverse=True, key=lambda tup: tup[1])]
        freq_next = [0]
        for j in range(sym_num - 1):
            freq_next.append(freq_next[j] + freq_sorted[j])
        # shannon code length
        shannon_code_len = []
        for j in range(sym_num):
            shannon_code_len.append(int(-math.log2(freq_sorted[j]) + 0.99))
        # generate encoding book
        codeword = []
        for j in range(sym_num):
            codeword.append(dec_to_bin(j).zfill(code_len))
        encode_book = dict(zip(sym_sorted, codeword))

        # encode
        sent = ''
        for j in range(msg_len):
            sent += encode_book[msg[j]]

        # BSC random noise
        received = ''
        for j in range(len(sent)):
            if random.randint(1, 1000) / 1000 <= code_err_rate:
                received += str(int(sent[j]) ^ 1)
            else:
                received += sent[j]

        # adjust codebook
        for key in encode_book.keys():
            encode_book[key] = int(encode_book[key], 2)
        codebook_reverse = {val: key for key, val in encode_book.items()}

        # JT decoder
        if epsilon:
            # JT decoding book
            jt_decode_book = defaultdict(list)
            for j in range(sym_num):
                jt_decode_book[codeword[j].count('0')].append(sym_sorted[j])
            freq_0 = received.count('0') / len(received)
            prob_0 = (freq_0 - code_err_rate) / (1 - 2 * code_err_rate)
            pxy = [[prob_0 * (1 - code_err_rate), prob_0 * code_err_rate],
                   [(1 - prob_0) * code_err_rate, (1 - prob_0) * (1 - code_err_rate)]]
            hxy = (- pxy[0][0] * math.log2(pxy[0][0] / prob_0) - pxy[0][1] * math.log2(pxy[0][1] / prob_0) - pxy[1][0]
                   * math.log2(pxy[1][0] / (1 - prob_0)) - pxy[1][1] * math.log2(pxy[1][1] / (1 - prob_0)) - freq_0
                   * math.log2(freq_0) - (1 - freq_0) * math.log2(1 - freq_0))
            received_len = defaultdict(list)
            for j in range(code_len):
                for k in range(1, max(shannon_code_len)):
                    if hxy - epsilon <= hxy_i(k, j) <= hxy + epsilon:
                        received_len[j].append(k)
            # JT decode
            jt_msg = []
            for j in range(msg_len):
                key = received[j * code_len: (j + 1) * code_len]
                val = codebook_reverse.get(int(key, 2))
                if val is None:
                    val = -1
                    est_len = received_len.get(key.count('0'))
                    if est_len is not None:
                        idx = 0 if len(est_len) <= 1 else random.randint(0, len(est_len) - 1)
                        ran_len = est_len[idx]
                        key = jt_decode_book.get(ran_len)
                        if key is not None:
                            idx = 0 if len(key) <= 1 else random.randint(0, len(key) - 1)
                            val = key[idx]
                jt_msg.append(val)

            # JT bit err rate
            bit_err_rate_jt = bit_err_rate(jt_msg)
            max_bit_err_rate.append(bit_err_rate_jt[0])
            avg_bit_err_rate.append(bit_err_rate_jt[1])

        # MLE decoder
        else:
            # MLE decoding book
            mle_decode_book = copy.deepcopy(codebook_reverse)
            for j in range(2 ** code_len):
                if j not in mle_decode_book:
                    for key in codebook_reverse.keys():
                        if j ^ key not in mle_decode_book.keys():
                            mle_decode_book[j ^ key] = codebook_reverse[key]

            # MLE decode
            mle_msg = []
            for j in range(msg_len):
                mle_msg.append(mle_decode_book[int(received[j * code_len: (j + 1) * code_len], 2)])

            # MLE bit error rate
            bit_err_rate_mle = bit_err_rate(mle_msg)
            max_bit_err_rate.append(bit_err_rate_mle[0])
            avg_bit_err_rate.append(bit_err_rate_mle[1])

        # code efficiency
        hx_msg = 0
        for j in range(sym_num):
            hx_msg -= freq_sorted[j] * math.log2(freq_sorted[j])
        code_eff.append(hx_msg / code_len * 100)

    # test
    for i in range(test_times):
        one_test()
    # output picture
    plot(epsilon)


if __name__ == '__main__':
    # sym_num, code_len, code_err_rate, epsilon
    # epsilon = 0 means MLE
    channel_test(5, 3, 0.45, 0.1)
    channel_test(5, 3, 0.2, 0.1)
    channel_test(5, 3, 0.1, 0.1)
    channel_test(5, 3, 0.05, 0.1)
    channel_test(5, 3, 0.01, 0.1)

    channel_test(5, 3, 0.45, 0.05)
    channel_test(5, 3, 0.2, 0.05)
    channel_test(5, 3, 0.1, 0.05)
    channel_test(5, 3, 0.05, 0.05)
    channel_test(5, 3, 0.01, 0.05)

    channel_test(10, 6, 0.45, 0.1)
    channel_test(10, 6, 0.2, 0.1)
    channel_test(10, 6, 0.1, 0.1)
    channel_test(10, 6, 0.05, 0.1)
    channel_test(10, 6, 0.01, 0.1)

    channel_test(10, 6, 0.45, 0.05)
    channel_test(10, 6, 0.2, 0.05)
    channel_test(10, 6, 0.1, 0.05)
    channel_test(10, 6, 0.05, 0.05)
    channel_test(10, 6, 0.01, 0.05)

    channel_test(20, 8, 0.45, 0.1)
    channel_test(20, 8, 0.2, 0.1)
    channel_test(20, 8, 0.1, 0.1)
    channel_test(20, 8, 0.05, 0.1)
    channel_test(20, 8, 0.01, 0.1)

    channel_test(20, 8, 0.45, 0.05)
    channel_test(20, 8, 0.2, 0.05)
    channel_test(20, 8, 0.1, 0.05)
    channel_test(20, 8, 0.05, 0.05)
    channel_test(20, 8, 0.01, 0.05)

    channel_test(5, 3, 0.45, 0)
    channel_test(5, 3, 0.2, 0)
    channel_test(5, 3, 0.1, 0)
    channel_test(5, 3, 0.05, 0)
    channel_test(5, 3, 0.01, 0)

    channel_test(10, 6, 0.45, 0)
    channel_test(10, 6, 0.2, 0)
    channel_test(10, 6, 0.1, 0)
    channel_test(10, 6, 0.05, 0)
    channel_test(10, 6, 0.01, 0)

    channel_test(20, 8, 0.45, 0)
    channel_test(20, 8, 0.2, 0)
    channel_test(20, 8, 0.1, 0)
    channel_test(20, 8, 0.05, 0)
    channel_test(20, 8, 0.01, 0)

# optimal receiver: https://blog.csdn.net/m0_59638635/article/details/134814686
