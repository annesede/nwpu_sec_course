import tushare as ts
import matplotlib.pyplot as plt
import mplfinance as mpf
from sklearn.cluster import KMeans
import pandas as pd

# 设置Tushare的API令牌
ts.set_token('api value') # 替换为个人API
pro = ts.pro_api()


# 获取股票历史数据
def get_stock_data(stock_code, start_date, end_date):
    df = pro.daily(ts_code=stock_code, start_date=start_date, end_date=end_date)
    df['trade_date'] = pd.to_datetime(df['trade_date'])
    df.set_index('trade_date', inplace=True)
    df = df.sort_index()

    # 重命名列名以适配mplfinance
    df.rename(columns={
        'open': 'Open',
        'high': 'High',
        'low': 'Low',
        'close': 'Close',
        'vol': 'Volume'
    }, inplace=True)

    return df


# 绘制K线图
def plot_k_line(stock_data, stock_code):
    mpf.plot(stock_data, type='candle', style='charles', title=stock_code,
             ylabel='Price', volume=True, mav=(5, 10, 20))


# 进行K-means聚类
def kmeans_clustering(stock_data, n_clusters):
    features = stock_data[['Open', 'High', 'Low', 'Close']]
    kmeans = KMeans(n_clusters=n_clusters)
    stock_data['cluster'] = kmeans.fit_predict(features)
    return kmeans, stock_data


# 主函数
if __name__ == "__main__":
    stock_code = '000001.SZ'  # 例：平安银行
    start_date = '20220101'
    end_date = '20220501'

    # 获取股票数据
    stock_data = get_stock_data(stock_code, start_date, end_date)

    # 绘制K线图
    plot_k_line(stock_data, stock_code)

    # 进行K-means聚类分析
    n_clusters = 4
    kmeans, clustered_data = kmeans_clustering(stock_data, n_clusters)

    # 打印聚类结果
    print(clustered_data[['Open', 'High', 'Low', 'Close', 'cluster']])

    # 显示聚类结果
    plt.figure(figsize=(14, 7))
    plt.scatter(clustered_data.index, clustered_data['Close'], c=clustered_data['cluster'])
    plt.title('K-means Clustering of Stock Prices')
    plt.xlabel('Date')
    plt.ylabel('Close Price')
    plt.show()
