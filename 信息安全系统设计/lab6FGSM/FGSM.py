import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.optim as optim
import os
import numpy as np
import matplotlib.pyplot as plt
import torchvision.models as models
import datetime

transform = transforms.Compose([
    transforms.Resize(224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

testset = torchvision.datasets.CIFAR100(root='./data', train=False, download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=200, shuffle=False)

net = models.resnet34(pretrained=False)

fc_inputs = net.fc.in_features
net.fc = nn.Sequential(
    nn.Linear(fc_inputs, 100),
    nn.LogSoftmax(dim=1)
)

net.load_state_dict(torch.load('./model/resnet34cifar100.pkl'))
mydict = net.state_dict()


def fgsm_attack(image, epsilon, data_grad):
    sign_data_grad = data_grad.sign()
    perturbed_image = image + epsilon * sign_data_grad
    perturbed_image = torch.clamp(perturbed_image, 0, 1)
    return perturbed_image


def test(net, device, test_loader, epsilon):
    correct = 0
    total = 0
    att_time = datetime.timedelta()
    adv_examples = []

    for data, target in test_loader:
        data, target = data.to(device), target.to(device)
        data.requires_grad = True
        total += target.size(0)

        output = net(data)
        _, init_pred = output.max(1)

        start = datetime.datetime.now()
        loss = nn.CrossEntropyLoss()(output, target)
        net.zero_grad()
        loss.backward()
        data_grad = data.grad.data
        perturbed_data = fgsm_attack(data, epsilon, data_grad)
        end = datetime.datetime.now()
        att_time += (end - start)

        output = net(perturbed_data)
        _, final_pred = output.max(1)
        correct += torch.sum(final_pred.eq(target)).item()

        for i in range(len(data)):
            if len(adv_examples) < 5:
                adv_ex = perturbed_data[i].squeeze().detach().cpu().numpy()
                adv_examples.append((init_pred[i].item(), final_pred[i].item(), adv_ex))

    final_acc = correct / total
    print(f'Att time: {att_time}')
    print(f'Epsilon: {epsilon} | Adv acc : {final_acc*100:.3f}%')

    return final_acc, adv_examples


device = torch.device('cuda')
net.to(device)

accuracies = []
examples = []

epsilons = [0, .05, .1, .15, .2, .25, .3]

for eps in epsilons:
    acc, ex = test(net, device, testloader, eps)
    accuracies.append(acc)
    examples.append(ex)

def save_adversarial_examples(examples, epsilons):
    count = 0
    for eps, ex in zip(epsilons, examples):
        for i, (orig, adv, ex_img) in enumerate(ex):
            ex_img = ex_img.transpose(1, 2, 0)
            ex_img = np.clip((ex_img * 255), 0, 255).astype(np.uint8)

            plt.imshow(ex_img)
            plt.axis('off')
            plt.title(f"Original: {orig}, Adversarial: {adv}")
            plt.savefig(f'./picture/fgsm_epsilon_{eps}_{i}.png', bbox_inches='tight')
            count += 1


save_adversarial_examples(examples, epsilons)
