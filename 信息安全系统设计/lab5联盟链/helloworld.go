func (t *HelloChaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
    fmt.Println("实例化....")
    
    _, args := stub.GetFunctionAndParameters()
    if len(args) != 2 {
        return shim.Error("Args Err!")
    }

    fmt.Println("Save data......")

    err := stub.PutState(args[0], []byte(args[1]))
    if err != nil {
        return shim.Error("Save data err...")
    }

    fmt.Println("实例化成功")

    return shim.Success(nil)
}
