from helium import *
import random
import time


def clear_and_write(data, label):
    click(S('.input-control', to_right_of=label))
    press(CONTROL + 'a')
    press(DELETE)
    write(data, label)


def get_output():
    payment = ''
    for element in find_all(S('table')):
        highlight(element)
        payment = payment + element.web_element.text
        time.sleep(.2)
    for i in range(6):
        print(payment.split('\n')[i])


driver = helium.start_chrome(r'file:///D:\downloads\lab\index.html')
time.sleep(1)

click('商业贷款')
time.sleep(1)

for j in range(5):
    price = random.randint(50, 200)
    ratio = random.randint(10, 50)
    loan = price * (100 - ratio) / 100
    interest = random.randint(350, 500) / 100
    clear_and_write(price, '房价总额')
    clear_and_write(ratio, '首付比例')
    clear_and_write(loan, '商业贷款金额')
    clear_and_write(interest, '商业贷款利率')
    click('计 算')
    print(f'第{j + 1}个测试数据: ')
    print(f'房价总额: {price}, 首付比例: {ratio}, 商业贷款金额: {loan}, 商业贷款利率: {interest}.')
    get_output()
    press(F5)


kill_browser()
