def triangle(a, b, c):
    if a + b > c and a + c > b and b + c > a:
        if a == b and b == c:
            return "等边三角形"
        elif a == b or a == c or b == c:
            return "等腰三角形"
        else:
            return "普通三角形"
    else:
        return "不是三角形"


def test_000():
    assert triangle(1, 2, 3) == "不是三角形"


def test_001():
    assert triangle(3, 4, 5) == "普通三角形"


def test_002():
    assert triangle(2, 2, 3) == "等腰三角形"


def test_003():
    assert triangle(3, 3, 3) == "等边三角形"


if __name__ == '__main__':
    print(triangle(2,3 , 4))
