import requests
import re
import json


def get_film_data():
    for i in range(0, 250, 25):
        url = 'https://movie.douban.com/top250?start=' + str(i) + '&filter='
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0"}
        response = requests.get(url, headers=headers)
        text = response.text
        regix = '<div class="item">.*?<div class="pic">.*?<em class="">(.*?)</em>.*?<img.*?src="(.*?)" class="">.*?div class="info.*?class="hd".*?class="title">(.*?)</span>.*?class="other">(.*?)</span>.*?<div class="bd">.*?<p class="">(.*?)<br>(.*?)</p>.*?class="star.*?<span class="(.*?)"></span>.*?span class="rating_num".*?average">(.*?)</span>'
        results = re.findall(regix, text, re.S)
        for item in results:
            dic = {
                '电影名称': item[2] + ' ' + re.sub('&nbsp;', '', item[3]),
                '导演和演员': re.sub('&nbsp;', '', item[4].strip()),
                '评分': item[7] + '分',
                '排名': item[0]
            }
            print(dic)
            file_data(dic)


def file_data(dic):
    with open(r'D:\douban_film.txt', 'a', encoding='utf-8') as f:
        f.write(json.dumps(dic, ensure_ascii=False) + '\n')


get_film_data()
