# rep E-mail 实现

以 163 邮箱为例, 设置中开启 smtp 和 pop3 服务, 并记录授权码.
运行 "appwiz.cpl" 进入 Windows 程序与功能, 启用 "telnet 客户端".

```bash
telnet smtp.163.com 25 # smtp 发送邮件
helo smtp # 确认连接
auth login # 登录
# 在 "dXN1cm5hbWU6"(即 "username:") 下一行输入 base64 编码后的用户名
# 在 "UGFzc3dvcmQ6"(即 "password:") 下一行输入 base64 编码后的授权码
# 返回 235 说明登录成功
MAIL FROM: <yours@email>
RCPT TO: <recipient@email>
DATA # 开始写邮件
subject: xxx # 主题
xxxyyyzzz # 内容
. # 发送
```

```bash
telnet pop3.163.com 110 # pop3 拉取邮件
user # 后接用户名
pass # 后接授权码
list # 查看未读邮件列表
retr 1 # 查看第一封邮件内容
dele 1 # 第一封邮件变为已读
```

# Lab1 TCP 抓包分析

wireshar 过滤规则: "tcp and ip.addr eq <ip 地址>"

# Lab2 网络设备配置

回车可进入命令模式; "conf t" 进入配置模式; "exit" 退出当前模式.

# Lab3 DNS 服务配置

[Windows Server 2019 下载地址](https://go.microsoft.com/fwlink/p/?LinkID=2195280&clcid=0x804&culture=zh-cn&country=CN)

Windows Server 2019 激活码: BH9T4-4N7CW-67J3M-64J36-WW98Y

**注：不要安装 vmtool, 会导致未知错误**

[Windows 虚拟机配置静态 IP 地址](https://blog.csdn.net/qq_51246603/article/details/126808125)

[Windows Server DNS 服务器搭建](https://blog.csdn.net/qq_45153670/article/details/105881608)

```bash
# 正向查找验证
nslookup <域名>

# 反向查找验证
nslookup -qt=ptr <IP 地址>
```

# Lab4 Web 服务配置

[Windows Server Web 服务器搭建](https://blog.csdn.net/qq_26012795/article/details/128304647)
