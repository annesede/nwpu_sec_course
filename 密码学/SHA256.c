#include <stdio.h>
#include <stdlib.h>

typedef unsigned int Byte4;

typedef struct
{
	char *Begin;
	char *End;
}
String;

typedef struct
{
	Byte4 *Begin;
	Byte4 *End;
}
StreamByte4;

#define M_LEN 64
#define MAX_LEN (M_LEN << 4)
#define RN(X, n) ((X << (32 - n)) | (X >> n))
#define SN(X, n) (X >> n)
#define CH(X, Y, Z) ((X & Y) ^ (~X & Z))
#define MAJ(X, Y, Z) ((X & Y) ^ (X & Z) ^ (Y & Z))
#define SIGMA_E0(X) (RN(X, 2) ^ RN(X, 13) ^ RN(X, 22))
#define SIGMA_E1(X) (RN(X, 6) ^ RN(X, 11) ^ RN(X, 25))
#define SIGMA_0(X) (RN(X, 7) ^ RN(X, 18) ^ SN(X, 3))
#define SIGMA_1(X) (RN(X, 17) ^ RN(X, 19) ^ SN(X, 10))

static const Byte4 K[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

String GetString(long long size);
StreamByte4 BitPadding(String msg);
void Hash(StreamByte4 pdg);

int main()
{
	String msg = GetString(MAX_LEN);
	StreamByte4 pdg = BitPadding(msg);
	Hash(pdg);
	free(msg.Begin);
	msg.Begin = NULL;
	return 0;
}

String GetString(long long size)
{
	register char* begin = NULL, *end = NULL;
	for (end = begin = (char*)calloc(size, 1); (*end = (char)getchar()) != '\n'; ++end);
	if (end == begin) exit(EXIT_FAILURE);
	*end = '\0';

	String str; str.Begin = begin; str.End = end;
	return str;
}

StreamByte4 BitPadding(String msg)
{
	register char *begin = msg.Begin, *end = msg.End;
	register long long len, size;
	size = end - begin;
	len = size & 63;
	len = size + ((len < 65) ? (M_LEN - len) : (M_LEN | (M_LEN - len)));
	*(end + size) = (char) 0x80;
	begin = end + (size | 3);

	register char tmp1, tmp2;
	do
	{
		tmp1 = *begin; *begin = *(begin - 3); --begin;
		tmp2 = *begin; *begin = *(begin - 1); --begin;
		*begin = tmp2; --begin;
		*begin = tmp1; --begin;
	}
	while (end < begin);

	StreamByte4 pdg;
	pdg.Begin = (Byte4*) end;
	*(pdg.Begin + len - 2) = size >> 29;
	*(pdg.Begin + len - 1) = size << 3;
	pdg.End = pdg.Begin + len;
	return pdg;
}

void Hash(StreamByte4 pdg)
{
	register Byte4 *m = pdg.Begin, *w = pdg.End;
	register Byte4 *kp, a, b, c, d, e, f, g, h, t1,
		h0 = 0x6a09e667,  h1 = 0xbb67ae85,  h2 = 0x3c6ef372,  h3 = 0xa54ff53a,
		h4 = 0x510e527f,  h5 = 0x9b05688c,  h6 = 0x1f83d9ab,  h7 = 0x5be0cd19;
	do
	{
		a = h0; b = h1; c = h2; d = h3;
		e = h4; f = h5; g = h6; h = h7;
		kp = K;
		do
		{
			*w = *m;
			t1 = h + SIGMA_E1(e) + CH(e, f, g) + *w + *kp;
			h = g;  g = f;  f = e;  e = d + t1;
			d = c;  c = b;  b = a;  a = t1 + SIGMA_E0(b) + MAJ(b, c, d);
			w++;  kp++;  m++;
		}
		while (kp != &K[16]);
		do
		{
			*w = SIGMA_1(w[-2]) + w[-7] + SIGMA_0(w[-15]) + w[-16];
			t1 = h + SIGMA_E1(e) + CH(e, f, g) + *w + *kp;
			h = g;  g = f;  f = e;  e = d + t1;
			d = c;  c = b;  b = a;  a = t1 + SIGMA_E0(b) + MAJ(b, c, d);
			w++;  kp++;
		}
		while (kp != &K[63]);
		t1 = h + SIGMA_E1(e) + CH(e, f, g) + *kp + SIGMA_1(w[-2]) + w[-7] + SIGMA_0(w[-15]) + w[-16];
		h1 += a; h2 += b; h3 += c; h4 += d + t1;
		h5 += e; h6 += f; h7 += g; h0 += t1 + SIGMA_E0(a) + MAJ(a, b, c);
		w -= 63;
	}
	while (m != w);
	printf("SHA-256: %.8x%.8x%.8x%.8x%.8x%.8x%.8x%.8x", h0, h1, h2, h3, h4, h5, h6, h7);
}