#include <stdio.h>
#include <string.h>

typedef unsigned char uint8;

void removeNewline(uint8 *str){
    char *find = strchr(str,'\n');
    if(find){
        *find = '\0';
    }
}

void rc4EnDe(uint8 *S, char *text, char *result);
void init(uint8 *S);

int main(){
    uint8 S[256] ={0};
    uint8 text[8000] = "";
    printf("Text:");
    fgets(text,8000,stdin);
    removeNewline(text);
    unsigned long len = strlen(text);
    uint8 result[len+1];
    memset(result,0,len+1);
    init(S);
    rc4EnDe(S,text,result);
    printf("%s",result);
    return 0;
}

void init(uint8 *S){
    uint8 key[256] = "", T[256] = "";
    printf("Key:");
    fgets(key,256,stdin);
    removeNewline(key);
    int keyLen = strlen(key);
    uint8 tmp = 0, j = 0, k = 0;
    for (int i = 0; i < 256; ++i){
        S[i] = i;
        T[i] = key[i%keyLen];
    }
    for (int i = 0; i < 256; ++i){
        j = (j+S[i]+T[i])%256;
        tmp = S[i];
        S[i] = S[j];
        S[j] = tmp;
    }
}

void rc4EnDe(uint8 *S, char *text, char *result){
    int i = 0, j = 0, t = 0;
    uint8 tmp;
    unsigned long len = strlen(text);
    for (unsigned long k = 0; k < len; ++k){
        i = (i+1)%256;
        j = (j+S[i])%256;
        tmp = S[i];
        S[i] = S[j];
        S[j] = tmp;
        t = (S[i]+S[j])%tmp;
        result[k] = text[k]^S[t];
    }
}