// MAX: 400000

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

// List
void workMode(int mode);

int gcd(int a, int b);
int extEuclid(int a, int b, int* x, int* y);
void factPrime(int n, int *primeFact);
bool nMod1(int a, int n, int p, int* primeFact);
int jacobi(int a, int n);
void wilson(int p);
void fermatX(int p);
void solovayStrassenX(int p);
void millerRabin(int p);
int minPrimeRoot(int p);
int rho(int n);
int pollard(int n);

int inverseExtEuclid(int a, int m); // 1. Modular Inverse
void isPrime(int p); // 2. Is Prime (Wilson)
int phiEulerLinerSieve(int n); // 3. Euler Phi Function
int fastPowerMod (int t, int ex, int modular); // 4. Modular Power
void linearCongEq (int a, int b, int m, int ansX[]); // 5. Linear Congruent Equation
long crt (int a[],int m[],int n); // 6. Chinese Remainder Theorem
int fastLegendre(int a, int p); // 7. Legendre Signal
void allPrimeRoot(int p); // 8. Primitive Root
int solvePrimeFact(int n); // 9. ???

int main (){
    while(1){
        printf("Work mode:\n"
               "1. Modular Inverse\n"
               "2. Is Prime\n"
               "3. Euler Phi Function\n"
               "4. Modular Power\n"
               "5. Linear Congruent Equation\n"
               "6. Chinese Remainder Theorem\n"
               "7. Legendre Signal\n"
               "8. Primitive Root\n"
               "0. Quit\n");
        int mode;
        scanf("%d",&mode);
        if (mode == 0){
            break;
        } else{
            workMode(mode);
            printf("\n");
        }
    }
    return 0;
}

// Euclid
int gcd(int a, int b){
    return b == 0 ? a : gcd(b,a%b);
}

int extEuclid(int a, int b, int* x, int* y){
    if (b == 0){
        *x = 1;
        *y = 0;
        return a;
    } else{
        int tempX,tempY;
        int gcd = extEuclid(b,a%b,&tempX,&tempY);
        *y = tempX-(a/b)*tempY;
        *x = tempY;
        return gcd;
    }
}

// Cpp version
/*#include <tuple>
tuple<int,int,int>extEuclidCpp(int a, int b){
    if (b==0){
        return make_tuple(a,1,0);
    } else{
        int x,y,gcd;
        tie(x,y,gcd) = extEuclidCpp(b,a%b);
        return make_tuple(gcd,y,x-(a/b)*y);
    }
}*/

int inverseExtEuclid(int a, int m){
    int s,t;
    int gcd = extEuclid(a,m,&s,&t);
    if (gcd == 1){
        return s;
    } else{
        return 0;
    }
}

void isPrime(int p){
    wilson(p);
    fermatX(p);
    solovayStrassenX(p);
    millerRabin(p);
}

void wilson(int p){
    int factMod = 1;
    for (int i = p-1; i >= 1; --i){
        factMod = (factMod*i)%p;
    }
    if((factMod+1)%p == 0){
        printf("Wilson: Prime\n");
    } else{
        printf("Wilson: Composite\n");
    }
}

// O(n)
int phiEulerLinerSieve(int n){
    int phi[n+1],prime[n+1];
    bool isSieved[n+1];
    int count = 1, composite;
    prime[0] = 1;
    phi[1] = 1;
    for (int i = 2; i < n; ++i){
        if (!isSieved[i]){
            prime[count++] = i;
            phi[i] = i-1;
        }
        for (int j = 1; i*prime[j] <= n; ++j){
            composite = i*prime[j];
            isSieved[composite] = true;
            if (i%prime[j] == 0){
                phi[composite] = prime[j]*phi[i];
                break;
            } else{
                phi[composite] = (prime[j]-1)*phi[i];
            }
        }
    }
    return phi[n];
}

int fastPowerMod (int t, int e, int m){
    int r = 1;
    while (e){
        if (e&1){
            r = (1LL*r*t)%m;
        }
        t = (1LL*t*t)%m;
        e >>= 1;
    }
    return r;
}

void linearCongEq (int a, int b, int m, int ansX[]){
    a %= m;
    b %= m;
    int s,t;
    int origM = m;
    int d = extEuclid(a,m,&s,&t);
    if (b%d == 0){
        b /= d;
        m /= d;
        s %= m;
        for (int r = 0; r <= d-1; ++r){
        ansX[r] = ((s*b+r*m)%(origM)+origM)%origM;
        }
        for (int i = 0; i <= d-1; ++i){
            printf("x(%d)=%d\n",i+1,ansX[i]);
        }
    } else{
        printf("No answer!\n");
    }
}

long crt (int a[],int m[],int n){
    int modSepM[n];
    int modIevM[n];
    long modM[n];
    long prodM = 1;
    long x = 0;
    for (int i = 0; i < n; ++i){
        prodM *= m[i]*1LL;
    }
    for (int i = 0; i < n; ++i){
        modM[i] = 1LL*prodM/m[i];
        modSepM[i] = (1LL*modM[i])%m[i];
        modIevM[i] = inverseExtEuclid(modSepM[i],m[i]);
        if (!modIevM[i]){
            return 0;
        }
    }
    for (int i = 0; i < n; ++i){
        x = (x+1LL*modIevM[i]*modM[i]*a[i])%prodM;
    }
    x = (x+prodM)%prodM;
    return x;
}

int jacobi(int a, int n){
    int e = 0, s;
    if (a == 0 || a == 1){
        return a;
    } else{
        while (a%2 == 0){
            a >>= 1;
            ++e;
        }
        if (e%2 == 0 || n%8 == 1 || n%8 == 7){
            s = 1;
        } else{
            s = -1;
        }
        if (n%4 == 3 && a%4 == 3){
            s = -s;
        }
        if (a == 1){
            return s;
        } else{
            return s*jacobi(n%a,a);
        }
    }
}

int fastLegendre(int a, int p){
    return jacobi(a,p);
}

void factPrime(int n, int *primeFact){
    bool isSieved[n];
    int prime[n];
    prime[0] = 1;
    memset(primeFact,0,10*sizeof(int));
    int countPrime = 1, countFact = 0, composite;
    for (int i = 2; i <= n/2; ++i){
        if (!isSieved[i]){
            prime[countPrime++] = i;
            if (n%i == 0){
                primeFact[countFact++] = i;
            }
        }
        for (int j = 1; i*prime[j] <= n; ++j){
            composite = i*prime[j];
            isSieved[composite] = true;
            if (i % prime[j] == 0){
                break;
            }
        }
    }
}

bool nMod1(int a, int n, int p, int* primeFact){
    int num = 0, r;
    while(primeFact[num]){
        r = fastPowerMod(a,n/primeFact[num],p);
        if (r == 1){
            break;
        } else{
            ++num;
        }
        if(!primeFact[num]){
            return true;
        }
    }
    return false;
}

int minPrimeRoot(int p){
    int n = p-1, res = 0;
    int primeFact[10] ={0};
    factPrime(n,primeFact);
    for (int i = 2; i <= p/2; ++i){
        if (nMod1(i,n,p,primeFact)){
            res = i;
            break;
        }
    }
    return res;
}

void allPrimeRoot(int p){
    int d = 0, m = p-1, g, r;
    g = minPrimeRoot(p);
    for (int i = 0; i < m; ++i){
        if (gcd(i,m) == 1){
            r = fastPowerMod(g,i,p);
            printf("%d ",r);
            ++d;
        }
    }
    printf("\nPrimitive Root Num: %d\n"
           "Minimal: %d\n",d,g);
}

void fermatX(int p){
    int a, r;
    for (int i = 0; i < 10; ++i){
        srand((unsigned int)time(0));
        a = rand()%p+1;
        r = fastPowerMod(a,p-1,p);
        if (r != 1){
            printf("Fermat X: Composite\n");
            return;
        }
    }
    printf("Fermat X: Prime\n");
}

void solovayStrassenX(int p){
    int a, x, y;
    for (int i = 0; i < 10; ++i){
        srand((unsigned int)time(0));
        a = rand()%p+1;
        x = jacobi(a,p);
        if (x == 0){
            printf("Solovay Strassen X: Composite\n");
            return;
        } else{
            x = (x+p)%p;
            y = fastPowerMod(a,(p-1)/2,p);
            if (x != y){
                printf("Solovay Strassen X: Composite\n");
                return;
            }
        }
    }
    printf("Solovay Strassen X: Prime\n");
}

void millerRabin(int p){
    int k = 0, t = p-1, a, r;
    while(t%2 == 0){
        t >>= 1;
        ++k;
    }
    srand((unsigned int)time(0));
    a = rand()%p+1;
    r = fastPowerMod(a,t,p);
    if (r == 1){
        printf("Miller Rabin X: Prime\n");
        return;
    } else{
        for (int j = 0; j < k; ++j){
            if (r == p-1){
                printf("Miller Rabin: Prime\n");
                return;
            } else{
                r = (1LL*r*r)%p;
            }
        }
    }
    printf("Miller Rabin: Composite\n");
}

int rho(int n){
    int a = 2, b = 2, d;
    do {
        a = ((1LL*a*a)+1)%n;
        b = ((1LL*b*b)+1)%n;
        b = ((1LL*b*b)+1)%n;
        d = gcd(a-b,n);
        if ((1 < d) && (d < n)){
            return d;
        }
    } while (d != n);
    return -1;
}

int pollard(int n){
    int B = 24;
    int a = 2;
    for (int i = 2; i <= B; ++i) {
        a = fastPowerMod(a,i,n);
    }
    int d = gcd(a-1,n);
    if ((d > 1) && (d < n)){
        return d;
    }
    return -1;
}

void workMode(int mode){
    switch (mode){
        case 1:{ // Modular Inverse
            int a, m, aInverse;
            scanf("%d %d", &a, &m);
            aInverse = inverseExtEuclid(a, m);
            printf("The inverse of %d up to %d is %d\n", a, m, aInverse);
            break;
        }
        case 2:{ // Is Prime (Wilson)
            int p;
            scanf("%d", &p);
            isPrime(p);
            break;
        }
        case 3:{ // Euler Phi Function
            int n, phi;
            scanf("%d", &n);
            phi = phiEulerLinerSieve(n);
            printf("EulerPhi(%d)=%d\n", n, phi);
            break;
        }
        case 4:{ // Modular Power
            int t, e, m, r;
            scanf("%d %d %d", &t, &e, &m);
            r = fastPowerMod(t, e, m);
            printf("%d^%d mod %d = %d\n", t, e, m, r);
            break;
        }
        case 5:{ // Linear Congruent Equation
            int a, b, m;
            scanf("%d %d %d", &a, &b, &m);
            int ansX[m];
            linearCongEq(a, b, m, ansX);
            break;
        }
        case 6:{ // Chinese Remainder Theorem
            int numLinEq;
            scanf("%d", &numLinEq);
            int a[numLinEq], m[numLinEq];
            for (int i = 0; i < numLinEq; ++i){
                scanf("%d %d", &a[i], &m[i]);
            }
            long crtX = crt(a, m, numLinEq);
            printf("%ld", crtX);
            break;
        }
        case 7:{ // Legendre Signal
            int a, p, l;
            scanf("%d %d", &a, &p);
            l = fastLegendre(a, p);
            printf("l(%d,%d)=%d\n", a, p, l);
            break;
        }
        case 8:{ // Primitive Root
            int p;
            scanf("%d",&p);
            allPrimeRoot(p);
            break;
        }
        default:{
            printf("Invalid Number!\n");
            int newMode;
            scanf("%d",&newMode);
            workMode(newMode);
        }
    }
}