#include <stdio.h>
#include <stdbool.h>
#include <string.h>

typedef unsigned char Byte;
typedef bool Bit;

void removeNewline(Byte *str){
    char *find = strchr(str,'\n');
    if(find){
        *find = '\0';
    }
}

void desEn(Byte *cipher, Byte *plain);
void desDe(Byte *plain, Byte *cipher);
void keyExp(Byte *key);
void F(Bit *output, Bit *input);
void SPermute(Bit *permuted, Bit *origin);
void tablePermute(Bit *permuted, Bit *origin, Byte *table, Byte size);

Byte bit8Reverse(Byte x);
void byteToBit(Bit *bit, Byte *byte, size_t len);
void bitToByte(Byte *byte, Bit *bit, size_t len);
void bitCopy(Bit *copy, Bit *origin, Byte len);
void bitXor(Bit *opL, Bit *opR, Byte len);
void bitRot(Bit *x, Byte len, Byte move);

// permutation table
const Byte PC1[56] = {
        57, 49, 41, 33, 25, 17, 9,
        1, 58, 50, 42, 34, 26, 18,
        10, 2, 59, 51, 43, 35, 27,
        19, 11, 3, 60, 52, 44, 36,
        63, 55, 47, 39, 31, 23, 15,
        7, 62, 54, 46, 38, 30, 22,
        14, 6, 61, 53, 45, 37, 29,
        21, 13, 5, 28, 20, 12, 4};

const Byte PC2[48] = {
        14, 17, 11, 24, 1, 5,
        3, 28, 15, 6, 21, 10,
        23, 19, 12, 4, 26, 8,
        16, 7, 27, 20, 13, 2,
        41, 52, 31, 37, 47, 55,
        30, 40, 51, 34, 33, 48,
        44, 49, 39, 56, 34, 53,
        46, 42, 50, 36, 29, 32};

const Byte LS[16] = {
        1, 1, 2, 2, 2, 2, 2, 2,
        1, 2, 2, 2, 2, 2, 2, 1};

const Byte IP[64] = {
        58, 50, 42, 34, 26, 18, 10, 2,
        60, 52, 44, 36, 28, 20, 12, 4,
        62, 54, 46, 38, 30, 22, 14, 6,
        64, 56, 48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17, 9, 1,
        59, 51, 43, 35, 27, 19, 11, 3,
        61, 53, 45, 37, 29, 21, 13, 5,
        63, 55, 47, 39, 31, 23, 15, 7};

const Byte IPR[64] = {
        40, 8, 48, 16, 56, 24, 64, 32,
        39, 7, 47, 15, 55, 23, 63, 31,
        38, 6, 46, 14, 54, 22, 62, 30,
        37, 5, 45, 13, 53, 21, 61, 29,
        36, 4, 44, 12, 52, 20, 60, 28,
        35, 3, 43, 11, 51, 19, 59, 27,
        34, 2, 42, 10, 50, 18, 58, 26,
        33, 1, 41, 9, 49, 17, 57, 25};

const Byte E[48] = {
        32, 1, 2, 3, 4, 5,
        4, 5, 6, 7, 8, 9,
        8, 9, 10, 11, 12,
        13, 12, 13, 14, 15, 16, 17,
        16, 17, 18, 19, 20, 21,
        20, 21, 22, 23, 24, 25,
        24, 25, 26, 27 ,28, 29,
        28, 29, 30, 31, 32, 1};

const Byte P[32] = {
        16, 7, 20, 21, 29, 12, 28, 17,
        1, 15, 23, 26, 5, 18, 31, 10,
        2, 8, 24, 14, 32, 27, 3, 9,
        19, 13, 30, 6, 22, 11, 4, 25};

Byte S[8][4][16] = {
        // S1
        14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
        0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
        4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
        15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13,
        // S2
        15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
        3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
        0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
        13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9,
        // S3
        10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
        13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
        13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
        1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12,
        // S4
        7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
        13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
        10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
        3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14,
        // S5
        2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
        14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
        4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
        11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3,
        // S6
        12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
        10, 15, 4, 2, 7, 12, 0, 5, 6, 1, 13, 14, 0, 11, 3, 8,
        9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
        4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13,
        // S7
        4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
        13, 0, 11, 7, 4, 0, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
        1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
        6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12,
        // S8
        13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
        1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
        7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
        2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11};

static Bit subKey[16][48];

int main(){

    Byte key[9] = "", text[8000] = "";
    printf("Text:\n");
    fgets(text,8000,stdin);
    removeNewline(text);
    printf("key:\n");
    fgets(key,9,stdin);
    removeNewline(key);
    keyExp(key);
    size_t len = (strlen(text)/8+1)*8;
    Byte result[len+1];
    memset(result,0,len+1);

    int mode;
    Byte *pText = text;
    Byte *pResult = result;
    printf("1 to Encrypt, 2 to Decrypt\n");
    scanf("%d",&mode);
    switch (mode) {
        case 1: {
            for (int i = 0; i < len/8; ++i) {
                desEn(pResult,pText);
                pText += 8;
                pResult += 8;
            }
            break;
        }
        case 2: {
            for (int i = 0; i < len/8; ++i) {
                desDe(pResult,pText);
                pText += 8;
                pResult += 8;
            }
            break;
        }
        default:
            printf("Invalid Number!\n");
    }

	for (size_t i = 0; i < len; ++i)
		printf("%02X ", result[i]);
    return 0;
}

Byte bit8Reverse(Byte x){
    x = ((x&0xaa)>>1)|((x&0x55)<<1);
    x = ((x&0xcc)>>2)|((x&0x33)<<2);
    return (x>>4)|(x<<4);
}

void byteToBit(Bit *bit, Byte *byte, size_t len){
    for (int i = 0; i < len; ++i) {
        for (int j = 0; j < 8; ++j) {
            bit[8*i+j] = (byte[i]>>(7-j))&1;
        }
    }
}

void bitToByte(Byte *byte, Bit *bit, size_t len){
    for (int i = 0; i < len/8; ++i) {
        for (int j = 0; j < 8; ++j) {
            byte[i] |= ((Byte)bit[8 * i + j]) << j;
        }
        byte[i] = bit8Reverse(byte[i]);
    }
}

void bitCopy(Bit *copy, Bit *origin, Byte len){
    for (int i = 0; i < len; ++i) {
        copy[i] = origin[i];
    }
}

void bitXor(Bit *opL, Bit *opR, Byte len){
    for (int i = 0; i < len; ++i) {
        opL[i] ^= opR[i];
    }
}

void bitRot(Bit *x, Byte len, Byte move){
    Bit tmp[64] = { 0};
    bitCopy(tmp,x,move);
    bitCopy(x,x+move,len-move);
    bitCopy(x+(len-move),tmp,move);
}

void tablePermute(Bit *permuted, Bit *origin, Byte *table, Byte size){
    Bit tmp[64] = { 0};
    for (int i = 0; i < size; ++i) {
        tmp[i] = origin[table[i]-1];
    }
    bitCopy(permuted,tmp,size);
}

void keyExp(Byte *key){
    Bit keyPerRound[64] = { 0};
    Bit *keyPrL = &keyPerRound[0];
    Bit *keyPrR = &keyPerRound[28];
    byteToBit(keyPerRound,key,8);
    tablePermute(keyPerRound,keyPerRound,PC1,56);
    for (int i = 0; i < 16; ++i) {
        bitRot(keyPrL,28,LS[i]);
        bitRot(keyPrR,28,LS[i]);
        tablePermute(subKey[i],keyPerRound,PC2,48);
    }
}

void SPermute(Bit *permuted, Bit *origin){
    int row, col;
    Bit *p1 = origin;
    Bit *p2 = permuted;
    for (int i = 0; i < 8; ++i) {
        row = 2*p1[0]+p1[5];
        col = 8*p1[1]+4*p1[2]+2*p1[3]+p1[4];
        byteToBit(permuted,&S[i][row][col],4);
        p1 += 6;
        p2 += 4;
    }
}

void F(Bit *output, Bit *input){
    Bit tmp[48] = {0};
    tablePermute(tmp,input,E,48);
    bitXor(tmp,input,48);
    SPermute(output,tmp);
    tablePermute(output,output,P,32);
}

void desEn(Byte *cipher, Byte *plain){
    Bit textBit[64] = {0}, tmp[32] = {0};
    Bit *textBitL = &textBit[0];
    Bit *textBitR = &textBit[32];
    byteToBit(textBit,plain,8);
    tablePermute(textBit,textBit,IP,64);
    for (int i = 0; i <= 15; ++i) {
        bitCopy(tmp,textBitR,32);
        F(textBitR,subKey[i]);
        bitXor(textBitR,textBitL,32);
        bitCopy(textBitL,tmp,32);
    }
    tablePermute(textBit,textBit,IPR,64);
    bitToByte(cipher,textBit,64);
}

void desDe(Byte *plain, Byte *cipher){
    Bit textBit[64] = { 0}, tmp[32] = { 0};
    Bit *textBitL = &textBit[0];
    Bit *textBitR = &textBit[32];
    byteToBit(textBit,cipher,8);
    tablePermute(textBit,textBit,IP,64);
    for (int i = 15; i >= 0; --i) {
        bitCopy(tmp,textBitL,32);
        F(textBitL,subKey[i]);
        bitXor(textBitL,textBitR,32);
        bitCopy(textBitR,tmp,32);
    }
    tablePermute(textBit,textBit,IPR,64);
    bitToByte(plain,textBit,64);
}