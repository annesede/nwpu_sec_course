
作者：annesede

Blog：https://annesede.github.io/

IDE：CLion | Build：CMake 2.26 | Compiler：GCC 8.1.0

---

密码学代码作业：RC4，ZUC-128-EEA3，DES，AES-128，SM4，SHA256。

Math为信息安全数学基础示例代码。

代码并不完全保证正确性，仅供参考。
