from collections import deque
import json

# 定义一个进程类，包含进程的 ID、服务时间、剩余时间以及当前所在的队列级别
class Process:
    def __init__(self, pid, burst_time):
        self.pid = pid                     # 进程的 ID
        self.burst_time = burst_time       # 进程的总服务时间
        self.remaining_time = burst_time   # 进程剩余的服务时间，初始化为总服务时间
        self.queue_level = 0               # 进程所在的队列级别，初始为 0（最高优先级队列）

# 多级反馈队列调度函数，参数：进程列表，时间片列表
def feedback_scheduler(processes, time_quantums):
    # 创建四个队列，分别对应四个优先级
    queues = [deque() for _ in range(4)]
    
    # 将所有进程放入最高优先级队列（第 0 级队列）
    for process in processes:
        queues[0].append(process)

    # 初始化时间和统计数据
    time = 0                              # 当前时间
    waiting_times = []                    # 存储每个进程的等待时间，用于计算平均等待时间
    response_times = {}                   # 字典存储每个进程的响应时间（第一次开始运行的时间）
    completed = 0                         # 已完成的进程数量

    # 主调度循环，直到所有进程完成
    while completed < len(processes):
        # 依次检查每个队列
        for i in range(4):
            # 如果当前队列有进程等待调度
            if queues[i]:
                # 从当前队列中取出一个进程进行处理
                current_process = queues[i].popleft()
                time_slice = time_quantums[i]  # 当前队列对应的时间片

                # 如果该进程首次运行，记录其响应时间
                if current_process.pid not in response_times:
                    response_times[current_process.pid] = time
                
                # 判断进程的剩余时间是否超过当前时间片
                if current_process.remaining_time > time_slice:
                    # 如果超过时间片，当前时间增加时间片长度，进程剩余时间减少
                    time += time_slice
                    current_process.remaining_time -= time_slice
                    # 若进程未完成，降级到下一优先级队列
                    if i < 3:
                        queues[i + 1].append(current_process)
                else:
                    # 如果进程剩余时间小于等于时间片，则该进程将在此时间片内完成
                    time += current_process.remaining_time
                    waiting_time = time - current_process.burst_time  # 计算等待时间
                    waiting_times.append(waiting_time)               # 记录等待时间
                    completed += 1                                   # 完成进程计数+1
                break

    # 计算平均等待时间和平均响应时间
    avg_waiting_time = sum(waiting_times) / len(waiting_times)
    avg_response_time = sum(response_times.values()) / len(response_times)
    return avg_waiting_time, avg_response_time

# 平均等待时间和平均响应时间序列
avg_waiting_time_list = []
avg_response_time_list = []

# 考察不同基础时间片下的平均等待时间和平均响应时间
for T in range (1, 11):
    process_list = [Process(pid=i, burst_time=i * 2) for i in range(5)]  # 创建 5 个进程
    time_quantums = [T, 2 * T, 4 * T, 8 * T]  # 四个队列的时间片长度分别为 T, 2T, 4T 和 8T
    avg_waiting_time, avg_response_time = feedback_scheduler(process_list, time_quantums)
    avg_waiting_time_list.append(avg_waiting_time)
    avg_response_time_list.append(avg_response_time)

# 输出平均等待时间和平均响应时间序列到指定文件
with open('Lab1_Result.json', 'w') as file:
    json.dump(avg_waiting_time_list, file)
    file.write('\n')
    json.dump(avg_response_time_list, file)
