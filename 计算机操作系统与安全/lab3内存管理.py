import random

PAGE_SIZE = 4 * 1024                    # 页面大小4KB
MEMORY_SIZE = 128 * 1024 * 1024         # 物理内存128MB
NUM_PAGES = MEMORY_SIZE // PAGE_SIZE    # 总页面数
NUM_PROCESSES = 5

free_pages = list(range(NUM_PAGES))     # 空闲页面管理 -> 页面号
page_table = {}                         # 进程页表 -> 进程ID：{逻辑页号：物理页号}

def allocate_memory(pid, num_pages):
    global free_pages, page_table

    if len(free_pages) < num_pages:
        print(f"内存不足，无法为进程 {pid} 分配 {num_pages} 页")
        return False

    if pid not in page_table:
        page_table[pid] = {}

    for logical_page in range(num_pages):
        physical_page = free_pages.pop(0)
        page_table[pid][logical_page] = physical_page

    print(f"已为进程 {pid} 分配 {num_pages} 页内存")
    return True

def free_memory(pid):
    global free_pages, page_table

    if pid not in page_table:
        print(f"进程 {pid} 未分配内存，无需回收")
        return

    for logical_page, physical_page in page_table[pid].items():
        free_pages.append(physical_page)

    free_pages.sort()                           # 保持空闲页面有序
    del page_table[pid]
    print(f"已回收进程 {pid} 的内存")

def logical_to_physical(pid, logical_address):
    if pid not in page_table:
        raise ValueError(f"进程 {pid} 未分配内存")

    page_offset = logical_address % PAGE_SIZE
    logical_page = logical_address // PAGE_SIZE

    if logical_page not in page_table[pid]:
        raise ValueError(f"逻辑地址 {logical_address} 超出进程 {pid} 的内存范围")

    physical_page = page_table[pid][logical_page]
    physical_address = physical_page * PAGE_SIZE + page_offset
    return physical_address


if __name__ == "__main__":
    # 内存分配
    for pid_test in range(NUM_PROCESSES):
        allocate_memory(pid_test, random.randint(1, 10))        # 每个进程随机分配1~10页

    # 转换逻辑地址为物理地址
    try:
        pid_test = 0
        logical_address_test = 5000
        physical_address_test = logical_to_physical(pid_test, logical_address_test)
        print(f"进程 {pid_test} 的逻辑地址 {logical_address_test} 对应的物理地址为 {physical_address_test}")
    except ValueError as e:
        print(e)

    # 回收内存
    for pid_test in range(NUM_PROCESSES):
        free_memory(pid_test)
