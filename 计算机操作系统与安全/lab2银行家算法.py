import numpy as np

# 初始化资源向量、最大需求矩阵、已分配矩阵、剩余需求矩阵
resources = np.array([10, 5, 7])  # 三类资源总数
allocated = np.array([[0, 1, 0], [2, 0, 0], [3, 0, 2], [2, 1, 1], [0, 0, 2]])  # 已分配矩阵
max_demand = np.array([[7, 5, 3], [3, 2, 2], [9, 0, 2], [2, 2, 2], [4, 3, 3]])  # 最大需求矩阵

# 计算当前剩余的资源数量
available = resources - allocated.sum(axis=0)

# 计算剩余需求矩阵
need = max_demand - allocated

def is_safe():
    """检测当前状态是否安全"""
    work = available.copy()
    finish = [False] * len(allocated)
    while True:
        safe = False
        for i, need_i in enumerate(need):
            if finish[i] == False and all(need_i <= work):
                work += allocated[i]
                finish[i] = True
                safe = True
        if not safe:
            break
    return all(finish)

def request_resources(process, request):
    """请求资源分配"""
    global available, allocated, need
    if all(request <= need[process]) and all(request <= available):
        # 临时分配
        available -= request
        allocated[process] += request
        need[process] -= request
        # 检查分配后状态是否安全
        if is_safe():
            print(f"请求满足: 进程 {process} 成功分配 {request}")
            return True
        else:
            # 回滚
            available += request
            allocated[process] -= request
            need[process] += request
            print(f"请求不满足: 进程 {process} 请求 {request} 会导致不安全状态")
            return False
    else:
        print(f"请求过多: 进程 {process} 请求 {request} 超过需要或可用资源")
        return False


# 安全请求示例
request_resources(1, np.array([1, 0, 2]))

# 不安全请求示例
request_resources(0, np.array([3, 3, 0]))
